package com.airasia.holder;

public class SQLHolder {

	public static final String COUNTRY_NAME = "CountryName";
	public static final String COUNTRY_CODE = "CountryCode";
	public static final String COUNTRY_DIALING_CODE = "DialingCode";
	
	public static final String STATION_KEY = "StationKey";
	public static final String STATION_NAME = "StationName";
	public static final String STATION_COUNTRY = "StationCountry";
	public static final String STATION_AIRPORT = "StationAirport";
	public static final String STATION_DESTINATION = "StationDesti";

	public static final String SSR_CODE = "SSRCode";
	public static final String SSR_DESCRIP = "Description";
	
	public static final String FAREC_KEY = "ClassKey";
	public static final String FAREC_TITLE = "ClassTitle";
	public static final String FAREC_UPGRADE = "UpgradeTitle";
	public static final String FAREC_BENEFIT = "Benefit";
	
	public static final String FARET_KEY = "FareTKey";
	public static final String FARET_TITLE = "FareTTitle";
	
	public static final String COUNTRY_TABLE = "country_table_list";
	public static final String STATION_TABLE = "station_table_list";
	public static final String DEPART_STATION_TABLE = "depart_destination_table";
	public static final String SSR_TABLE = "ssr_table_list";
	public static final String FAREC_TABLE = "fareclass_table_list";
	public static final String FARET_TABLE = "faretype_table_list";
	
	public static final String CURRENCY_TABLE = "exchange_table_list";
	public static final String CURRENCY_COUNTRY = "exchange_country";
	public static final String CURRENCY_PRICE_LIST = "exchange_price_list";
			
	public final static String CREATE_COUNTRY_TABLE = "CREATE TABLE " + COUNTRY_TABLE + 
	" ( " + COUNTRY_CODE + " TEXT PRIMARY KEY, "+ COUNTRY_NAME +" TEXT, " + COUNTRY_DIALING_CODE + " TEXT ); ";

	
	public final static String CREATE_STAION_TABLE = "CREATE TABLE " + STATION_TABLE + 
			" ( " + STATION_KEY + " TEXT PRIMARY KEY, "+ STATION_NAME +" TEXT, " + STATION_AIRPORT +" TEXT, " + STATION_COUNTRY + " TEXT ); ";

	public final static String CREATE_STAION_DEPART_TABLE = "CREATE TABLE " + DEPART_STATION_TABLE + 
			" ( " + STATION_KEY + " TEXT PRIMARY KEY, "+ STATION_NAME +" TEXT, " + STATION_AIRPORT +" TEXT, "
			+ STATION_DESTINATION +" TEXT, " + STATION_COUNTRY + " TEXT ); ";

	
	public final static String CREATE_SSR_TABLE = "CREATE TABLE " + SSR_TABLE + 
			" ( " + SSR_CODE + " TEXT PRIMARY KEY, "+ SSR_DESCRIP + " TEXT ); ";
	
	public final static String CREATE_FAREC_TABLE = "CREATE TABLE " + FAREC_TABLE + 
			" ( " + FAREC_KEY + " INTEGER PRIMARY KEY, " + FAREC_TITLE +" TEXT, "+  FAREC_UPGRADE +" TEXT, " + FAREC_BENEFIT + " TEXT ); ";

	public final static String CREATE_FARET_TABLE = "CREATE TABLE " + FARET_TABLE + 
			" ( " + FARET_KEY + " INTEGER TEXT KEY, " + FARET_TITLE +" TEXT ); ";
	
	public final static String CREATE_CURRENCY_TABLE = "CREATE TABLE " + CURRENCY_TABLE + 
			" ( " + CURRENCY_COUNTRY + " TEXT TEXT KEY, " + CURRENCY_PRICE_LIST +" BLOB ); ";
	
	
	
}
