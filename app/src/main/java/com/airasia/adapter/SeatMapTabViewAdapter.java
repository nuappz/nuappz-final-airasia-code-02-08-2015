package com.airasia.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.airasia.fragment.SeatMapSelectFragment;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.SegmentModel;

/**
 * Created by NUappz1 on 7/26/2015.
 */
public class SeatMapTabViewAdapter extends FragmentStatePagerAdapter {

    BookingInfoModel booking;

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public SeatMapTabViewAdapter(FragmentManager fm,BookingInfoModel booking) {
        super(fm);

        this.booking = booking;



    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        String journeyKey;
        int segmentKey = position;
        if (position>=booking.getTotalSegmentForFlightOnly(true))
        {
            journeyKey = "1";
            segmentKey -= booking.getTotalSegmentForFlightOnly(true);
        }
        else
        {
            journeyKey = "0";
        }

        Bundle bundle = new Bundle();
        bundle.putString("journey", journeyKey);
        bundle.putString("segment", String.valueOf(segmentKey));
        bundle.putSerializable("flight",booking);


        SeatMapSelectFragment tab = new SeatMapSelectFragment();
        tab.setArguments(bundle);

      /* if(position == 0) // if the position is 0 we are returning the First tab
        {
            return tab1;
        }
        else             // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            SeatMapSelectFragment tab2 = new SeatMapSelectFragment();
            return tab2;
        }       */
        return tab;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {

        int pos = position;
        SegmentModel model;
        if (position>=booking.getTotalSegmentForFlightOnly(true))
        {
            pos -= booking.getTotalSegmentForFlightOnly(true);
            model = booking.getReturnSegments().get(pos);
        }
        else
        {
            model = booking.getDepartSegments().get(pos);
        }
        return model.getDepartReturnStation();
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount()
    {
        if (booking==null)
        {
            return 0;
        }

        return booking.getTotalSegmentForFlightOnly(true)+booking.getTotalSegmentForFlightOnly(false);
    }

}
