package com.airasia.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airasia.holder.ConstantHolder;
import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.MealBundleModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SSRContainer;
import com.airasia.model.SSRItemModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.List;

public class MealListingAdapter extends BaseAdapter{

	SSRContainer container;

	LayoutInflater inflater;
	BookingInfoModel booking;
	ViewHolder.MealHolder holder;
	AQuery aq;
	//SSRItemModel personMeal;
	MemberInfoModel person;
	Activity act;

	public MealListingAdapter(SSRContainer _container,
							  Activity _act,
							  View view,
							  BookingInfoModel _booking,
							  MemberInfoModel _person){
		this.person = _person;
		this.container = _container;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
		this.act = _act;
		this.booking = _booking;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return container.getMealList().size();
	}

	@Override
	public SSRItemModel getItem(int position) {
		// TODO Auto-generated method stub
		SSRItemModel item = container.getMealList().get(position);
		return item;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			holder = new ViewHolder.MealHolder();
			vi = inflater.inflate(R.layout.item_ssr_meal, null);
			holder.img = (ImageView) vi.findViewById(R.id.item_meal_img);
			holder.name = (TextView) vi.findViewById(R.id.item_meal_name);
			holder.dollar = (TextView) vi.findViewById(R.id.item_meal_dollar);
			holder.sens = (TextView) vi.findViewById(R.id.item_meal_sens);
			holder.currency = (TextView) vi.findViewById(R.id.item_meal_currency);
			holder.expandIcon = (ImageView) vi.findViewById(R.id.item_meal_expand);
			holder.bundleLayout = (LinearLayout) vi.findViewById(R.id.item_meal_bundle);
			holder.bundleContainer = (LinearLayout) vi.findViewById(R.id.item_meal_bundle_view);
			vi.setTag(holder);
		}else{
			holder = (ViewHolder.MealHolder) vi.getTag();
		}

		AQuery taq = this.aq.recycle(vi);
		SSRItemModel mSSR = container.getMealList().get(position);
		String url = ConstantHolder.BASE_URL_HTTP+mSSR.getImage();
		LogHelper.debug("Picture url " + url) ;
//		taq.id(holder.img).image(url, false, true);
//		Picasso.with(act).load(url)
//				.into(holder.img);

		ImageLoader.getInstance().displayImage(url, taq.id(holder.img).getImageView(), new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				// Do whatever you want with Bitmap
				ImageView v = (ImageView) view;
				v.setImageBitmap(loadedImage);
				LogHelper.debug("Load Image Success");
			}

			@Override
			public void onLoadingFailed(String imageUri, View view,
										FailReason failReason) {
				// TODO Auto-generated method stub
				ImageView v = (ImageView) view;
				v.setImageResource(R.drawable.icon_food);
				LogHelper.debug("Load Image Fail");
			}

		});
		taq.id(holder.name).text(mSSR.getDescripion());
		taq.id(holder.currency).text(booking.getCurrencyCode());
		String value = String.valueOf(mSSR.getAmountTotal());
		String[] splitTotal = value.split("\\.");
		taq.id(holder.dollar).text(splitTotal[0]);
		taq.id(holder.sens).text(splitTotal[1]);

		LinearLayout bundlerLayout = holder.bundleLayout;
		bundlerLayout.removeAllViews();
		List<SSRItemModel> personMealList = person.getMealItem(container.isDepart(), container.getMealSegPos());
		if (personMealList == null || personMealList.size()<=0){
			LogHelper.debug("No Meal Selected");
			aq.id(holder.bundleContainer).gone();
			taq.id(holder.expandIcon).image(R.drawable.icon_plus);
		}else{
			LogHelper.debug("Bundle View Refresh");
			for (int mealIndex = 0; mealIndex < personMealList.size(); mealIndex++){
				SSRItemModel personMeal = personMealList.get(mealIndex);
				if (personMeal != null &&
						personMeal.getSsrCode() != null &&
						personMeal.getSsrCode().equals(mSSR.getSsrCode())){
					taq.id(holder.expandIcon).image(R.drawable.tick_3);
					if (container.getMealBundle() != null){
						aq.id(holder.bundleContainer).visible();
						LogHelper.debug("Meal Selected");
						for (int i = 0; i < container.getMealBundle().size();i++){
							final MealBundleModel mBundle = container.getMealBundle().get(i);
							View bundleView = inflater.inflate(R.layout.item_meal_bundle, null);
							bundleView.setTag(personMeal);
							ImageView img = (ImageView) bundleView.findViewById(R.id.item_bundle_indicator);
							TextView name = (TextView) bundleView.findViewById(R.id.item_bundle_name);
							TextView sen = (TextView) bundleView.findViewById(R.id.item_bundle_sens);
							TextView dollar = (TextView) bundleView.findViewById(R.id.item_bundle_dollar);
							TextView currency = (TextView) bundleView.findViewById(R.id.item_bundle_currency);

							name.setText(mBundle.getDescripion());
							currency.setText(booking.getCurrencyCode());
							String bundleCost = String.valueOf(mBundle.getAmountTotal());
							String[] costSplit = bundleCost.split("\\.");
							dollar.setText(costSplit[0]);
							sen.setText(costSplit[1]);

							if (personMeal.getBundleList()!=null){
								for (int iBundle = 0; iBundle < personMeal.getBundleList().size(); iBundle++){
									MealBundleModel tempBundle = personMeal.getBundleList().get(iBundle);
									if(tempBundle.getSsrCode().equals(mBundle.getSsrCode())){
										img.setImageResource(R.drawable.tick_3);
										break;
									}else{
										img.setImageResource(R.drawable.icon_plus);
									}

								}
							}else{
								img.setImageResource(R.drawable.icon_plus);
							}

							bundleView.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {

									Object personMealObj = v.getTag();
									if(personMealObj != null && personMealObj instanceof SSRItemModel)
									{
										SSRItemModel personMeal = (SSRItemModel)personMealObj;
										// TODO Auto-generated method stub
										if (personMeal.getBundleList()!=null){
											for (int i = 0; i < personMeal.getBundleList().size(); i ++){
												MealBundleModel tempMeal = personMeal.getBundleList().get(i);
												if (tempMeal!=null &&
														tempMeal.getSsrCode()!=null &&
														tempMeal.getSsrCode().equals(mBundle.getSsrCode())){
													personMeal.removeBundle(tempMeal.getSsrCode());
													notifyDataSetChanged();
													return;
												}
											}
										}

//										suggestionPopup(getActivity(), meal);
										personMeal.addBundle(mBundle);
										notifyDataSetChanged();
									}

								}
							});

							bundlerLayout.addView(bundleView);
						}
					}else{
						aq.id(holder.bundleContainer).gone();
					}
					break;
				}else{
					aq.id(holder.bundleContainer).gone();
					taq.id(holder.expandIcon).image(R.drawable.icon_plus);
				}
			}
		}

		return vi;
	}

	public void updatePerson(MemberInfoModel _person){
		this.person = _person;
		notifyDataSetChanged();
	}

}