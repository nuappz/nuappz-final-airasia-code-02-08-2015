package com.airasia.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.CheckInModel;
import com.airasia.model.FlightsModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

public class FlightAdapter extends BaseExpandableListAdapter {

	List<FlightsModel> items;
	LayoutInflater inflater;
	ViewHolder.FlighItemHolder holder;
	ViewHolder.BookingHeaderHolder headerHolder;
	AQuery aq;
	Activity act;
	
	public FlightAdapter(List<FlightsModel> _items, Activity _act, View view){
		this.items = _items;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
		this.act = _act;
		
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return this.items.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			vi = inflater.inflate(R.layout.item_flight_item, null);
			holder = new ViewHolder.FlighItemHolder();
			holder.fFrom = (TextView) vi.findViewById(R.id.item_flight_from);
			holder.fTo = (TextView) vi.findViewById(R.id.item_flight_to);
			holder.fTime = (TextView) vi.findViewById(R.id.item_flight_date);
			holder.fNUm = (TextView) vi.findViewById(R.id.item_flight_num);
			vi.setTag(holder);
		}else{
			holder = (ViewHolder.FlighItemHolder) vi.getTag();
		}
		AQuery taq = this.aq.recycle(vi);
		
		FlightsModel model = items.get(childPosition);
		taq.id(holder.fTime).text(model.getFlightDate());
		taq.id(holder.fNUm).text(model.getFligthNum());
		taq.id(holder.fTo).text(model.getToCity());
		taq.id(holder.fFrom).text(model.getFromCity());
		
		return vi;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		if (groupPosition == 1){
			return 0;
		}
		return this.items.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View vi = convertView;
		if (vi == null){
			vi = inflater.inflate(R.layout.item_check_in_header, null);
			headerHolder = new ViewHolder.BookingHeaderHolder();
			headerHolder.name = (TextView) vi.findViewById(R.id.expandable_name_header);
			headerHolder.textV1 = (TextView) vi.findViewById(R.id.check_in_location_1);
			headerHolder.textV2 = (TextView) vi.findViewById(R.id.check_in_location_2);
			headerHolder.textV3 = (TextView) vi.findViewById(R.id.check_in_location_3);
			headerHolder.image1 = (ImageView) vi.findViewById(R.id.check_in_location_divider);
			headerHolder.image2 = (ImageView) vi.findViewById(R.id.check_in_location_divider_2);
			vi.setTag(headerHolder);
		}else{
			headerHolder = (ViewHolder.BookingHeaderHolder) vi.getTag();
		}
		AQuery taq = this.aq.recycle(vi);
		
		if (groupPosition == 0){
			aq.id(headerHolder.name).getTextView().setText(R.string.upcoming_flights);
		}else{
			aq.id(headerHolder.name).getTextView().setText(R.string.flown_flights);
		}

		taq.id(headerHolder.textV1).gone();
		taq.id(headerHolder.textV2).gone();
		taq.id(headerHolder.textV3).gone();
		taq.id(headerHolder.image1).gone();
		taq.id(headerHolder.image2).gone();

		
		return vi;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	

}