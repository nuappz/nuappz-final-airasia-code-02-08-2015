package com.airasia.adapter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airasia.fragment.FragmentCase;
import com.airasia.holder.ConstantHolder;
import com.airasia.holder.ViewHolder;
import com.airasia.mobile.LoginActivity;
import com.airasia.mobile.MainActivity;
import com.airasia.mobile.R;
import com.airasia.mobile.SearchStateActivity;
import com.airasia.model.MemberInfoModel;
import com.airasia.util.ConstantHelper;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GuestListingAdapter extends BaseAdapter{

	
	AQuery aq;

	List<MemberInfoModel> fullItems;
	List<MemberInfoModel> items;
	Activity act;
	ViewHolder.GuestHolder holder;
	ViewHolder.FrenFamilyHolder parentHolder;
	View v;
	LayoutInflater inflater;
	boolean isLogin, isContact = false;
	public int positionExpand = -1;
	
	public GuestListingAdapter(View view, Activity _act, List<MemberInfoModel> _items, boolean _isLogin){
		aq = new AQuery(view);
		this.fullItems = _items;
		this.items = _items;
		this.act = _act;
		this.isLogin = _isLogin;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<MemberInfoModel> getMemberInfos(){
		return this.items;
	}
	
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		v = null;
		EditText fName = null;
		EditText lName = null;
		Object oNameWatch = null , oLNameWatch = null, oMobileWatch = null, oEmailWatch = null;
		v = convertView;
		MemberInfoModel person = items.get(position);
		int type = person.getPsgType();
		if (v == null){
			holder = new ViewHolder.GuestHolder();
			switch(type){
			case -1:
				LogHelper.debug("Position 0 without login");
				v = inflater.inflate(R.layout.item_guest_login, null);
				holder.header = (TextView) v.findViewById(R.id.guest_login_text);
				holder.login = (Button) v.findViewById(R.id.guest_login_btn);
				break;
			default:
				v = inflater.inflate(R.layout.item_guest_adult, null);
				holder.mr = (Button) v.findViewById(R.id.adult_btn_mr);
				holder.ms = (Button) v.findViewById(R.id.adult_btn_ms);
				holder.bigId = (EditText) v.findViewById(R.id.adult_big_id);
				holder.fName = (EditText) v.findViewById(R.id.adult_fname);
				holder.lName = (EditText) v.findViewById(R.id.adult_lname);
				holder.dob = (Button) v.findViewById(R.id.adult_dob);
				holder.nationality = (Button) v.findViewById(R.id.adult_national);
				holder.isContact = (ImageView) v.findViewById(R.id.adult_contact);
				holder.email = (EditText) v.findViewById(R.id.adult_email);
				holder.emailText = (TextView) v.findViewById(R.id.adult_email_text);
				holder.mobile = (EditText) v.findViewById(R.id.adult_mobile);
				holder.tick = (ImageView) v.findViewById(R.id.adult_info_tick); 
				holder.header = (TextView) v.findViewById(R.id.adult_info_text);
				holder.familyBtn = (Button) v.findViewById(R.id.adult_family);
				
				holder.adaultViewContainer = (LinearLayout) v.findViewById(R.id.item_guest_adult_view);
				holder.bigIdContainer = (LinearLayout) v.findViewById(R.id.adult_big_id_container);
				holder.contactContainer = (LinearLayout) v.findViewById(R.id.adult_contact_container);
				holder.contactSelectView = (LinearLayout) v.findViewById(R.id.adult_contact_select_view);
				holder.birthContainer = (LinearLayout) v.findViewById(R.id.birthContainer);
				holder.genderContainer = (LinearLayout) v.findViewById(R.id.adult_gender_selection);
				break;
			}
			v.setTag(holder);
			fName = holder.fName;
			lName = holder.lName;
			
		}else{
			holder = (ViewHolder.GuestHolder) v.getTag();
			fName = holder.fName;
			lName = holder.lName;
			if (fName!=null){
				oNameWatch = fName.getTag();
			}
			if (lName!=null){
				oLNameWatch = lName.getTag();
			}
			if (holder.mobile !=null){
				oMobileWatch = holder.mobile.getTag();
			}
			if (holder.email !=null){
				oEmailWatch = holder.email.getTag();
			}
		}
		
//		final AQuery taq = this.aq.recycle(v);
		AQuery taq = new AQuery (v);
		if (type == MemberInfoModel.HEADER ){
			taq.id(holder.header).text(items.get(position).getfName());
			taq.id(holder.login).getView().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(act, LoginActivity.class);
					Bundle extras = new Bundle();
					extras.putInt("frag_type", 1);
					extras.putInt("frag_id", FragmentCase.FRAG_GUEST_DETAILS);
					i.putExtras(extras);
					act.startActivityForResult(i, MainActivity.LOGIN_REQUEST);
					
				}
			});
		}else {
			
			MyTextWatcher fNameWatcher = new MyTextWatcher(position, fName);
			MyTextWatcher lNameWatcher = new MyTextWatcher(position, lName);
			if (oNameWatch != null){
				LogHelper.debug("Remove Watcher");
				fName.removeTextChangedListener((MyTextWatcher) oNameWatch);
			}
			if (oLNameWatch !=null){
				lName.removeTextChangedListener((MyTextWatcher) oLNameWatch);
			}
 			
			fName.setTag(fNameWatcher);
			lName.setTag(lNameWatcher);
			fName.addTextChangedListener(fNameWatcher);
			lName.addTextChangedListener(lNameWatcher);
			fName.setText(person.getfName());
			lName.setText(person.getlName());
			
			
			if (type == MemberInfoModel.CONTACT){
				taq.id(holder.header).text(act.getString(R.string.guest_contact_header));
			}else if (type == MemberInfoModel.EMERGENCY){
				taq.id(holder.header).text(act.getString(R.string.guest_emergency_header));
			}else{
				taq.id(holder.header).text(act.getString(R.string.guest_header).replace("$NUM$", ""+(person.getPassangerNum()+1))
										+ " (" + person.getPsgTypeString() +") ");
			}
			
			if (type == 1){
				taq.id(holder.bigIdContainer).visible();
				taq.id(holder.bigId).text(""+person.getBigID());
				taq.id(holder.bigId).getEditText().addTextChangedListener(new MyTextWatcher(position, holder.bigId));
			
			}else{
				taq.id(holder.bigIdContainer).gone();
			}
			
			//update Gender
			if (person.getPsgType() == MemberInfoModel.INFANT){
				taq.id(holder.mr).getButton().setBackgroundResource(R.drawable.button_male);
				taq.id(holder.ms).getButton().setBackgroundResource(R.drawable.button_female);
			}else{
				taq.id(holder.ms).getButton().setBackgroundResource(R.drawable.button_mrs);
				taq.id(holder.mr).getButton().setBackgroundResource(R.drawable.button_mr);
			}
			
			if (person.getGenderStr().equalsIgnoreCase("Male") ||
					person.getGenderStr().equalsIgnoreCase("Mr")){
				taq.id(holder.mr).getButton().setEnabled(false);
				taq.id(holder.ms).getButton().setEnabled(true);
			}else{
				taq.id(holder.ms).getButton().setEnabled(false);
				taq.id(holder.mr).getButton().setEnabled(true);
			}
			
			//Fren Family
			if (isLogin){
				if (type != MemberInfoModel.CONTACT 
						&& type != MemberInfoModel.INFANT 
						&& type != MemberInfoModel.EMERGENCY){
					taq.id(holder.familyBtn).visible();
				}else{
					taq.id(holder.familyBtn).gone();
				}
			}else{
				taq.id(holder.familyBtn).gone();
			}
			
			taq.id(holder.mr).getButton().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View _v) {
					// TODO Auto-generated method stub
					_v.setEnabled(false);
					updateMemberGender(position, 0);
				}
			});
			taq.id(holder.ms).getButton().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View _v) {
					// TODO Auto-generated method stub
					_v.setEnabled(false);
//						taq.id(holder.mr).getView().setEnabled(true);
					updateMemberGender(position, 1);
				}
			});
			
			
			if (person.getDob() != null && person.getDob().length()>0){
				String dob = ConstantHelper.changeDateFormat(person.getDob()
										, ConstantHolder.PATTERN_YMD
										, "dd MMMM yyyy");
				taq.id(holder.dob).text(dob);
			}else{
				taq.id(holder.dob).text("");
			}
			
			taq.id(holder.dob).getButton().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Calendar c = Calendar.getInstance();
				    int mYear = c.get(Calendar.YEAR);
				    int mMonth = c.get(Calendar.MONTH);
				    int mDay = c.get(Calendar.DAY_OF_MONTH);
				    System.out.println("the selected " + mDay);
					final Button tv = (Button) v;
				    DatePickerDialog dpd = new DatePickerDialog(act,
				            new OnDateSetListener() {

					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						// TODO Auto-generated method stub
						Calendar mCalender = Calendar.getInstance();
						mCalender.set(year, monthOfYear, dayOfMonth);
						tv.setText(dayOfMonth+" "+
								mCalender.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())+" "+
								year);
						Date d = mCalender.getTime();
						SimpleDateFormat format = new SimpleDateFormat(ConstantHolder.PATTERN_YMD);
						LogHelper.debug(position + " Date Picked " + format.format(d));
						items.get(position).setDob(format.format(d));
						}
					
					}, mYear, mMonth, mDay);
				    dpd.setTitle(R.string.signup_dob);
				    dpd.show();
				}
			});
			
//			SQLhelper db = new SQLhelper(act);
			if (person.getCountry() != null && person.getCountry().length()>0){
//				String countryName = db.getCountryName(person.getCountryCode());
				taq.id(holder.nationality).text(person.getCountry());
				taq.id(holder.nationality).getButton().setTag(person.getCountryCode());
			}else{
				taq.id(holder.nationality).text("");
				taq.id(holder.nationality).getButton().setTag("");
			}
			
			taq.id(holder.nationality).getButton().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent i = new Intent(act, SearchStateActivity.class);
					Bundle extras = new Bundle();
					extras.putInt("type", 1);
					extras.putInt("viewId", position);
					i.putExtras(extras);
					act.startActivityForResult(i, MainActivity.GUEST_NATION);
				}
			});
			
			if (type == 0){
				aq.id(holder.contactContainer).gone();
				aq.id(holder.email).text(person.getUserName());
			}
			
			MyTextWatcher mobileWatcher = new MyTextWatcher(position, holder.mobile);
			MyTextWatcher emailWatcher = new MyTextWatcher(position, holder.email);
			if (oMobileWatch != null){
				LogHelper.debug("Remove Watcher");
				holder.mobile.removeTextChangedListener((MyTextWatcher) oMobileWatch);
			}
			if (oEmailWatch !=null){
				holder.email.removeTextChangedListener((MyTextWatcher) oEmailWatch);
			}
			holder.mobile.setTag(mobileWatcher);
			holder.email.setTag(oEmailWatch);
			taq.id(holder.mobile).getEditText().addTextChangedListener(mobileWatcher);
			taq.id(holder.email).getEditText().addTextChangedListener(emailWatcher);



			if (type ==  MemberInfoModel.EMERGENCY){
				taq.id(holder.emailText).text(R.string.guest_emergency_relation);
			}else{
				taq.id(holder.emailText).text(R.string.login_email);
			}
			
			if (type == 1){
				taq.id(holder.contactSelectView).visible();
				if (person.isContactPerson()){


					taq.id(holder.mobile).getEditText().setText(person.getMobileNo());
					taq.id(holder.email).getEditText().setText(person.getUserName());


					taq.id(holder.mobile).text(person.getMobileNo());
					taq.id(holder.email).text(person.getUserName());

					taq.id(holder.isContact).getView().setActivated(false);
					taq.id(holder.contactContainer).visible();
				}else{
					taq.id(holder.isContact).getView().setActivated(true);
					taq.id(holder.contactContainer).gone();
				}
				
				taq.id(holder.isContact).getView().setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (v.isActivated()){
							v.setActivated(false);
							LogHelper.debug("Enable Contact Person");
							for (int i = 0; i<items.size(); i ++){
								if (position == i){
									items.get(i).setContactPerson(true);
								}else{
									items.get(i).setContactPerson(false);
								}
								isContact = true;
							}
						}else{
							LogHelper.debug("Diasble Contact Person");
							isContact = false;
							v.setActivated(true);
							items.get(position).setContactPerson(false);
						}
						refreshList();
						notifyDataSetChanged();
					}
				});
				taq.id(holder.genderContainer).visible();
				taq.id(holder.birthContainer).visible();
			}else{
				taq.id(holder.contactSelectView).gone();
				if (type == MemberInfoModel.CONTACT || type == MemberInfoModel.EMERGENCY){
					taq.id(holder.birthContainer).gone();
					taq.id(holder.contactContainer).visible();
					
					if (type ==  MemberInfoModel.EMERGENCY){
						taq.id(holder.genderContainer).gone();
					}else{
						if(isContact){
							taq.id(holder.adaultViewContainer).gone();
						}else{
							taq.id(holder.adaultViewContainer).visible();
						}
						taq.id(holder.genderContainer).visible();
					}
				}else{
					taq.id(holder.contactContainer).gone();
				}
			}
			
			
		}
		return v;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
//		return super.getItemViewType(position);
//		if (position!= (getCount()-1)){
//			int type = items.get(position).getPsgType();
//			if (type == 1){
//				return 1;
//			}else if (type == 2){
//				return 2;
//			}else if (type == -1){
//				return -1;
//			}else{
//				return 3;
//			}
//		}else{
//			return 0;
//		}
		if (items.get(position).isHuman()){
			return 1;
		}else{
			return 0;
		}
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		//Maximum 19 different ppl
		return 19;
	}
	

	private class MyTextWatcher implements TextWatcher {
	    private int index;
	    private EditText edittext;
	    
	    public MyTextWatcher(){
	    	
	    }
	    
	    public MyTextWatcher(int index, EditText _editText) { 
	               this.index = index; 
	               this.edittext = _editText;    
        }
	    
	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	    	
	    }
	    
	    public void onTextChanged(CharSequence s, int start, int before, int count) {
	    	LogHelper.debug( index + " Index Text Changed " + s);
	    	if (index >= items.size()){
	    		return;
	    	}
	    	MemberInfoModel mModel = items.get(index);
	    	switch (this.edittext.getId()){
	    	case R.id.adult_big_id:
	    		if (String.valueOf(s).trim().length()>0){
	    			mModel.setBigID(Integer.valueOf(s.toString()));
	    		}else{
	    			mModel.setBigID(0);
	    		}
	    		break;
	    	case R.id.adult_fname:
	    		mModel.setfName(s.toString());
	    		break;
	    	case R.id.adult_lname:
	    		mModel.setlName(s.toString());
	    		break;
	    	case R.id.adult_email:
	    		mModel.setUserName(s.toString());
	    		break;
	    	case R.id.adult_mobile:
	    		mModel.setMobileNo(s.toString());
	    		break;
	    	default:
	    		break;
	    	}
	    }
	    
	    public void afterTextChanged(Editable s) {  
	    	LogHelper.debug("Text Change Detected " + s.toString());
	    	
	    }
	}	
	
	public void updateGuestNation(String _key, String _name, int position){
		items.get(position).setCountryCode(_key);
		items.get(position).setCountry(_name);
		notifyDataSetChanged();
	}
	
	// 0 for Men / Guy / Male
	public void updateMemberGender(int position, int genderType){
		MemberInfoModel mModel = items.get(position);
		if (mModel.getPsgType() == MemberInfoModel.ADULT ||
				mModel.getPsgType() == MemberInfoModel.CHILD ||
				mModel.getPsgType() == MemberInfoModel.INFANT){
			if (genderType == 0){
				mModel.setGenderStr("Male");
			}else{
				mModel.setGenderStr("Female");
			}
		}else if (mModel.getPsgType() == MemberInfoModel.CONTACT){
			if (genderType == 0){
				mModel.setGenderStr("Ms");
			}else{
				mModel.setGenderStr("Mr");
			}
		}
		notifyDataSetChanged();
	}
	
	public void refreshList(){
		if (isContact){
			this.items = new ArrayList<MemberInfoModel>();
			for (int i = 0; i < fullItems.size();i ++){
				if (fullItems.get(i).getPsgType() !=MemberInfoModel.CONTACT){
					MemberInfoModel mPerson = fullItems.get(i);
					items.add(mPerson);
				}
			}
		}else{
			this.items = this.fullItems;
		}
	}

	public void updateLoginStatus(boolean _isLogin){
		this.isLogin = _isLogin;
		notifyDataSetChanged();
	}
	
}
