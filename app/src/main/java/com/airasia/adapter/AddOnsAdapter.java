package com.airasia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airasia.mobile.R;
import com.airasia.mobile.SeatMapActivity;
import com.airasia.model.AddOnsCardViewModel;

import java.util.ArrayList;

/**
 * Created by NUappz-5 on 7/29/2015.
 */
public class AddOnsAdapter extends RecyclerView
        .Adapter<AddOnsAdapter
        .DataObjectHolder> {
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private ArrayList<AddOnsCardViewModel> mDataset;
    private static MyClickListener myClickListener;
    static Context ctxt;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {
        TextView add_ons_list_name, add_ons_desc, amount;
        ImageView tick, arrow;
        LinearLayout addOnsLayout;

        //Terry testing bitbucket

        public DataObjectHolder(View itemView) {
            super(itemView);
            add_ons_list_name = (TextView) itemView.findViewById(R.id.tv_AddOnList);
            add_ons_desc = (TextView) itemView.findViewById(R.id.tv_AddOnDesc);
            amount = (TextView) itemView.findViewById(R.id.tv_AddOnListAmount);
            tick = (ImageView) itemView.findViewById(R.id.addOn_tick);
            arrow = (ImageView) itemView.findViewById(R.id.addOnsArrow);
            addOnsLayout = (LinearLayout) itemView.findViewById(R.id.myrLyt);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
            if (getAdapterPosition() == 0) {
                add_ons_list_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.stars1, 0);
                add_ons_desc.setVisibility(View.GONE);
                amount.setVisibility(View.VISIBLE);
                tick.setVisibility(View.VISIBLE);
                addOnsLayout.setVisibility(View.VISIBLE);
            }
            if (getAdapterPosition() == 1) {
                add_ons_desc.setVisibility(View.GONE);
                amount.setVisibility(View.VISIBLE);
                add_ons_list_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.stars1, 0);
                tick.setVisibility(View.VISIBLE);
                addOnsLayout.setVisibility(View.VISIBLE);
            }
            if (getAdapterPosition() == 2) {
                add_ons_desc.setVisibility(View.VISIBLE);
                Intent intent = new Intent(ctxt, SeatMapActivity.class);
                ctxt.startActivity(intent);

            }

        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener) {
        this.myClickListener = myClickListener;

    }

    public AddOnsAdapter(Context context, ArrayList<AddOnsCardViewModel> myDataset) {
        mDataset = myDataset;
        this.ctxt = context;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_add_ons_card_view_row, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
        holder.add_ons_list_name.setText(mDataset.get(position).getAddOnListName());
    }

    public void addItem(AddOnsCardViewModel dataObj, int index) {
        mDataset.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}