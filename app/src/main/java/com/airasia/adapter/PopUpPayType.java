package com.airasia.adapter;

/**
 * Created by appcell on 7/30/15.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.model.PayMethodModel;
import com.airasia.model.PaymentOptionModel;
import com.androidquery.AQuery;

import java.util.List;

public class PopUpPayType extends BaseAdapter {

    LayoutInflater inflator;
    List<PaymentOptionModel> items;
    AQuery aq;
    ViewHolder.FrenFamilyHolder holder;


    public PopUpPayType(View v, Activity _act, List<PaymentOptionModel> _items){
        this.items = _items;
        inflator = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        aq = new AQuery(v);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return items.get(position);

    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View vi = convertView;
        if (vi == null){
            holder = new ViewHolder.FrenFamilyHolder();
            vi = inflator.inflate(android.R.layout.simple_list_item_1, null);
            holder.fName = (TextView) vi.findViewById(android.R.id.text1);
            vi.setTag(holder);
        }else{
            holder = (ViewHolder.FrenFamilyHolder) vi.getTag();
        }

        AQuery taq = this.aq.recycle(vi);

        taq.id(holder.fName).text(items.get(position).getChannel());

        return vi;
    }




}
