package com.airasia.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.JourneyModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SSRContainer;
import com.airasia.model.SSRItemModel;
import com.airasia.model.SSRMainModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.List;

public class SSRBaggageAdapter  extends BaseAdapter {

	SSRMainModel item;
	LayoutInflater inflater;
	BookingInfoModel booking;
	ViewHolder.BaggageHolder holder;
	List<SSRItemModel> departBaggage, returnBaggage;
	AQuery aq;
	Activity act;
	
	public SSRBaggageAdapter(SSRMainModel _item, BookingInfoModel _booking, Activity _act, View view){
		this.item = _item;
		inflater = (LayoutInflater) _act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		aq = new AQuery(view);
		this.booking = _booking;
		this.act = _act;
		this.departBaggage = _item.getDepartContainer().get(0).getBaggageList();
		if (_item.getReturnContainer()!=null && _item.getReturnContainer().size()>0){
			this.returnBaggage = _item.getReturnContainer().get(0).getBaggageList();
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return item.getPassangers().size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int pos, View view, ViewGroup arg2) {
		// TODO Auto-generated method stub
		View vi = view;
		if(vi == null){
			holder = new ViewHolder.BaggageHolder();
			vi = inflater.inflate(R.layout.item_baggage, null);
			holder.departKg = (TextView) vi.findViewById(R.id.item_baggage_depart_kg);
			holder.returnKg = (TextView) vi.findViewById(R.id.item_baggage_return_kg);
			holder.personName = (TextView) vi.findViewById(R.id.item_baggage_psg_name);
			holder.currency = (TextView) vi.findViewById(R.id.item_baggage_currency);
			holder.dollar = (TextView) vi.findViewById(R.id.item_baggage_dollar);
			holder.sens = (TextView) vi.findViewById(R.id.item_baggage_sen);
			holder.departJourney = (LinearLayout) vi.findViewById(R.id.item_baggage_depart);
			holder.returnJourney = (LinearLayout) vi.findViewById(R.id.item_baggage_return);
			holder.returnContainer = (LinearLayout) vi.findViewById(R.id.item_baggage_return_view);
			holder.departIncrease = (Button) vi.findViewById(R.id.item_baggage_depart_increase);
			holder.departDecrease = (Button) vi.findViewById(R.id.item_baggage_depart_decrease);
			holder.returnIncrease = (Button) vi.findViewById(R.id.item_baggage_return_increase);
			holder.returnDecrease = (Button) vi.findViewById(R.id.item_baggage_return_decrease);
			vi.setTag(holder);
		}else{
			holder = (ViewHolder.BaggageHolder) vi.getTag();
		}
		
		AQuery taq = this.aq.recycle(vi);
		
		MemberInfoModel person = item.getPassangers().get(pos);
		taq.id(holder.personName).text(person.getFullName());
		
		//Depart
		LinearLayout flightLayout = holder.departJourney;
		flightLayout.removeAllViews();
		
		List<JourneyModel> mJour = booking.getDepartJourney();
		for (int i = 0; i < mJour.size(); i++){
			View childView = inflater.inflate(R.layout.item_ssr_station, null);
			if(i==0){
				ImageView img = (ImageView) childView.findViewById(R.id.item_cont_icon);
				img.setVisibility(View.GONE);
			}
			TextView flightName = (TextView) childView.findViewById(R.id.item_station_name);
			flightName.setText(mJour.get(i).getName());
			flightLayout.addView(childView);
		}
		
		List<SSRItemModel> bList = person.getBaggage(true);
		String description="";
		if (bList!=null){
			description = bList.get(0).getDescripion();
		}else{
			description = departBaggage.get(0).getDescripion();
		}
		LogHelper.debug("Deactip tion " + description);
		if(description!=null && description.length()>5){
			int index = description.indexOf("kg");
			description = description.substring(index-3).trim();
		}
		int dBP = person.getdBaggagePos();
		taq.id(holder.departKg).text(description);
		

		if(dBP == departBaggage.size()-1){
			taq.id(holder.departIncrease).getView().setEnabled(false);
		}else{
			taq.id(holder.departIncrease).getView().setEnabled(true);
			if ((person.getBaggage(true)== null || person.getBaggage(true).size()<=0)
					&& departBaggage != null
					&& departBaggage.size()>0){
				SSRItemModel baggageSelected = departBaggage.get(0);
				person.setdBaggagePos(0);
				List<SSRItemModel> baglist = new ArrayList<SSRItemModel>();
				for (int i = 0; i < item.getDepartContainer().size(); i++){
					SSRContainer container = item.getDepartContainer().get(i);
					SSRItemModel mSSR = container.getItemBaseKey(baggageSelected.getSsrCode());
					if (mSSR != null){
						baglist.add(mSSR);
					}
				}
				if (baglist != null && baglist.size()>0){
					person.setItemDepartBaggage(baglist);
				}
			}
		}
		
		if (dBP == 0){
			taq.id(holder.departDecrease).getView().setEnabled(false);
		}else{
			taq.id(holder.departDecrease).getView().setEnabled(true);
		}
		
		taq.id(holder.departIncrease).getView().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MemberInfoModel mSelectedPerson = item.getPassangers().get(pos);
				int bPos = mSelectedPerson.getdBaggagePos();
				SSRItemModel baggageSelected = departBaggage.get(bPos+1);
				mSelectedPerson.increseDBaggagePos();
				List<SSRItemModel> baglist = new ArrayList<SSRItemModel>();
				for (int i = 0; i < item.getDepartContainer().size(); i++){
					SSRContainer container = item.getDepartContainer().get(i);
					SSRItemModel mSSR = container.getItemBaseKey(baggageSelected.getSsrCode());
					if (mSSR != null){
						baglist.add(mSSR);
					}
				}
				if (baglist != null && baglist.size()>0){
					mSelectedPerson.setItemDepartBaggage(baglist);
				}else{
					mSelectedPerson.setItemDepartBaggage(new ArrayList<SSRItemModel>());
				}
				notifyDataSetChanged();
			}
		});
		taq.id(holder.departDecrease).getView().setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SSRItemModel baggageSelected = departBaggage.get(item.getPassangers().get(pos).getdBaggagePos()-1);
				MemberInfoModel mSelectedPerson = item.getPassangers().get(pos);
				mSelectedPerson.decreseDBaggagePos();
				List<SSRItemModel> baglist = new ArrayList<SSRItemModel>();
				for (int i = 0; i < item.getDepartContainer().size(); i++){
					SSRContainer container = item.getDepartContainer().get(i);
					SSRItemModel mSSR = container.getItemBaseKey(baggageSelected.getSsrCode());
					if (mSSR != null){
						baglist.add(mSSR);
					}
				}
				if (baglist != null && baglist.size()>0){
					mSelectedPerson.setItemDepartBaggage(baglist);
				}
				notifyDataSetChanged();
			}
		});
		
		
		//Return
		List<JourneyModel> returnJour = booking.getReturnJourney();
		flightLayout = holder.returnJourney;
		flightLayout.removeAllViews();
		if(returnJour != null && returnJour.size()>0){
			taq.id(holder.returnContainer).visible();
			for (int i = 0; i < returnJour.size(); i++){
				View childView = inflater.inflate(R.layout.item_ssr_station, null);
				if(i==0){
					ImageView img = (ImageView) childView.findViewById(R.id.item_cont_icon);
					img.setVisibility(View.GONE);
				}
				TextView flightName = (TextView) childView.findViewById(R.id.item_station_name);
				flightName.setText(returnJour.get(i).getName());
				flightLayout.addView(childView);
			}
			
			
			List<SSRItemModel> personalReturn = person.getBaggage(false);
			int rBP = person.getrBaggagePos();

			String returnDesc="";
			if (personalReturn!=null && personalReturn.size()>0){
				returnDesc = personalReturn.get(0).getDescripion();
			}else{
				returnDesc = returnBaggage.get(0).getDescripion();
			}
			if(returnDesc!=null && returnDesc.length()>5){
				int index = returnDesc.indexOf("kg");
				returnDesc = returnDesc.substring(index-3).trim();
			}
			taq.id(holder.returnKg).text(returnDesc);
			
			
			
			if(rBP == returnBaggage.size()-1){
				taq.id(holder.returnIncrease).getView().setEnabled(false);
			}else{
				taq.id(holder.returnIncrease).getView().setEnabled(true);
				if ((person.getBaggage(false)== null || person.getBaggage(false).size()<=0)
						&& returnBaggage != null
						&& returnBaggage.size()>0){
					SSRItemModel baggageSelected = returnBaggage.get(0);
					person.setrBaggagePos(0);
					List<SSRItemModel> baglist = new ArrayList<SSRItemModel>();
					for (int i = 0; i < item.getReturnContainer().size(); i++){
						SSRContainer container = item.getReturnContainer().get(i);
						SSRItemModel mSSR = container.getItemBaseKey(baggageSelected.getSsrCode());
						if (mSSR != null){
							baglist.add(mSSR);
						}
					}
					if (baglist != null && baglist.size()>0){
						person.setItemReturnBaggage(baglist);
					}
				}
			}
			
			if (rBP == 0){
				taq.id(holder.returnDecrease).getView().setEnabled(false);
			}else{
				taq.id(holder.returnDecrease).getView().setEnabled(true);
			}
			
			taq.id(holder.returnIncrease).getView().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SSRItemModel baggageSelected = returnBaggage.get(item.getPassangers().get(pos).getrBaggagePos()+1);
					MemberInfoModel mSelectedPerson = booking.getPassengerInfo().get(pos);
					mSelectedPerson.increseRBaggagePos();
					List<SSRItemModel> baglist = new ArrayList<SSRItemModel>();
					for (int i = 0; i < item.getReturnContainer().size(); i++){
						SSRContainer container = item.getReturnContainer().get(i);
						SSRItemModel mSSR = container.getItemBaseKey(baggageSelected.getSsrCode());
						if (mSSR != null){
							baglist.add(mSSR);
						}
					}
					if (baglist != null && baglist.size()>0){
						mSelectedPerson.setItemReturnBaggage(baglist);
					}
					notifyDataSetChanged();
				}
			});
			taq.id(holder.returnDecrease).getView().setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SSRItemModel baggageSelected = returnBaggage.get(item.getPassangers().get(pos).getrBaggagePos()-1);
					MemberInfoModel mSelectedPerson = booking.getPassengerInfo().get(pos);
					mSelectedPerson.decreseRBaggagePos();
					List<SSRItemModel> baglist = new ArrayList<SSRItemModel>();
					for (int i = 0; i < item.getReturnContainer().size(); i++){
						SSRContainer container = item.getReturnContainer().get(i);
						SSRItemModel mSSR = container.getItemBaseKey(baggageSelected.getSsrCode());
						if (mSSR != null){
							baglist.add(mSSR);
						}
					}
					if (baglist != null && baglist.size()>0){
						mSelectedPerson.setItemReturnBaggage(baglist);
					}
					
					notifyDataSetChanged();
				}
			});
			
		}else{
			taq.id(holder.returnContainer).gone();
		}
		
		//update Price

		taq.id(holder.currency).text(booking.getCurrencyCode());
		double total =  person.getTotalBaggageCharge();
		String[] splitTotal = String.valueOf(total).split("\\.");
		taq.id(holder.dollar).text(splitTotal[0]);
		taq.id(holder.sens).text(splitTotal[1]);
		
		return vi;
	}
	
	public void increaseBaggage(){
		
	}

	
}