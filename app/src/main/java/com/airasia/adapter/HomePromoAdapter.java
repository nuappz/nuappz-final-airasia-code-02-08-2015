package com.airasia.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.airasia.holder.ViewHolder;
import com.airasia.mobile.R;
import com.airasia.model.BannerModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;

import java.util.List;

public class HomePromoAdapter extends PagerAdapter {

	ViewHolder.HomePagerHolder holder;
	AQuery aq;
	Activity act;
	LayoutInflater inflater;
	List<BannerModel> bannerList;
	
	public HomePromoAdapter(View view, Activity _act, List<BannerModel> _bannerList){
		aq = new AQuery(view);
		this.act = _act;
		this.bannerList = _bannerList;
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if (bannerList == null ){
			return 0;
		}
		return bannerList.size();
	}
	
	public void updateBanners(List<BannerModel> banners){
		this.bannerList = banners;
		this.notifyDataSetChanged();
		
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		// TODO Auto-generated method stub
		inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		ImageView img;
		View vi = inflater.inflate(R.layout.fragment_home_pager, container, false);
//		img = (ImageView) vi.findViewById(R.id.home_pager_image);
//		img.setImageResource(R.drawable.bg_sso);
		AQuery taq = new AQuery(vi);

		String url = bannerList.get(position).getUrl().replace("https", "http");

		LogHelper.debug("Banner url " + url);
		
		taq.id(R.id.home_pager_image)
		.visible()
//		.progress(R.id.hlist_channel_home_loader)
		.image(url , false,
				true, 0,
				0,
				new BitmapAjaxCallback() {
					@Override
					public void callback(String url,
							ImageView iv, Bitmap bm,
							AjaxStatus status) {

						if (bm != null) {
							iv.setImageBitmap(bm);
						} else {
//							iv.setImageResource(R.drawable.bg_sso);
						}
					}
				});


		((ViewPager) container).addView(vi);
		return vi;
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		// TODO Auto-generated method stub
		return view == ((RelativeLayout) object);
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((RelativeLayout) object);

	}

}
