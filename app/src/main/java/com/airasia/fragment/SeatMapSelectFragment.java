package com.airasia.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.airasia.dialog.SeatMapAssignDialog;
import com.airasia.dialog.SeatMapRejectDialog;
import com.airasia.holder.PrefsHolder;
import com.airasia.mobile.MainActivity;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SeatMapModel;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




/**
 * Created by NUappz1 on 7/26/2015.
 */
public class SeatMapSelectFragment extends
        Fragment implements SeatMapAssignDialog.finishCallBack {//implements OnToggledListener {



    /* GridLayoutView[] myViews;

     GridLayout myGridLayout;*/
    SeatMapAssignDialog dialog;
    SeatMapRejectDialog dialog1;

    String journeyKey = "0";
    String segmentKey = "0";

    //int status[][];
    int i = 0;
    int j = 0;
    ScrollView scrollView;

    SharedPreferences prefs;

    ProgressDialog progress;

    String equipmentType;

    BookingInfoModel booking;

    Map<String,SeatMapModel> seatMapList;

    TableLayout seatMapTableLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        journeyKey = getArguments().getString("journey");
        segmentKey = getArguments().getString("segment");
        booking = (BookingInfoModel) getArguments().getSerializable("flight");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_seatmapselectpick, container, false);

        scrollView = (ScrollView) v.findViewById(R.id.scrollView_seatMap);

        prefs = getActivity().getSharedPreferences(PrefsHolder.PREFS, Context.MODE_PRIVATE);

        dialog = new SeatMapAssignDialog(getActivity());
        dialog.callback = this;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog1 = new SeatMapRejectDialog(getActivity());
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);

        connGetSeatMap();




        return v;
    }

    private TableLayout createTableLayout(List<String> rv, List<String> cv) {
        // 1) Create a tableLayout and its params
        TableLayout.LayoutParams tableLayoutParams = new TableLayout.LayoutParams();
        TableLayout tableLayout = new TableLayout(getActivity());
        //tableLayout.setBackgroundColor(Color.WHITE);
        tableLayout.setBackgroundColor(getResources().getColor(R.color.bg_color));


        int measuredWidth = 0;
        //int measuredHeight = 0;
        WindowManager w = getActivity().getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            w.getDefaultDisplay().getSize(size);
            measuredWidth = size.x;
            //measuredHeight = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            measuredWidth = d.getWidth();
            //measuredHeight = d.getHeight();
        }

        int cellWidth = (measuredWidth-4*cv.size()-getActivity().getResources().getDimensionPixelOffset(R.dimen.seat_map_scrollViewPadding)*2)/cv.size();

        for (/*int */i = 0; i < rv.size(); i++) {
            // 3) create tableRow
            TableRow tableRow = new TableRow(getActivity());
            for (/*int */j = 0; j < cv.size(); j++) {
                // 4) create textView



                final View view = getActivity().getLayoutInflater().inflate(R.layout.custom_seat_map_cell, null);

                //RelativeLayout layout = (RelativeLayout) view.findViewById(R.id.seat_map_cell);


                //final TextView textView = new TextView(getActivity());
                //  textView.setText(String.valueOf(j));
                // textView.setBackgroundColor(Color.WHITE);
                //view.setBackgroundColor(getResources().getColor(R.color.bg_color));
                //textView.setBackgroundDrawable(getResources().getDrawable(R.drawable.user_gray));
                //String s1 = Integer.toString(i);
                //String s2 = Integer.toString(j);

                TextView textView = (TextView) view.findViewById(R.id.seat_map_text);
                textView.setText("");
                ImageView imageView = (ImageView)view.findViewById(R.id.seat_map_image);
                imageView.setVisibility(View.INVISIBLE);
                View bgView = view.findViewById(R.id.seat_map_cell);

                int width = cellWidth;

                if (i == 0 && j == 0) {
                }
                else
                {
                    String colText = cv.get(j);
                    String rowText = rv.get(i);

                    if (colText.equals("-") || i == 0 || j ==0) {

                        if (i == 0) {

                            if (colText.equals("-"))
                            {
                                textView.setText("");
                            }
                            else
                            {
                                textView.setText(colText);
                            }

                        } else if (j == 0) {

                            if (rowText.equals("-"))
                            {
                                textView.setText("");
                                width = width/2;
                            }
                            else
                            {
                                textView.setText(rowText);
                            }

                        }
                        else
                        {
                            width = width/2;
                        }
                    }
                    else {

                        String s3 = rowText + colText;

                        if (seatMapList!=null)
                        {
                            SeatMapModel model = seatMapList.get(s3);
                            if (model!=null)
                            {
                                LogHelper.debug("number = " + s3);

                                bgView.setBackgroundColor(getResources().getColor(model.seatColor()));

                                int icon = model.seatIcon();
                                if (icon!=0)
                                {
                                    imageView.setImageResource(icon);
                                    imageView.setVisibility(View.VISIBLE);
                                }

                                if (!model.isOccupied)
                                {
                                    bgView.setTag(s3);
                                    bgView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            LogHelper.debug("seat presssed = "+v.getTag());


                                            dialog.show();

                                            SeatMapModel model = seatMapList.get(v.getTag());
                                            dialog.resetList(model, booking.getMemberList());


                                        }
                                    });
                                }

                            }
                        }




//                            status[i - 1][j - 1] = 0;

                        }
                }

                // 2) create tableRow params
                TableRow.LayoutParams tableRowParams = new TableRow.LayoutParams(width
                        ,
                        cellWidth);


                tableRowParams.setMargins(2, 2, 2, 2);
                tableRowParams.weight = 1;

                // 5) add textView to tableRow
                tableRow.addView(view, tableRowParams);

            }

            // 6) add tableRow to tableLayout
            tableLayout.addView(tableRow, tableLayoutParams);
        }

     return tableLayout;
    }


    private void connGetSeatMap(){
        RequestParams params = new RequestParams();
        params.put("journey_key", journeyKey);
        params.put("segment_key", segmentKey);



        params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));

        CustomHttpClient.postWithHash(CustomHttpClient.GET_SEAT_MAP, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart()
            {
                progress = ProgressDialog.show(SeatMapSelectFragment.this.getActivity(), "", SeatMapSelectFragment.this.getResources().getString(R.string.loading));
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // TODO Auto-generated method stub
                String response = new String(responseBody);
                LogHelper.debug("seat map Response " + response);
                try {
                    int responseCode = JSonHelper.GetResponseStatus(response);
                    if (responseCode == JSonHelper.SUCCESS_REPONSE) {
                        JSONObject json = new JSONObject(response);
                        JSONObject data = json.getJSONObject("data");

                        JSONObject seat_data = data.getJSONObject("seat_data");

                        equipmentType = seat_data.getString("EquipmentType");

                        JSONArray seatInfoArray = seat_data.getJSONArray("SeatInfo");

                        if (seatMapList!=null)
                        {
                            seatMapList.clear();
                        }
                        else
                        {
                            seatMapList = new HashMap<String, SeatMapModel>();
                        }


                        for (int i = 0; i < seatInfoArray.length(); i++){
                            JSONObject childObj = seatInfoArray.getJSONObject(i);


                            SeatMapModel model = new SeatMapModel();
                            model.seatType = childObj.getInt("SeatAttribute");
                            model.seatDesignator = childObj.getString("SeatDesignator");

                            LogHelper.debug("found seat = "+model.seatDesignator);
                            model.isOccupied = !childObj.getString("Assignable").equals("true");
                            if (!model.isOccupied)
                            {
                                if (!childObj.getString("SeatAvailability").equals("Open")) {
                                    model.isOccupied = true;
                                }
                            }

                            model.fare = childObj.getDouble("SeatFee");

                            seatMapList.put(model.seatDesignator,model);
                        }

                        resetView();

                    } else {
                        String errorMsg = JSonHelper.GetResponseMessage(response);
                        MainActivity.showErrorMessage(SeatMapSelectFragment.this.getActivity(), errorMsg);
                    }
                } catch (Exception e) {

                    MainActivity.showErrorMessage(SeatMapSelectFragment.this.getActivity(), e.getLocalizedMessage());
                }
                finally {

                    if (progress!=null)
                    {
                        progress.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] responseBody, Throwable error) {
                // TODO Auto-generated method stub
                if (progress!=null)
                {
                    progress.dismiss();
                }
            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub
                if (progress != null && progress.isShowing()) {
                    progress.dismiss();
                }
                super.onFinish();
            }


        });
    }

    void resetView()
    {
        List<String> column = new ArrayList<String>();
        column.add("-");
        List<String> rowList = new ArrayList<String>();
        rowList.add("-");

        if(equipmentType.equals("330"))
        {
            if (seatMapList.size()>50)
            {
                column.add("A");
                column.add("B");
                column.add("C");
                column.add("-");
                column.add("D");
                column.add("F");
                column.add("G");
                column.add("-");
                column.add("H");
                column.add("J");
                column.add("K");



                for (int i = 7; i<=51;i++)
                {
                    if (i!=13) {
                        rowList.add(String.valueOf(i));
                    }
                    if(i==14 || i==33)
                    {
                        rowList.add("-");
                    }
                }
            }
            else
            {
                column.add("A");
                column.add("-");
                column.add("C");
                column.add("-");
                column.add("D");
                column.add("-");
                column.add("G");
                column.add("-");
                column.add("H");
                column.add("-");
                column.add("K");



                rowList.add(String.valueOf(1));
                rowList.add("-");
                rowList.add(String.valueOf(2));
            }

        }
        else
        {
            column.add("A");
            column.add("B");
            column.add("C");
            column.add("-");
            column.add("D");
            column.add("E");
            column.add("F");



            for (int i = 1; i<=31;i++)
            {
                if (i!=13) {
                    rowList.add(String.valueOf(i));
                }
                if(i==11 || i==12 || i==31)
                {
                    rowList.add("-");
                }
            }
        }
        //seatmap_tooltip

        scrollView.removeAllViews();

        seatMapTableLayout = createTableLayout(rowList, column);
        scrollView.addView(seatMapTableLayout);

    }


    @Override
    public void passengerDidPressed(MemberInfoModel obj,SeatMapModel model,boolean assign) {

        connAssignSeatMap(obj,model,assign);
    }

    private void connAssignSeatMap(final MemberInfoModel obj ,final SeatMapModel model,boolean assign){
        RequestParams params = new RequestParams();
        params.put("journey_key", journeyKey);
        params.put("segment_key", segmentKey);

        params.put("request_type", assign?"0":"1");



        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("PassengerNumber", obj.getPassangerNum());
            jsonObj.put("SeatDesignator", model.seatDesignator);
            jsonObj.put("SeatAttribute", String.valueOf(model.infoType));

            params.put("passenger_data", jsonObj.toString());
        }catch(JSONException e)
        {

        }

        params.put("EquipmentType", this.equipmentType);


        params.put("userSession", prefs.getString(PrefsHolder.SESSION_TICKET, ""));

        CustomHttpClient.postWithHash(CustomHttpClient.ASSIGN_SEAT_MAP, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                progress = ProgressDialog.show(SeatMapSelectFragment.this.getActivity(), "", SeatMapSelectFragment.this.getResources().getString(R.string.loading));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                // TODO Auto-generated method stub
                String response = new String(responseBody);
                LogHelper.debug("seat map Response " + response);
                try {
                    int responseCode = JSonHelper.GetResponseStatus(response);
                    if (responseCode == JSonHelper.SUCCESS_REPONSE) {



                        for (Map.Entry<String, SeatMapModel> entry : seatMapList.entrySet()) {
                            String key = entry.getKey();
                            SeatMapModel value = entry.getValue();

                            if (value.passangerNumberAssigned == obj.getPassangerNum())
                            {
                                value.passangerNumberAssigned = -1;
                                seatMapList.put(key,value);
                                break;
                            }
                        }

                        model.passangerNumberAssigned = obj.getPassangerNum();
                        seatMapList.put(model.seatDesignator, model);

                        resetView();

                    } else {
                        String errorMsg = JSonHelper.GetResponseMessage(response);
                        MainActivity.showErrorMessage(SeatMapSelectFragment.this.getActivity(), errorMsg);
                    }
                } catch (Exception e) {

                    MainActivity.showErrorMessage(SeatMapSelectFragment.this.getActivity(), e.getLocalizedMessage());
                } finally {

                    if (progress != null) {
                        progress.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  byte[] responseBody, Throwable error) {
                // TODO Auto-generated method stub
                if (progress != null) {
                    progress.dismiss();
                }
            }

            @Override
            public void onFinish() {
                // TODO Auto-generated method stub
                if (progress != null && progress.isShowing()) {
                    progress.dismiss();
                }
                super.onFinish();
            }


        });
    }
}



