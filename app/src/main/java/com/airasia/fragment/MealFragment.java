package com.airasia.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupWindow;

import com.airasia.adapter.MealListingAdapter;
import com.airasia.holder.ConstantHolder;
import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SSRContainer;
import com.airasia.model.SSRItemModel;
import com.airasia.util.LogHelper;
import com.androidquery.AQuery;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

public class MealFragment extends Fragment {

    AQuery aq;
    Handler handler;
    int position;
    ProgressDialog progress;
    //	List<SSRItemModel> list;
    SSRContainer item;
    MemberInfoModel person;
    BookingInfoModel booking;

    public static MealFragment newInstance(SSRContainer _item,
                                           BookingInfoModel _booking,
                                           int _position,
                                           MemberInfoModel _person) {
        MealFragment f = new MealFragment();
//		f.list = _list;
        f.item = _item;
        f.booking = _booking;
        f.position = _position;
        f.person = _person;
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View v = inflater.inflate(R.layout.fragment_meal_list, null);
        if (handler == null) {
            handler = new Handler();
        }

        aq = new AQuery(v);

        handler.postDelayed(viewUpdate, 300);

        return v;
    }

    Runnable viewUpdate = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            setupView();
            updateMealItem();
        }
    };

    public void updateMealItem() {
        List<SSRItemModel> mealList = person.getMealItem(item.isDepart(), item.getMealSegPos());

        if (mealList != null && mealList.size() > 0) {
            String url = ConstantHolder.BASE_URL_HTTP + mealList.get(0).getImage();
            aq.id(R.id.meal_meal1).image(url, false, true);

            if (mealList.size() > 1) {
                url = ConstantHolder.BASE_URL_HTTP + mealList.get(1).getImage();
                aq.id(R.id.meal_meal2).image(url, false, true);
            }else{
                aq.id(R.id.meal_meal2).image(R.drawable.icon_food);
            }
        } else {
            aq.id(R.id.meal_meal1).image(R.drawable.icon_food);
            aq.id(R.id.meal_meal2).image(R.drawable.icon_food);
        }
    }

    public void setupView() {
        aq.id(R.id.meal_arrival).text(item.getArrivalCode());
        aq.id(R.id.meal_depart).text(item.getDepartCode());

        MealListingAdapter mAdapter = new MealListingAdapter(item,
                getActivity(),
                aq.id(R.id.meal_listing).getView(),
                booking,
                person);
        aq.id(R.id.meal_listing).getListView().setAdapter(mAdapter);
        aq.id(R.id.meal_listing).getListView().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                // TODO Auto-generated method stub
                MealListingAdapter tempAdapter = (MealListingAdapter) arg0.getAdapter();
                SSRItemModel meal = tempAdapter.getItem(pos);

                List<SSRItemModel> mealList = person.getMealItem(item.isDepart(), item.getMealSegPos());
                if (mealList != null) {
                    for (int i = 0; i < mealList.size(); i++) {
                        SSRItemModel tempMeal = mealList.get(i);
                        if (tempMeal != null &&
                                tempMeal.getSsrCode() != null &&
                                tempMeal.getSsrCode().equals(meal.getSsrCode())) {
                            person.removeMeal(meal.getSsrCode(), item.isDepart(), item.getMealSegPos());
                            tempAdapter.notifyDataSetChanged();
                            updateMealItem();
                            return;
                        }
                    }
                }
                if (mealList != null && mealList.size() >= 2) {
                    return;
                }

//				suggestionPopup(getActivity(), meal);
                person.addMeal(meal, item.isDepart(), item.getMealSegPos());
                updateMealItem();
                tempAdapter.notifyDataSetChanged();
            }
        });

    }

    public static PopupWindow suggestionPopup(Activity act, SSRItemModel _item) {

        // initialize a pop up window type
        PopupWindow popupWindow = new PopupWindow(act);
        LayoutInflater layoutInflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mainLayout = layoutInflater.inflate(R.layout.pop_meal_item, null);

        // some other visual settings
        popupWindow.setContentView(mainLayout);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        AQuery suggestionAQ = new AQuery(mainLayout);
        suggestionAQ.id(R.id.pop_meal_name).text(_item.getDescripion());
        String url = ConstantHolder.BASE_URL + _item.getImage();
        LogHelper.debug("Photo url = " + url);
        ImageLoader.getInstance().displayImage(url, suggestionAQ.id(R.id.pop_meal_image).getImageView());

        popupWindow.showAtLocation(mainLayout, Gravity.CENTER, 0, 0);

        return popupWindow;
    }

    public void setPersonSelect(MemberInfoModel _person) {
        this.person = _person;
        MealListingAdapter mAdapter = (MealListingAdapter) aq.id(R.id.meal_listing).getListView().getAdapter();
        mAdapter.updatePerson(this.person);
    }

}