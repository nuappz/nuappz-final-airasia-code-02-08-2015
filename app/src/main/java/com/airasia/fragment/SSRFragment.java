package com.airasia.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.SSRMainModel;
import com.androidquery.AQuery;

public class SSRFragment extends Fragment{

	int type = 0;
	AQuery aq;
	BookingInfoModel booking;
	SSRMainModel ssrInfo;
	ViewPager pager;
	Handler handler;
	
	
	public static SSRFragment newInstance(int _type, SSRMainModel _ssrInfo) {
		SSRFragment f = new SSRFragment();
		f.type = _type;
		f.ssrInfo = _ssrInfo;
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			 ViewGroup container,  Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view;
		
		switch(type){
		default:
			view = inflater.inflate(R.layout.fragment_ssr_list, null);
			break;
		}
		
		aq = new AQuery(view);
		
		setupView();
		
//		if (handler == null){
//			handler = new Handler();
//		}
//		
//		handler.postDelayed(viewUpdate, 200);
		return view;
	}
	
	Runnable viewUpdate = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			setupView();
		}
	};
	
	public void setupView(){
		switch(type){
		default:
			if (!ssrInfo.isHasInsurance()){
				aq.id(R.id.ssr_insurance_view).clickable(false);
				aq.id(R.id.ssr_insurance_arrow).gone();
			}else{
				aq.id(R.id.ssr_insurance_view).clickable(true);
				aq.id(R.id.ssr_insurance_arrow).visible();
			}
		break;
		}
	}
	
	
	
}
