package com.airasia.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airasia.layout.FontableEditText;
import com.airasia.layout.FontableTextView;
import com.airasia.mobile.R;
import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by NUappz-5 on 7/27/2015.
 */
public class PaymentItineraryFragment extends Fragment {
    AQuery aq;
    int type;
    private FontableTextView tv_resendMail;
    FontableEditText tv_email;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static PaymentItineraryFragment newInstance(int _type) {
        PaymentItineraryFragment f = new PaymentItineraryFragment();
        f.type = _type;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.fragment_payment_itinerary, null);
        tv_resendMail = (FontableTextView) view.findViewById(R.id.itinerary_send_mail_link);
        aq = new AQuery(view);

        tv_resendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog dialog = null;
                dialog = new MaterialDialog.Builder(getActivity())
                        .title(R.string.itinerary_title)
                        .customView(R.layout.custom_dialog_edittext, true)
                        .positiveText("Done")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {

                                Success();
                            }
                        })
                        .build();
                tv_email = (FontableEditText) dialog.findViewById(R.id.itinerary_resendmailid);
                tv_email.setText("test@test.com");
                dialog.show();
                //DialogPlus();
            }
        });
        return view;
    }

    public void setValues() {
        aq.id(R.id.itinerary_booking_status).getTextView().setText("");
        aq.id(R.id.itinerary_booking_no).getTextView().setText("");
        aq.id(R.id.itinerary_points_earned).getTextView().setText("");
        aq.id(R.id.itinerary_send_mail_link).getTextView().setText("");
        aq.id(R.id.itinerary_depart_flight_start).getTextView().setText("");
        aq.id(R.id.itinerary_depart_flight_destination).getTextView().setText("");
        aq.id(R.id.itinerary_depart_amount).getTextView().setText("");
        aq.id(R.id.itinerary_depart_amt_decimal).getTextView().setText("");
        aq.id(R.id.itinerary_depart_currency).getTextView().setText("");
        aq.id(R.id.itinerary_depart_date).getTextView().setText("");
        aq.id(R.id.itinerary_return_flight_start).getTextView().setText("");
        aq.id(R.id.itinerary_return_flight_destination).getTextView().setText("");
        aq.id(R.id.itinerary_return_amount).getTextView().setText("");
        aq.id(R.id.itinerary_return_currency).getTextView().setText("");
        aq.id(R.id.itinerary_return_amount_decimal).getTextView().setText("");
        aq.id(R.id.itinerary_return_date).getTextView().setText("");
        aq.id(R.id.itinerary_total_amount).getTextView().setText("");
        aq.id(R.id.itinerary_total_currency).getTextView().setText("MYR");
        aq.id(R.id.itinerary_total_amount_decimal).getTextView().setText(".00");

    }

    private void Success() {
        String str_email = tv_email.getText().toString();

        if (str_email.length() > 0) {
            MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                    .content("Your Itinerary has been sent to " + "\"" + str_email + "\".")
                    .positiveText("OK")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                        }
                    })
                    .build();
            dialog.show();
        } else {
            Toast.makeText(getActivity(), "Invalid email id.", Toast.LENGTH_LONG).show();
        }
    }

    public void DialogPlus() {
        View alertView;
        String[] nameList = new String[]{"Koo Liew Xing", "Lim Soo Ling", "Harina", "Shanmugavel", "Nehru", "Surya", "Parthiban"};
        CharSequence[] items = {"Harina Elaine", "Koo liew xing", "Lim soo Ling"};

        List<HashMap<String, String>> fillMaps = new
                ArrayList<HashMap<String, String>>();
        int sno = 1;
        fillMaps.clear();
        for(int i = 0; i < nameList.length; i++){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("sno", String.valueOf(sno));
            map.put("name", nameList[i]);
            fillMaps.add(map);
            sno++;
        }
        String[] from = new String[] {"sno", "name"};
        int[] to = new int[] { R.id.tv_adapter_sno, R.id.tv_adapter_name };
        SimpleAdapter adapter = new SimpleAdapter(getActivity(), fillMaps, R.layout.custom_list_adapter_items, from, to);
        //

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        alertView = inflater.inflate(R.layout.custom_list_assistance_namelist, null);
        ListView lv = (ListView)alertView.findViewById(R.id.listview1);
        lv.setAdapter(adapter);
        builder.setView(alertView);
        builder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        dialog.show();





        /*String[] from = new String[] {"sno", "name"};
        int[] to = new int[] { R.id.tv_adapter_sno, R.id.tv_adapter_name };

        List<HashMap<String, String>> fillMaps = new
                ArrayList<HashMap<String, String>>();
        int sno = 1;
        fillMaps.clear();
        for(int i = 0; i < nameList.length; i++){
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("sno", String.valueOf(sno));
            map.put("name", nameList[i]);
            fillMaps.add(map);
            sno++;
        }


        SimpleAdapter adapter = new SimpleAdapter(getActivity(), fillMaps, R.layout.custom_list_adapter_items, from, to);
        //lv.setAdapter(adapter);

        final DialogPlus dialog = DialogPlus.newDialog(getActivity())
                //.setContentHolder(R.layout.custom_dialog_edittext)
                .setAdapter(adapter)
                .setHeader(R.layout.custom_list_assistance_namelist)
                .setCancelable(true)
                .setGravity(Gravity.BOTTOM)
                //.setOnClickListener(clickListener)
                .setOnItemClickListener(itemClickListener)
                .setOnDismissListener(dismissListener)
                .setOnCancelListener(cancelListener)
                .setOnBackPressListener(backPressed)
                .setExpanded(true, 800)
                .create();
        dialog.show();*/
        //ListView lv= (ListView)dialog.findViewById(R.id.listview1);
        //lv.setAdapter(adapter);
    }

  /*  OnItemClickListener itemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

            TextView textView = (TextView) view.findViewById(R.id.tv_adapter_name);
            String clickedAppName = textView.getText().toString();
            Toast.makeText(getActivity(), clickedAppName + " clicked", Toast.LENGTH_LONG).show();
        }
    };
    OnDismissListener dismissListener = new OnDismissListener() {
        @Override
        public void onDismiss(DialogPlus dialog) {
                    //Toast.makeText(getActivity(), "dismiss listener invoked!", Toast.LENGTH_SHORT).show();
        }
    };
    OnCancelListener cancelListener = new OnCancelListener() {
        @Override
        public void onCancel(DialogPlus dialog) {
            dialog.dismiss();
                 //Toast.makeText(getActivity(), "cancel listener invoked!", Toast.LENGTH_SHORT).show();
        }
    };
    OnBackPressListener backPressed = new OnBackPressListener() {
        @Override
        public void onBackPressed(DialogPlus dialog) {
            dialog.dismiss();
        }
    };

    public OnBackPressListener getBackPressed() {
        return backPressed;
    }*/
}
