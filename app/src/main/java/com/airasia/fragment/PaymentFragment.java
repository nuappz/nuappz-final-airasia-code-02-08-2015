package com.airasia.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airasia.mobile.R;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.PayMethodModel;
import com.airasia.model.PaymentInfo;
import com.airasia.model.SSRMainModel;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

public class PaymentFragment extends Fragment {
	
	BookingInfoModel booking;
	SSRMainModel ssrMain;
	PaymentInfo payInfo;
	Handler h;
	AQuery aq;
	
	public static PaymentFragment newInstance(SSRMainModel _ssrMain,
			BookingInfoModel _booking, PaymentInfo _payInfo) {
		PaymentFragment f = new PaymentFragment();
		f.booking = _booking;
		f.ssrMain = _ssrMain;
		f.payInfo = _payInfo;
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View v = inflater.inflate(R.layout.fragment_payment, null);
		
		aq = new AQuery(v);

		h = new Handler();
		h.postDelayed(runnable, 500);

		return v;
	}

	Runnable runnable = new Runnable() {
		@Override
		public void run() {
			updateView();
		}
	};

	public void updateView(){
		aq.id(R.id.pay_pay_type).text(payInfo.getSelectedPayType().getChannel());
		aq.id(R.id.pay_btn).getView().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				connPaymentCharge(payInfo);
			}
		});
	}

	public void connPaymentCharge(PaymentInfo _pay){
		RequestParams params = new RequestParams();
		int _method = payInfo.getSelectedPayType().getPayId();
		PayMethodModel mMethod = payInfo.getPayMethod();
		params.put("paymentMethod", String.valueOf(_method));
		params.put("userCurrencyCode", booking.getCurrencyCode());

		if (_method!=1){
			return;
		}
		if (_method == 2) {
			params.put("accountNumber", "");
			params.put("embossName", "");
			params.put("cardIssuer", "");
			params.put("issueCountry", "");
			params.put("merchant", "");
			params.put("expireYear", "");
			params.put("expireMonth", "");
		}

		if (_method == 3) {
			params.put("virtualNo", "");
			params.put("customerId", "");
		}

		if (_method == 2 || _method == 3) {
			params.put("verificationCode", "");
		}

		if (_method == 1) {
			params.put("bankID", mMethod.getBankId());
		}

		if (_method == 5) {
			params.put("gwID", "");
		}

//		params.put("recordLoator","");
//
//		params.put("gst","");
//		params.put("companyName","");
//		params.put("address","");
//		params.put("city","");
//		params.put("state","");
//		params.put("postcode","");

		params.put("userSession","");
		CustomHttpClient.postWithHash(CustomHttpClient.PAYMENT_CHARGES, params, new AsyncHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
				String response = JSonHelper.removeNull(responseBody);
				int responseCode = JSonHelper.GetResponseStatus(response);
				if (responseCode == JSonHelper.SUCCESS_REPONSE) {

				} else {

				}

			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

			}
		});
	}
	
}
