package com.airasia.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.airasia.util.LogHelper;

public class FlightSearchModel {

	public String JourneyDisplay, userCurrencyCode, displayCurrencyCode;
	public String exchangeRate, departureCurrencyCode;

	public String lowestFareDepart, LowestFareReturn;
	
	public Date date;
	
	public List<FlightDetailsModel> departFlight;
	public List<FlightDetailsModel> returnFlight;
	
	public boolean hasDepart = false, hasReturn = false;
	public boolean isOutRange;

	public FlightSearchModel(){
		departFlight = new ArrayList<FlightDetailsModel>();
		returnFlight = new ArrayList<FlightDetailsModel>();
	}
	
	public FlightSearchModel(Date date){
		this.date = date;
	}
	

	public String getJourneyDisplay() {
		return JourneyDisplay;
	}

	public void setJourneyDisplay(String journeyDisplay) {
		JourneyDisplay = journeyDisplay;
	}

	public String getUserCurrencyCode() {
		return userCurrencyCode;
	}

	public void setUserCurrencyCode(String userCurrencyCode) {
		this.userCurrencyCode = userCurrencyCode;
	}

	public String getDisplayCurrencyCode() {
		return displayCurrencyCode;
	}

	public void setDisplayCurrencyCode(String displayCurrencyCode) {
		this.displayCurrencyCode = displayCurrencyCode;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getDepartureCurrencyCode() {
		return departureCurrencyCode;
	}

	public void setDepartureCurrencyCode(String departureCurrencyCode) {
		this.departureCurrencyCode = departureCurrencyCode;
	}

	public List<FlightDetailsModel> getFlightList() {
		return departFlight;
	}

	public void setFlightList(List<FlightDetailsModel> flightList, int type) {
		if (type == 0){
			if (this.departFlight == null){
				this.departFlight = new ArrayList<FlightDetailsModel>();
			}
			this.departFlight.addAll(flightList);
			LogHelper.debug("Message " + departFlight.size());
		}else{
			if (this.returnFlight==null){
				this.returnFlight = new ArrayList<FlightDetailsModel>();
			}
			
			this.returnFlight.addAll(flightList);
			LogHelper.debug("Message " + returnFlight.size());
		}
	}

	public List<FareTypeModel> getDepartFareList(int type){
		List<FareTypeModel> list = new ArrayList<FareTypeModel>();
		if (departFlight == null || departFlight.size() <= 0){
			return list;
		}
		for (int i = 0; i <departFlight.size();i++){
			List<FareTypeModel> tmp = new ArrayList<FareTypeModel>();
			if (type == 1){
				tmp = departFlight.get(i).getBusinessList();
			}else if (type == 2){
				tmp = departFlight.get(i).getPremiumList();
			}else{
				tmp = departFlight.get(i).getLowFareList();
			}
			if (tmp!=null && tmp.size()>0){
				list.addAll(tmp);
			}
		}
		return list;
	}

	public FareTypeModel getDepartUpgrade(int type, int pos){
		FareTypeModel tmp = new FareTypeModel();
		if (departFlight == null || departFlight.size() <= 0){
			return tmp;
		}
		for (int i = 0; i <departFlight.size();i++){
			if (type == 0){
				LogHelper.debug("Premium Size " + departFlight.get(i).getPremiumList().size());
				LogHelper.debug("Business Size " + departFlight.get(i).getBusinessList().size());
				if (departFlight.get(i).getPremiumList().size()>0){
					tmp = departFlight.get(i).getPremiumList().get(0);
				}else if(departFlight.get(i).getBusinessList().size()>0){
					tmp = departFlight.get(i).getBusinessList().get(0);
				}
			}else{
				if (departFlight.get(i).getBusinessList().size()>0){
					tmp = departFlight.get(i).getBusinessList().get(0);
				}
			}
		}
		return tmp;
	}
	
	public FareTypeModel getReturnUpgrade(int type, int pos){
		FareTypeModel tmp = new FareTypeModel();
		if (returnFlight == null || returnFlight.size() <= 0){
			return tmp;
		}
		for (int i = 0; i <returnFlight.size();i++){
			if (type == 0){
				if (returnFlight.get(i).getPremiumList().size()>0){
					tmp = returnFlight.get(i).getPremiumList().get(0);
				}else if(returnFlight.get(i).getBusinessList().size()>0){
					returnFlight.get(i).getBusinessList().get(0);
				}
			}else{
				if (returnFlight.get(i).getBusinessList().size()>0){
					tmp = returnFlight.get(i).getBusinessList().get(0);
				}
			}
			
		}
		return tmp;
	}
	
	public List<FareTypeModel> getReturnFareList(int type){
		List<FareTypeModel> list = new ArrayList<FareTypeModel>();
		if (returnFlight==null){
			return list;
		}
		for (int i = 0; i <returnFlight.size();i++){
			List<FareTypeModel> tmp = new ArrayList<FareTypeModel>();
			if (type == 1){
				tmp = returnFlight.get(i).getBusinessList();
			}else if (type == 2){
				tmp = returnFlight.get(i).getPremiumList();
			}else{
				tmp = returnFlight.get(i).getLowFareList();
			}
			if (tmp!=null && tmp.size()>0){
				list.addAll(tmp);
			}
		}
		return list;
	}
	

	public Date getDate() {
		return date;
	}
	
	public String getDateyyyyMMdd(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = format.format(this.date);
		return dateStr;
	}

	public String getDateEEEddMM(){
		SimpleDateFormat format = new SimpleDateFormat("EEE dd MMM");
		String dateStr = format.format(this.date);
		return dateStr;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}

	public List<FlightDetailsModel> getDepartFlight() {
		return departFlight;
	}

	public void setDepartFlight(List<FlightDetailsModel> departFlight) {
		this.departFlight = departFlight;
	}

	public List<FlightDetailsModel> getReturnFlight() {
		return returnFlight;
	}

	public void setReturnFlight(List<FlightDetailsModel> returnFlight) {
		this.returnFlight = returnFlight;
	}

	public String getLowestFareDepart() {
		return lowestFareDepart;
	}

	public void setLowestFareDepart(String lowestFareDepart) {
		this.lowestFareDepart = lowestFareDepart;
	}

	public String getLowestFareReturn() {
		return LowestFareReturn;
	}

	public void setLowestFareReturn(String lowestFareReturn) {
		LowestFareReturn = lowestFareReturn;
	}

	public boolean isHasDepart() {
		return hasDepart;
	}

	public void setHasDepart(boolean hasDepart) {
		this.hasDepart = hasDepart;
	}

	public boolean isHasReturn() {
		return hasReturn;
	}

	public void setHasReturn(boolean hasReturn) {
		this.hasReturn = hasReturn;
	}

	public boolean isOutRange() {
		return isOutRange;
	}

	public void setOutRange(boolean isOutRange) {
		this.isOutRange = isOutRange;
	}
	
}
