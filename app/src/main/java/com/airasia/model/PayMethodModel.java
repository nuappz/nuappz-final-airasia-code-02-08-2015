package com.airasia.model;


public class PayMethodModel {
	
	String brand, brandName, brandLogo, brandCurrency;
	String bankId;
	double processFee;

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getBrandName() {
		return brandName;
	}
	
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	public String getBrandLogo() {
		return brandLogo;
	}
	
	public void setBrandLogo(String brandLogo) {
		this.brandLogo = brandLogo;
	}
	
	public String getBrandCurrency() {
		return brandCurrency;
	}
	
	public void setBrandCurrency(String brandCurrency) {
		this.brandCurrency = brandCurrency;
	}
	
	public double getProcessFee() {
		return processFee;
	}
	
	public void setProcessFee(double processFee) {
		this.processFee = processFee;
	}
	
	
	
}
