package com.airasia.model;

import java.util.List;

/**
 * Created by appcell on 7/30/15.
 */
public class PaymentInfo {


    List<PaymentOptionModel> payOptions;

    int selectedID = 0, subId = 0;

//    if (_method == 2) {
//        params.put("accountNumber", "");
//        params.put("embossName", "");
//        params.put("cardIssuer", "");
//        params.put("issueCountry", "");
//        params.put("merchant", "");
//        params.put("expireYear", "");
//        params.put("expireMonth", "");
//    }

    String bankId, gwId, verificationCode, virtualNo, customerId;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public int getSelectedID() {
        return selectedID;
    }

    public void setSelectedID(int selectedID) {
        this.selectedID = selectedID;
    }

    public PaymentOptionModel getSelectedPayType(){
        if (payOptions == null || payOptions.size()<=0){
            return new PaymentOptionModel();
        }
        return this.payOptions.get(selectedID);
    }

    public List<PaymentOptionModel> getPayOptions() {
        return payOptions;
    }

    public void setPayOptions(List<PaymentOptionModel> payOptions) {
        this.payOptions = payOptions;
    }

    public PayMethodModel getPayMethod(){
        return this.payOptions.get(selectedID).getPayMethods().get(subId);
    }

    public void setSubId(int id){
        this.subId = id;
    }
}
