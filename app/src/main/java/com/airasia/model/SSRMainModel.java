package com.airasia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SSRMainModel implements Serializable{
	
    String planCode, feeCode;
	boolean hasInsurance = false;
	boolean isInsuranceSelect = false;
	List<MemberInfoModel> passangers;
	List<SSRContainer> ssrContainers;
	List<SSRContainer> departContainer, returnContainer;
	
	public boolean isHasInsurance() {
		return hasInsurance;
	}

	public void setHasInsurance(boolean hasInsurance) {
		this.hasInsurance = hasInsurance;
	}

	public List<MemberInfoModel> getPassangers() {
		return passangers;
	}

	public void setPassangers(List<MemberInfoModel> _passangers) {
		if (passangers == null){
			passangers = new ArrayList<MemberInfoModel>();
		}else{
			passangers.clear();
			passangers = null;
			passangers = new ArrayList<MemberInfoModel>();
		}
		this.passangers.addAll(_passangers);
	}

	public String getPlanCode() {
		return planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getFeeCode() {
		return feeCode;
	}

	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}

	public List<SSRContainer> getSsrContainers() {
		return ssrContainers;
	}

	public void setSsrContainers(List<SSRContainer> ssrContainers) {
		this.ssrContainers = ssrContainers;
	}

	public List<SSRContainer> getDepartContainer() {
		return departContainer;
	}

	public void setDepartContainer(List<SSRContainer> departContainer) {
		this.departContainer = departContainer;
	}

	public List<SSRContainer> getReturnContainer() {
		return returnContainer;
	}

	public void setReturnContainer(List<SSRContainer> returnContainer) {
		this.returnContainer = returnContainer;
	}

	public String getTotalInsurance(){
		String result = "0.00";
		double total = 0.00;
		if (passangers != null && passangers.size()>0){
			for (int i = 0; i < passangers.size(); i ++){
				total += passangers.get(i).getPremiumAmount();
			}
		}
		
		result = String.valueOf(total);
		
		return result;
	}
	

	public void generateEmptyMeal(){
		for (int i = 0; i<departContainer.size(); i++){
			for (int psgIndex = 0 ; psgIndex < passangers.size(); psgIndex++){
				MemberInfoModel model = passangers.get(psgIndex);
				model.addMeal(new ArrayList<SSRItemModel>(), true);
			}
		}
		
		if (returnContainer != null && returnContainer.size()>0){
			for (int i = 0; i<returnContainer.size(); i++){
				for (int psgIndex = 0 ; psgIndex < passangers.size(); psgIndex++){
					MemberInfoModel model = passangers.get(psgIndex);
					model.addMeal(new ArrayList<SSRItemModel>(), false);
				}
			}
		}
	}

	public String getTotalMeal(){
		String result = "0.00";
		double total = 0.00;
		if (passangers != null && passangers.size()>0){
			for (int i = 0; i < passangers.size(); i ++){
				total += passangers.get(i).getTotalMealAmount();
			}
		}

		result = String.valueOf(total);

		return result;
	}

	public String getTotalBaggagePrice(){
		String result = "0.00";
		double total = 0.00;
		if (passangers != null && passangers.size()>0){
			for (int i = 0; i< passangers.size(); i++){
				total+= passangers.get(i).getTotalBaggageCharge();
			}
		}

		result = String.valueOf(total);

		return result;
	}

	public boolean isInsuranceSelect() {
		return isInsuranceSelect;
	}

	public void setIsInsuranceSelect(boolean isInsuranceSelect) {
		this.isInsuranceSelect = isInsuranceSelect;
	}
}
