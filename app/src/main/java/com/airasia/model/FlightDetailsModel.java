package com.airasia.model;

import java.util.ArrayList;
import java.util.List;

public class FlightDetailsModel {

	String journeyKey, lowfareKey, preflexKey, businessclassKey;
	String transitInfo;
	double lowestFare, discount;
	boolean flightStatus = false, isCityIsland = false;
	
	List<SegmentModel> journeySeg;
	
	List<String> fareProduct;

	List<FareTypeModel> lowFareList = new ArrayList<FareTypeModel>();
	List<FareTypeModel> businessList = new ArrayList<FareTypeModel>();
	List<FareTypeModel> premiumList = new ArrayList<FareTypeModel>();
	
	public String getJourneyKey() {
		return journeyKey;
	}

	public void setJourneyKey(String journeyKey) {
		this.journeyKey = journeyKey;
	}

	public double getLowestFare() {
		return lowestFare;
	}

	public void setLowestFare(double lowestFare) {
		this.lowestFare = lowestFare;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}

	public List<SegmentModel> getJourneySeg() {
		return journeySeg;
	}

	public void setJourneySeg(List<SegmentModel> jourSeg) {
		this.journeySeg = jourSeg;
	}

	public List<String> getFareProduct() {
		return fareProduct;
	}

	public void setFareProduct(List<String> fareProduct) {
		this.fareProduct = fareProduct;
	}

	public String getLowfareKey() {
		return lowfareKey;
	}

	public void setLowfareKey(String lowfareKey) {
		this.lowfareKey = lowfareKey;
	}

	public String getPreflexKey() {
		return preflexKey;
	}

	public void setPreflexKey(String preflexKey) {
		this.preflexKey = preflexKey;
	}

	public String getBusinessclassKey() {
		return businessclassKey;
	}

	public void setBusinessclassKey(String businessclassKey) {
		this.businessclassKey = businessclassKey;
	}

	public List<FareTypeModel> getLowFareList() {
		return lowFareList;
	}

	public void setLowFareList(List<FareTypeModel> lowFareList) {
		this.lowFareList.addAll(lowFareList);
	}

	public List<FareTypeModel> getBusinessList() {
		return businessList;
	}

	public void setBusinessList(List<FareTypeModel> businessList) {
		this.businessList.addAll(businessList);
	}

	public List<FareTypeModel> getPremiumList() {
		return premiumList;
	}

	public void setPremiumList(List<FareTypeModel> premiumList) {
		this.premiumList.addAll(premiumList);
	}
	
	public void addPremiumList(FareTypeModel model){
		if (premiumList == null){
			premiumList = new ArrayList<FareTypeModel>();
		}
		premiumList.add(model);
	}

	public void addBusinessList(FareTypeModel model){
		if (businessList == null){
			businessList = new ArrayList<FareTypeModel>();
		}
		businessList.add(model);
	}
	
	public void addLowFareList(FareTypeModel model){
		if (lowFareList == null){
			lowFareList = new ArrayList<FareTypeModel>();
		}
		lowFareList.add(model);
	}

	public boolean isFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(boolean flightStatus) {
		this.flightStatus = flightStatus;
	}

	public boolean isCityIsland() {
		return isCityIsland;
	}

	public void setCityIsland(boolean isCityIsland) {
		this.isCityIsland = isCityIsland;
	}

	public String getTransitInfo() {
		return transitInfo;
	}

	public void setTransitInfo(String transitInfo) {
		this.transitInfo = transitInfo;
	}

	
	
}
