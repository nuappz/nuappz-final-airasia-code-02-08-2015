package com.airasia.model;

/**
 * Created by appcell on 7/29/15.
 */
public class BannerModel {

    int id, lastModeified;
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLastModeified() {
        return lastModeified;
    }

    public void setLastModeified(int lastModeified) {
        this.lastModeified = lastModeified;
    }




}
