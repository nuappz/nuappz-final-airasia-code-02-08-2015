package com.airasia.model;

import com.airasia.mobile.R;

/**
 * Created by terrychong on 7/30/15.
 */

//enum kSeatType
//{
//        kSeatTypeStandard,
//        kSeatTypeHotSeat,
//        kSeatTypeHotSeatWithEmergency,
//        kSeatTypeQuiteZoneWithHotSeat,
//        kSeatTypeQuiteZone,
//        kSeatTypeHotSeatWithBabyBassinet,
//        kSeatTypeHotSeat2,
//        kSeatTypeBusinessClassWithBabyBassinet,
//        kSeatTypeBusinessClass;
//};

public class SeatMapModel {

    final public static int kSeatInfoTypeEmpty = 0;
    final public static int kSeatInfoTypeSeat = 1;
    final public static int kSeatInfoTypeToilet = 2;
    final public static int kSeatInfoTypeLabel =3;
    final public static int kSeatInfoTypeLeftNumber = 4;
    final public static int kSeatInfoTypeCorridor = 5;
    final public static int kSeatInfoTypeExit = 6;

    final public static int kSeatTypeStandard = 0;
    final public static int kSeatTypeHotSeat = 1;
    final public static int kSeatTypeHotSeatWithEmergency = 2;
    final public static int  kSeatTypeQuiteZoneWithHotSeat = 3;
    final public static int kSeatTypeQuiteZone = 4;
    final public static int kSeatTypeHotSeatWithBabyBassinet = 5;
    final public static int kSeatTypeHotSeat2 = 6;
    final public static int kSeatTypeBusinessClassWithBabyBassinet = 7;
    final public static int kSeatTypeBusinessClass = 8;

    public String seatColumn;
    public String seatRow;
    public String seatDesignator;

    public String equipmentType;
    public String equipmentTypeSuffix;
    public String equipmentPRBCCode;
    public int passangerNumberAssigned = -1;


    public boolean isOccupied;
    public double fare;


    public int seatType;


    public int infoType = kSeatInfoTypeSeat;

    public int seatColor()
    {


        switch (infoType) {
            case kSeatInfoTypeSeat:
            {
                switch (seatType) {


                    case kSeatTypeHotSeatWithEmergency:case kSeatTypeHotSeat: case kSeatTypeHotSeatWithBabyBassinet:
                        return R.color.color_seat_hot_seat;
                    case kSeatTypeQuiteZone:
                        return R.color.color_seat_quiet_zone;

                    case kSeatTypeHotSeat2: case kSeatTypeQuiteZoneWithHotSeat:
                        return R.color.color_seat_hot_seat2;

                    case kSeatTypeBusinessClass: case kSeatTypeBusinessClassWithBabyBassinet:
                        return R.color.color_seat_business_class;
                    default:
                        return R.color.color_seat_normal;
                    //return [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1];
                }
            }

            default:
                return android.R.color.transparent;

        }
    }

    public boolean isSeat()
    {
        return this.infoType == kSeatInfoTypeSeat;
    }

    public int seatIcon()
    {
        if (!this.isSeat()) {
            return 0;
        }

        if (this.passangerNumberAssigned>=0) {
            return R.drawable.icon_legend_assigned;
        }

        if (this.isOccupied) {
            return R.drawable.icon_legend_occupied;
        }

        switch (infoType) {
            case kSeatInfoTypeSeat:
            {
                switch (this.seatType) {


                    case kSeatTypeHotSeatWithEmergency:
                        return R.drawable.icon_legend_seat_near_emergency;
                    case kSeatTypeBusinessClassWithBabyBassinet:
                        return R.drawable.icon_legend_seat_baby;
                    case kSeatTypeHotSeatWithBabyBassinet:
                        return R.drawable.icon_legend_seat_baby_red;

                    default:
                        return 0;
                }

            }

            default:
                return 0;

        }

    }


}
