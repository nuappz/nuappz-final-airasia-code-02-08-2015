package com.airasia.model;

import java.util.List;

public class PaymentOptionModel {

	int payId;
	String channel;
	
	List<PayMethodModel> payMethods;

	public int getPayId() {
		return payId;
	}

	public void setPayId(int payId) {
		this.payId = payId;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public List<PayMethodModel> getPayMethods() {
		return payMethods;
	}

	public void setPayMethods(List<PayMethodModel> payMethods) {
		this.payMethods = payMethods;
	}
	
	
	
}
