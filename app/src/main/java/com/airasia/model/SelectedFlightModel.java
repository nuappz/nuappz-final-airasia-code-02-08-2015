package com.airasia.model;

import java.util.List;

import com.airasia.holder.ConstantHolder;

public class SelectedFlightModel {

	
	public String departId, returnId;
	public String dDate, rDate, currency;
	
	boolean isUpgradeDepart = false;
	boolean isUpgradeReturn = false;
	
	public int adultPax =0 , childPax = 0, infantPax = 0;
	
	public FareTypeModel departModel = new FareTypeModel();
	public FareTypeModel returnModel = new FareTypeModel();
	
	public FareTypeModel upgradeDepart = new FareTypeModel();
	public FareTypeModel upgradeReturn = new FareTypeModel();
	
	public List<String> departName;
	public List<String> returnName;
	
	public void setPaxCount(int adult, int child, int infant){
		this.adultPax = adult;
		this.childPax = child;
		this.infantPax = infant;
	}
	
	
	//0 = current selected; 1 = upgrade flight price
	public double getDepartTotal(int type){
		double total = 0;
		if (type ==0){
			if(adultPax!=0){
				total += (adultPax * departModel.getHashMap().get(ConstantHolder.FARE_ADT).getFinalFare());
			}
			if(childPax!=0){
				total += (adultPax * departModel.getHashMap().get(ConstantHolder.FARE_CHD).getFinalFare());
			}
			if(infantPax!=0){
				total += (adultPax * departModel.getHashMap().get(ConstantHolder.FARE_INF).getFinalFare());
			}
		}else{
			if (upgradeDepart.hashMap != null && upgradeDepart.hashMap.size()>0){
				if(adultPax!=0){
					if (upgradeDepart.getHashMap().get(ConstantHolder.FARE_ADT)!=null){
						total += (adultPax * upgradeDepart.getHashMap().get(ConstantHolder.FARE_ADT).getFinalFare());
					}
				}
				if(childPax!=0){
					if (upgradeDepart.getHashMap().get(ConstantHolder.FARE_CHD)!=null){
						total += (adultPax * upgradeDepart.getHashMap().get(ConstantHolder.FARE_CHD).getFinalFare());
					}
				}
				if(infantPax!=0){
					if (upgradeDepart.getHashMap().get(ConstantHolder.FARE_INF)!=null){
						total += (adultPax * upgradeDepart.getHashMap().get(ConstantHolder.FARE_INF).getFinalFare());
					}
				}
			}
		}
		
		return total;
	}
	
	public double getReturnTotal(int type){
		double total = 0;
		if (type == 0){
			if(adultPax!=0){
				total += (adultPax * returnModel.getHashMap().get(ConstantHolder.FARE_ADT).getFinalFare());
			}
			if(childPax!=0){
				total += (adultPax * returnModel.getHashMap().get(ConstantHolder.FARE_CHD).getFinalFare());
			}
			if(infantPax!=0){
				total += (adultPax * returnModel.getHashMap().get(ConstantHolder.FARE_INF).getFinalFare());
			}
		}else{
			if (upgradeReturn.hashMap != null && upgradeReturn.hashMap.size()>0){
				if(adultPax!=0){
					if (upgradeReturn.getHashMap().get(ConstantHolder.FARE_ADT)!=null){
						total += (adultPax * upgradeReturn.getHashMap().get(ConstantHolder.FARE_ADT).getFinalFare());
					}
				}
				if(childPax!=0){
					if (upgradeReturn.getHashMap().get(ConstantHolder.FARE_CHD)!=null){
						total += (adultPax * upgradeReturn.getHashMap().get(ConstantHolder.FARE_CHD).getFinalFare());
					}
				}
				if(infantPax!=0){
					if (upgradeReturn.getHashMap().get(ConstantHolder.FARE_INF)!=null){
						total += (adultPax * upgradeReturn.getHashMap().get(ConstantHolder.FARE_INF).getFinalFare());
					}
				}
			}
		}
		
		return total;
	}
	
	
	public int totalPassanger(){
		int total = this.adultPax + this.childPax + this.infantPax;
		return total;
	}


	public boolean isUpgradeDepart() {
		return isUpgradeDepart;
	}


	public void setUpgradeDepart(boolean isUpgradeDepart) {
		this.isUpgradeDepart = isUpgradeDepart;
	}


	public boolean isUpgradeReturn() {
		return isUpgradeReturn;
	}


	public void setUpgradeReturn(boolean isUpgradeReturn) {
		this.isUpgradeReturn = isUpgradeReturn;
	}


	public FareTypeModel getUpgradeDepart() {
		return upgradeDepart;
	}


	public void setUpgradeDepart(FareTypeModel upgradeDepart) {
		this.upgradeDepart = upgradeDepart;
	}


	public FareTypeModel getUpgradeReturn() {
		return upgradeReturn;
	}


	public void setUpgradeReturn(FareTypeModel upgradeReturn) {
		this.upgradeReturn = upgradeReturn;
	}
	
}
