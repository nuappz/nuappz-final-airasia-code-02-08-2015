package com.airasia.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.impl.cookie.DateUtils;

import com.airasia.util.ConstantHelper;
import com.airasia.util.LogHelper;


public class SearchInfoModel {

	String departStation, arrivalStation;
	String promoCode, userCurrency, displayCurrency, departureCurrency;
	int adultCount, childCount, infantCount;
	
	List<FlightSearchModel> flightList;
	List<String> departList;
	List<String> returnList;
	
	boolean searchReturn = false;
	float exchangeRate = 0;
	boolean fareDiscount = false;

	String selectDepartDate = "";
	String selectReturnDate = "";
	
	public SearchInfoModel(){
		
	}
	
	public SearchInfoModel(String dStation, String aStation, String _promoCode,
			String _userCurrency, int adultC, int childC, int infantC, String departDate, String returnDate){
		this.departStation = dStation;
		this.arrivalStation = aStation;
		this.promoCode = _promoCode;
		this.userCurrency = _userCurrency;
		this.adultCount = adultC;
		this.childCount = childC;
		this.infantCount = infantC;
		this.selectDepartDate = departDate;
		this.selectReturnDate = returnDate;
		generateFlightList();
	}

	public String getDepartStation() {
		return departStation;
	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public String getUserCurrency() {
		return userCurrency;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}
	
	public void generateFlightList(){
		flightList = new ArrayList<FlightSearchModel>();
		Date date = new Date();
		date = ConstantHelper.addDays(date, -1);
		for (int i = 0; i < 422; i ++){
			FlightSearchModel item = new FlightSearchModel(date);
			if (i == 0 || i == 421){
				item.isOutRange = true;
			}else{
				item.isOutRange = false;
			}
			date = ConstantHelper.addDays(date, 1);
			flightList.add(item);
		}
		
	}
	
	public void addFlightIntoDate(String date, String FlightSearchModel){
		
	}
	
	public FlightSearchModel getFlightByDate(String inDate){
		FlightSearchModel item = new FlightSearchModel();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = dateFormat.parse(inDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			date = new Date();
		}
		
		for (int i = 0; i < flightList.size() ; i ++){
			Date tempDate = flightList.get(i).getDate();
			if (ConstantHelper.sameday(date, tempDate)){
				item = flightList.get(i);
//				LogHelper.debug("Date Match " +item.getDate().toString());
				break;
			}
		}
		
		return item;
	}

	public List<FlightSearchModel> getFlightList() {
		return flightList;
	}
	
	public void updateModel(FlightSearchModel model){
		for (int i = 0; i < flightList.size(); i ++){
			boolean sameD = ConstantHelper.sameday(flightList.get(i).getDate(), model.getDate());
			if (sameD){
				flightList.set(i, model);
//				LogHelper.debug("Message 2 " + flightList.size());
//				LogHelper.debug("Message 3 " + flightList.get(i).getDepartFareList(0).size());
				break;
			}
		}
	}

	public List<String> getDepartList() {
		return departList;
	}

	public void setDepartList(List<String> departList) {
		this.departList = departList;
	}

	public List<String> getReturnList() {
		return returnList;
	}

	public void setReturnList(List<String> returnList) {
		this.returnList = returnList;
	}

	public String getSelectDepartDate() {
		return selectDepartDate;
	}

	public void setSelectDepartDate(String selectDepartDate) {
		this.selectDepartDate = selectDepartDate;
	}

	public String getSelectReturnDate() {
		return selectReturnDate;
	}

	public void setSelectReturnDate(String selectReturnDate) {
		this.selectReturnDate = selectReturnDate;
	}

	public boolean isReturn() {
		return searchReturn;
	}

	public void setReturn(boolean isReturn) {
		this.searchReturn = isReturn;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public void setDepartStation(String departStation) {
		this.departStation = departStation;
	}

	public String getDepartureCurrency() {
		return departureCurrency;
	}

	public void setDepartureCurrency(String departureCurrency) {
		this.departureCurrency = departureCurrency;
	}

	public String getDisplayCurrency() {
		return displayCurrency;
	}

	public void setDisplayCurrency(String displayCurrency) {
		this.displayCurrency = displayCurrency;
	}

	public float getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(float exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public boolean isFareDiscount() {
		return fareDiscount;
	}

	public void setFareDiscount(int fareDisc){
		this.fareDiscount = (fareDisc == 1);
	}

	public void setFareDiscount(boolean fareDiscount) {
		this.fareDiscount = fareDiscount;
	}

	public void setFlightList(List<FlightSearchModel> flightList) {
		this.flightList = flightList;
	}

	public void setInfantCount(int infantCount) {
		this.infantCount = infantCount;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public boolean isSearchReturn() {
		return searchReturn;
	}

	public void setSearchReturn(boolean searchReturn) {
		this.searchReturn = searchReturn;
	}

	public void setUserCurrency(String userCurrency) {
		this.userCurrency = userCurrency;
	}
}
