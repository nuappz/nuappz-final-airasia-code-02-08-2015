package com.airasia.model;

import java.io.Serializable;
import java.util.List;

public class SegmentModel implements Serializable {
	
	String arriveStation, departStation, flgithNum, carrierCode;
	String dateSTA, dataSTD, dateSTD4, dataSTD24, dateSTDCHECK;
	String arrTimeZone, departTimeZone, fareProduct, airLineCompany;
	String estimateTime;
	
	StationModel departItem, returnItem;
	SegmentLegs segLegs;
	
	int fareType, totalSegment, inftSeat, segmentType;
	
	List<SeatSummary> seatList;
	
	boolean isInternational;

	public String getArriveStation() {
		return arriveStation;
	}

	public void setArriveStation(String arriveStation) {
		this.arriveStation = arriveStation;
	}

	public String getDepartStation() {
		return departStation;
	}

	public void setDepartStation(String departStation) {
		this.departStation = departStation;
	}

	public String getFlgithNum() {
		return flgithNum;
	}

	public void setFlgithNum(String flgithNum) {
		this.flgithNum = flgithNum;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getDateSTA() {
		return dateSTA;
	}

	public void setDateSTA(String dateSTA) {
		this.dateSTA = dateSTA;
	}

	public String getDataSTD() {
		return dataSTD;
	}

	public void setDataSTD(String dataSTD) {
		this.dataSTD = dataSTD;
	}

	public String getDateSTA24() {
		return dateSTD4;
	}

	public void setDateSTD4(String dateSTD4) {
		this.dateSTD4 = dateSTD4;
	}

	public String getDataSTD24() {
		return dataSTD24;
	}

	public void setDataSTD24(String dataSTD24) {
		this.dataSTD24 = dataSTD24;
	}

	public String getDateSTDCHECK() {
		return dateSTDCHECK;
	}

	public void setDateSTDCHECK(String dateSTDCHECK) {
		this.dateSTDCHECK = dateSTDCHECK;
	}

	public String getArrTimeZone() {
		return arrTimeZone;
	}

	public void setArrTimeZone(String arrTimeZone) {
		this.arrTimeZone = arrTimeZone;
	}

	public String getDepartTimeZone() {
		return departTimeZone;
	}

	public void setDepartTimeZone(String departTimeZone) {
		this.departTimeZone = departTimeZone;
	}

	public String getFareProduct() {
		return fareProduct;
	}

	public void setFareProduct(String fareProduct) {
		this.fareProduct = fareProduct;
	}

	public int getFareType() {
		return fareType;
	}

	public void setFareType(int fareType) {
		this.fareType = fareType;
	}

	public int getTotalSegment() {
		return totalSegment;
	}

	public void setTotalSegment(int totalSegment) {
		this.totalSegment = totalSegment;
	}

	public boolean isInternational() {
		return isInternational;
	}

	public void setInternational(boolean isInternational) {
		this.isInternational = isInternational;
	}

	public List<SeatSummary> getSeatList() {
		return seatList;
	}

	public void setSeatList(List<SeatSummary> seatList) {
		this.seatList = seatList;
	}

	public String getAirLineCompany() {
		return airLineCompany;
	}

	public void setAirLineCompany(String airLineCompany) {
		this.airLineCompany = airLineCompany;
	}

	public String getDateSTD4() {
		return dateSTD4;
	}

	public int getInftSeat() {
		return inftSeat;
	}

	public void setInftSeat(int inftSeat) {
		this.inftSeat = inftSeat;
	}

	public String getEstimateTime() {
		return estimateTime;
	}

	public void setEstimateTime(String estimateTime) {
		this.estimateTime = estimateTime;
	}

	public StationModel getDepartItem() {
		return departItem;
	}

	public void setDepartItem(StationModel departItem) {
		this.departItem = departItem;
	}

	public StationModel getReturnItem() {
		return returnItem;
	}

	public void setReturnItem(StationModel returnItem) {
		this.returnItem = returnItem;
	}

	
	public String getDepartReturnStation()
	{
		return departStation+"/"+arriveStation;
	}
	
	
	public int getSegmentType() {
		return segmentType;
	}

	public void setSegmentType(int segmentType) {
		this.segmentType = segmentType;
	}

	public SegmentLegs getSegLegs() {
		return segLegs;
	}

	public void setSegLegs(SegmentLegs segLegs) {
		this.segLegs = segLegs;
	}

}
