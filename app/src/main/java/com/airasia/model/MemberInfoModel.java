package com.airasia.model;

import com.airasia.util.ConstantHelper;
import com.airasia.util.LogHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MemberInfoModel implements Serializable {

	public static final int ADULT =1, CHILD = 2, INFANT = 3, HEADER = -1
			, CONTACT = 4, EMERGENCY = 5;
	public static final String ADT_TEXT = "Adult", CHD_TEXT = "Child", INF_TEXT = "Infant";
	
	String userName, fName, lName, address1, address2, city, countryCode, country,
			cultureCode, nationality, personalEmail, postCode, state, title, dateOfBirth
			, passport, issuingCountry, paxType, refUserId,  dob, mobileNo, expiredDate, frenCID,
			genderStr, wheelChair;
	
	int passangerNum, psgNumber;
	int cid, pid, notifPref,
			gender, bigID, id, seq, bringInfant;
	
	List<List<CheckInModel>> checkinList;
	boolean isBigCustomer, isInfant, isContactPerson = false, isQualified = false;
	boolean checkIn = false, isDepart = false, isReturn = false;
	
	//Guest List
	int psgType = 0;
	float premiumAmount= 0.00f;
	
	
	//SSR Item
	SSRItemModel itemWheel;
	int dBaggagePos = 0, rBaggagePos = 0;
	List<SSRItemModel> itemDepartBaggage;
	List<SSRItemModel> itemReturnBaggage;
	List<List<SSRItemModel>> itemDepartMeal;
	List<List<SSRItemModel>> itemReturnMeal;
	

	//for signup only
	private String password;
	
	List<TravelDocModel> departDoc = new ArrayList<TravelDocModel>();
	List<TravelDocModel> returnDoc = new ArrayList<TravelDocModel>();
	
	public MemberInfoModel(){
		
	}
	
	public MemberInfoModel(String _username, String _password, String _national, String _firstName
				,String _lastName, String _dob, String _address, String _country, String _state
				,String _town, String _postCode, String _mobile){
		this.userName = _username;
		this.password = _password;
		this.nationality = _national;
		this.fName = _firstName;
		this.lName = _lastName;
		this.dob = _dob;
		this.dateOfBirth = _dob;
		this.address1 = _address;
		this.country = _country;
		this.state = _state;
		this.city = _town;
		this.postCode = _postCode;
		this.mobileNo = _mobile;
		
	}
	

	public String getFullAddress(){
		return (getAddress1()+getAddress2());
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	
	public String getFullName(){
		return (this.lName + " " + this.fName);
	}
	
	public String getAddress1() {
		return address1;
	}
	
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress2() {
		return address2;
	}
	
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getCultureCode() {
		return cultureCode;
	}
	
	public void setCultureCode(String cultureCode) {
		this.cultureCode = cultureCode;
	}
	
	public String getDob() {
		return dob;
	}
	
	public String getDisplayDate(){
		return ConstantHelper.getDateFromddMMyyyy(this.dob);
	}
	
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	public int getGender() {
		return gender;
	}
	
	public String getGenderAlphabet(){
		if (this.gender == 1){
			return "M";
		}else{
			return "F";
		}
	}
	
	public void setGender(int _gender) {
		this.gender = _gender;
	}
	
	
	public void setFrenGender(String _gender) {
		if (_gender == null || _gender.length()<=0){
			this.gender = 0;
		}else{
			this.gender = Integer.parseInt(_gender);
		}
	}
	
	public String getMobileNo() {
		return mobileNo;
	}
	
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	
	public String getNationality() {
		return nationality;
	}
	
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public int getNotifPref() {
		return notifPref;
	}
	
	public void setNotifPref(int notifPref) {
		this.notifPref = notifPref;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPersonalEmail() {
		return personalEmail;
	}
	
	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}
	
	public String getPostCode() {
		return postCode;
	}
	
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getCid() {
		return cid;
	}
	
	public void setCid(int cid) {
		this.cid = cid;
	}
	
	public int getPid() {
		return pid;
	}
	
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	public boolean isBigCustomer() {
		return isBigCustomer;
	}
	
	public void setBigCustomer(boolean isBigCustomer) {
		this.isBigCustomer = isBigCustomer;
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getBigID() {
		return bigID;
	}

	public String getPaxType() {
		return paxType;
	}

	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	public void setBigID(int bigID) {
		this.bigID = bigID;
	}
	
	public void setDobString(String _dob){
		this.dateOfBirth = _dob;
	}
	
	public String getDateOfBirth(){
		return this.dateOfBirth;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setFrenId(String id) {
		if (id == null || id.length()<=0){
			this.gender = 0;
		}else{
			this.id = Integer.parseInt(id);
		}
	}

	
	public void setExpiredDate(String expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getIssuingCountry() {
		return issuingCountry;
	}

	public void setIssuingCountry(String issuingCountry) {
		this.issuingCountry = issuingCountry;
	}

	public String getRefUserId() {
		return refUserId;
	}

	public void setRefUserId(String refUserId) {
		this.refUserId = refUserId;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public void setSeq(String seq) {
		if(seq == null || seq.length()<=0){
			this.seq = 0;
		}else{
			this.seq = Integer.parseInt(seq);
		}
	}
	
	public String getFrenCID() {
		return frenCID;
	}

	public void setFrenCID(String frenCID) {
		this.frenCID = frenCID;
	}

	public int getPassangerNum() {
		return passangerNum;
	}

	public void setPassangerNum(int passangerNum) {
		this.passangerNum = passangerNum;
	}

	public boolean isInfant() {
		return isInfant;
	}

	public String getWheelChair() {
		return wheelChair;
	}

	public void setWheelChair(String wheelChair) {
		this.wheelChair = wheelChair;
	}

	public void setInfant(String isInfant) {
		this.isInfant = isInfant.equalsIgnoreCase("YES");
	}
	
	public void setGenderStr(String _gender){
		this.genderStr = _gender;
	}
	
	public String getGenderStr(){
		if (this.genderStr == null || this.genderStr.length()<=0){
			if (psgType == CONTACT){
				return "Mr";
			}
			return "Male";
		}
		return this.genderStr;
	}

	public int getBringInfant() {
		return bringInfant;
	}

	public void setBringInfant(int bringInfant) {
		this.bringInfant = bringInfant;
	}

	public List<List<CheckInModel>> getCheckinList() {
		return checkinList;
	}

	public void setCheckinList(List<List<CheckInModel>> checkinList) {
		this.checkinList = checkinList;
	}

	public boolean isCheckIn() {
		return checkIn;
	}

	public void setCheckIn(boolean checkIn) {
		this.checkIn = checkIn;
	}

	public boolean isReturn() {
		return isReturn;
	}

	public void setReturn(boolean isReturn) {
		this.isReturn = isReturn;
	}
	
	public boolean isDepart() {
		return isDepart;
	}

	public void setDepart(boolean isDepart) {
		this.isDepart = isDepart;
	}

	public void setInfant(boolean isInfant) {
		this.isInfant = isInfant;
	}

	public List<TravelDocModel> getDepartDoc() {
		return departDoc;
	}

	public void setDepartDoc(List<TravelDocModel> departDoc) {
		this.departDoc = departDoc;
	}

	public List<TravelDocModel> getReturnDoc() {
		return returnDoc;
	}

	public void setReturnDoc(List<TravelDocModel> returnDoc) {
		this.returnDoc = returnDoc;
	}

	public int getPsgType() {
		return psgType;
	}
	
	public boolean isHuman(){
		switch (psgType){
		case HEADER:
			return false;
		default:
			return true;
		}
	}
	
	public String getPsgTypeString(){
		
		switch (psgType){
		case 2:
			return CHD_TEXT;
		case 3:
			return INF_TEXT;
		default :
			return ADT_TEXT;
		}
		
	}

	public void setPsgType(int psgType) {
		this.psgType = psgType;
	}
	
	public void setPsgType(){
		if (paxType==null){
			psgType = ADULT;
			return;
		}
		if (paxType.equalsIgnoreCase("adult")){
			psgType = ADULT;
			return;
		}
		if (paxType.equalsIgnoreCase("child")){
			psgType = CHILD;
			return;
		}
		if (paxType.equalsIgnoreCase("infant")){
			psgType = INFANT;
			return;
		}
		
		psgType = ADULT;
		return;
	}
	
	public String getPsgTypeCode(){
		if (psgType == ADULT){
			return "ADT";
		}
		if (psgType == CHILD){
			return "CHD";
		}
		if (psgType == INFANT){
			return "INF";
		}
		
		return "ADT";
	}

	public boolean isContactPerson() {
		return isContactPerson;
	}

	public void setContactPerson(boolean isContactPerson) {
		this.isContactPerson = isContactPerson;
	}

	public int getPsgNumber() {
		return psgNumber;
	}

	public void setPsgNumber(int psgNumber) {
		this.psgNumber = psgNumber;
	}

	public boolean isQualified() {
		return isQualified;
	}

	public void setQualified(boolean isQualified) {
		this.isQualified = isQualified;
	}

	public float getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(float premiumAmount) {
		this.premiumAmount = premiumAmount;
	}

	public SSRItemModel getItemWheel() {
		return itemWheel;
	}

	public void setItemWheel(SSRItemModel itemWheel) {
		this.itemWheel = itemWheel;
	}
	
	public void addMeal(List<SSRItemModel> _meal, boolean isDepart){
		List<List<SSRItemModel>> itemMeal = null;
		if (isDepart){
			if (itemDepartMeal == null){
				itemDepartMeal = new ArrayList<List<SSRItemModel>>();
			}
			itemMeal = itemDepartMeal;
		}else{
			if (itemReturnMeal == null){
				itemReturnMeal = new ArrayList<List<SSRItemModel>>();
			}
			itemMeal = itemReturnMeal;
		}
		
		itemMeal.add(_meal);

	}
	
	public void addMeal(SSRItemModel _meal, boolean isDepart, int position){
		List<List<SSRItemModel>> itemMeal = null;
		if (isDepart){
			if (itemDepartMeal == null){
				itemDepartMeal = new ArrayList<List<SSRItemModel>>();
			}
			itemMeal = itemDepartMeal;
		}else{
			if (itemReturnMeal == null){
				itemReturnMeal = new ArrayList<List<SSRItemModel>>();
			}
			itemMeal = itemReturnMeal;
		}
		
		itemMeal.get(position).add(_meal);
		
	}
	
	public void removeMeal(String _mealKey, boolean isDepart, int position){
		List<List<SSRItemModel>> itemMeal = null;
		if (isDepart){
			itemMeal = itemDepartMeal;
		}else{
			itemMeal = itemReturnMeal;
		}
		
		List<SSRItemModel> mealList = itemMeal.get(position);
		for (int i=0; i < mealList.size();i++){
			if (mealList.get(i).getSsrCode()!=null &&
					mealList.get(i).getSsrCode().equals(_mealKey)){
//				SSRItemModel newMeal = new SSRItemModel();
//				mealList.set(i, newMeal);
				mealList.remove(i);
			}
		}
		
	}

	public List<List<SSRItemModel>> getMealList(boolean isDepart){
		if (isDepart){
			return itemDepartMeal;
		}else{
			return itemReturnMeal;
		}
	}
	
	public List<SSRItemModel> getMealItem(boolean isDepart, int position){
		List<List<SSRItemModel>> itemMeal = null;
		if (isDepart){
			itemMeal = itemDepartMeal;
		}else{
			itemMeal = itemReturnMeal;
		}
		
		if (itemMeal == null){
			return null;
		}
		return itemMeal.get(position);
	}
	
	public void setItemDepartBaggage(List<SSRItemModel> _itemDepartBaggage) {
		this.itemDepartBaggage = _itemDepartBaggage;
	}

	public void setItemReturnBaggage(List<SSRItemModel> _itemReturnBaggage) {
		this.itemReturnBaggage = _itemReturnBaggage;
	}

	public List<SSRItemModel> getBaggage(boolean isDepart){
		if (isDepart){
			return this.itemDepartBaggage;
		}else{
			return this.itemReturnBaggage;
		}
	}

	public int getrBaggagePos() {
		return rBaggagePos;
	}

	public void setrBaggagePos(int rBaggagePos) {
		this.rBaggagePos = rBaggagePos;
	}

	public int getdBaggagePos() {
		LogHelper.debug("Baggatge Pos " + dBaggagePos);
		return dBaggagePos;
	}

	public void setdBaggagePos(int dBaggagePos) {
		this.dBaggagePos = dBaggagePos;
	}
	
	public void increseDBaggagePos() {
		this.dBaggagePos += 1;
	}
	
	public void decreseDBaggagePos() {
		this.dBaggagePos -= 1;
	}
	
	public void increseRBaggagePos() {
		this.rBaggagePos += 1;
	}
	
	public void decreseRBaggagePos() {
		this.rBaggagePos -= 1;
	}
	
	public double getTotalBaggageCharge(){
		double total = 0.00;
		if (itemDepartBaggage!=null){
			for (int i = 0 ; i < itemDepartBaggage.size(); i++){
				total += itemDepartBaggage.get(i).getChargesTotal();
			}
		}
		if (itemReturnBaggage!=null){
			for (int i = 0; i < itemReturnBaggage.size();i++){
				total += itemReturnBaggage.get(i).getChargesTotal();
			}
		}
		
		return total;
	}
	
	public int checkEmergencyContactValid(){
		String eName = getfName();
		String eLName = getlName();
		String eMobile = getMobileNo();
		String eRelation = getUserName();
		if (eName != null &&eName.length()>0){
			if (eLName != null &&eLName.length()>0){
				if (eMobile != null && eMobile.length()>0){
					if (eRelation!=null && eRelation.length()>0){
						return 1;
					}else{
						return 2;
					}
				}else{
					return 2;
				}
			}else{
				return 2;
			}
		}else if((eName == null || eName.length()<=0) &&
				((eLName != null &&eLName.length()>0) || 
						(eMobile != null && eMobile.length()>0) || 
						(eRelation!=null && eRelation.length()>0))){
			return 2;
			
		}
		return 0;
	}

	public double getTotalMealAmount(){
		double total = 0.00;
		if (itemDepartMeal != null && itemDepartMeal.size()>0){
			for (int i = 0 ; i < itemDepartMeal.size() ; i ++){
				if (itemDepartMeal!=null && itemDepartMeal.get(i).size()>0){
					List<SSRItemModel> mealList = itemDepartMeal.get(i);
					for (int childIndex = 0; childIndex < mealList.size(); childIndex++){
						total += mealList.get(childIndex).getAmountTotal();
						List<MealBundleModel> bundleList = mealList.get(childIndex).getBundleList();
						if (bundleList!=null && bundleList.size()>0){
							for (int iBundle = 0; iBundle< bundleList.size();iBundle++){
								total+=bundleList.get(iBundle).getAmountTotal();
							}
						}
					}
				}
			}
		}

		if (itemReturnMeal != null && itemReturnMeal.size()>0){
			for (int i = 0 ; i < itemReturnMeal.size() ; i ++){
				if (itemReturnMeal!=null && itemReturnMeal.get(i).size()>0){
					List<SSRItemModel> mealList = itemReturnMeal.get(i);
					for (int childIndex = 0; childIndex < mealList.size(); childIndex++){
						total += mealList.get(childIndex).getAmountTotal();
						List<MealBundleModel> bundleList = mealList.get(childIndex).getBundleList();
						if (bundleList!=null && bundleList.size()>0){
							for (int iBundle = 0; iBundle< bundleList.size();iBundle++){
								total+=bundleList.get(iBundle).getAmountTotal();
							}
						}
					}
				}
			}
		}
		return total;
	}

	public List<SSRItemModel> getItemBaggage(boolean isDepart){
		if (isDepart){
			return itemDepartBaggage;
		}else{
			return itemReturnBaggage;
		}
	}

	public List<SSRItemModel> getItemDepartBaggage() {
		return itemDepartBaggage;
	}

	public List<SSRItemModel> getItemReturnBaggage() {
		return itemReturnBaggage;
	}

	public List<List<SSRItemModel>> getItemMeal(boolean isDepart){
		if (isDepart){
			return itemDepartMeal;
		}else{
			return itemReturnMeal;
		}
	}

	public List<List<SSRItemModel>> getItemDepartMeal() {
		return itemDepartMeal;
	}

	public List<List<SSRItemModel>> getItemReturnMeal() {
		return itemReturnMeal;
	}
}
