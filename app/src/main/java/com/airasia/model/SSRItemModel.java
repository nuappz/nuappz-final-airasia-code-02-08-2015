package com.airasia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SSRItemModel implements Serializable{

	String ssrCode, available, feeCode,
			ssrNumber, descripion, ssrName, feeNumber;
	double amountTotal;
	int baggageOrder;
	List<ChargeModel> charges;
	String image, category;
	boolean isDepart = false;
	List<MealBundleModel> bundleList;

	public String getSsrCode() {
		return ssrCode;
	}
	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}
	public String getAvailable() {
		return available;
	}
	public void setAvailable(String available) {
		this.available = available;
	}


	public String getFeeCode() {
		return feeCode;
	}
	public void setFeeCode(String feeCode) {
		this.feeCode = feeCode;
	}
	public String getSsrNumber() {
		return ssrNumber;
	}
	public void setSsrNumber(String ssrNumber) {
		this.ssrNumber = ssrNumber;
	}
	public String getDescripion() {
		return descripion;
	}
	public void setDescripion(String descripion) {
		this.descripion = descripion;
	}
	public double getAmountTotal() {
		return amountTotal;
	}
	public void setAmountTotal(double amountTotal) {
		this.amountTotal = amountTotal;
	}
	public List<ChargeModel> getCharges() {
		return charges;
	}
	public void setCharges(List<ChargeModel> charges) {
		this.charges = charges;
	}
	public int getBaggageOrder() {
		return baggageOrder;
	}
	public void setBaggageOrder(int _baggageOrder) {
		this.baggageOrder = _baggageOrder;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getSsrName() {
		return ssrName;
	}
	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}
	public String getFeeNumber() {
		return feeNumber;
	}
	public void setFeeNumber(String feeNumber) {
		this.feeNumber = feeNumber;
	}

	public double getChargesTotal(){
		double total = 0.00;
		if (charges != null && charges.size()>0){
			for (int i = 0; i < charges.size(); i ++){
				total +=charges.get(i).getAmount();
			}
			return total;
		}else{
			return total;
		}
	}

	public boolean isDepart() {
		return isDepart;
	}

	public void setDepart(boolean isDepart) {
		this.isDepart = isDepart;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public List<MealBundleModel> getBundleList() {
		if (bundleList == null){
			return new ArrayList<MealBundleModel>();
		}
		return bundleList;
	}

	public void setBundleList(List<MealBundleModel> bundleList) {
		this.bundleList = bundleList;
	}

	public void addBundle(MealBundleModel _bundle){
		if(this.bundleList == null){
			this.bundleList = new ArrayList<MealBundleModel>();
		}
		this.bundleList.add(_bundle);
	}

	public void removeBundle(String ssrCode){
		if (this.bundleList == null || this.bundleList.size()<=0){
			return;
		}

		for (int i = 0; i < bundleList.size(); i ++){
			if(bundleList.get(i).getSsrCode().equals(ssrCode)){
				bundleList.remove(i);
			}
		}
	}



}
