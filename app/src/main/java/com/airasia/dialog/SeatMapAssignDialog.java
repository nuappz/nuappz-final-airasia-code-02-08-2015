package com.airasia.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airasia.adapter.SeatAssignAdapter;
import com.airasia.mobile.R;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.SeatMapModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by NUappz-5 on 7/27/2015.
 */
public class SeatMapAssignDialog extends Dialog {


    public finishCallBack callback;

    Context context;
    ListView list_view_list;

    ArrayList<MemberInfoModel> data = new ArrayList<MemberInfoModel>();
    SeatAssignAdapter adapter;
    Button button1;


    TextView seatNoTextView;
    TextView rateTextView;

    RelativeLayout rlProgress;

    SeatMapModel seatModel;

    public SeatMapAssignDialog(Context ctx) {
        super(ctx);
        this.context = ctx;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asign_seat);
        list_view_list =(ListView)findViewById(R.id.customerNameList);
//        button1 = (Button) findViewById(R.id.assignButton);
//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (callback!=null)
//                {
//                    callback.passengerDidPressed(obj);
//                }
//
//                dismiss();
//            }
//        });

        seatNoTextView = (TextView) findViewById(R.id.seatNo);
        rateTextView = (TextView) findViewById(R.id.rate);

        list_view_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {

                if (callback!=null)
                {

                    callback.passengerDidPressed(data.get(position),seatModel,true);
                }

                dismiss();

//                View view = list_view_list.getChildAt(position);
//                int desiredBackgroundColor = Color.rgb(255, 48, 48);
//                ColorDrawable viewColor = (ColorDrawable) view.getBackground();
//                if (viewColor == null) {
////                    Intent i = new Intent(SeatAssignActivity.this, SeatAssignActivity.class);
////                    startActivity(i);
//                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
//                    view.setBackgroundColor(desiredBackgroundColor);
//                    return;
//                }
//                int currentColorId = viewColor.getColor();
//                if (currentColorId == desiredBackgroundColor) {
//                    view.setBackgroundColor(Color.TRANSPARENT);
//                } else {
////                    Intent i = new Intent(SeatAssignActivity.this, SeatAssignActivity.class);
////                    startActivity(i);
//                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
//                    view.setBackgroundColor(desiredBackgroundColor);
//                }
//
//                list_view_list.getChildAt(position).setBackgroundColor(Color.RED);
//                Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();

            }
        });

    }

       /* oldPassword = (EditText) findViewById(R.id.editTextOldPassword);
        newPassword = (EditText) findViewById(R.id.editTextNewPassword);
        confirmPassword = (EditText) findViewById(R.id.editTextConfirmPassword);
        rlProgress = (RelativeLayout) findViewById(R.id.rlProgress);
        save = (FButton) findViewById(R.id.buttonSave);
        preference = new MySharedPreference(context);
        cd = new ConnectionDetector(context);
        oldPassword.setText("");
        newPassword.setText("");
        confirmPassword.setText("");*/


    public void resetList(SeatMapModel model, List<MemberInfoModel> list) {
        data.clear();

        seatModel = model;


        if (model!=null)
        {
            seatNoTextView.setText(model.seatDesignator + " " + "seat");
            rateTextView.setText(""+model.fare+ " MYR");
        }





        for (MemberInfoModel passanger : list)
        {
            if (passanger.getPsgType()!=MemberInfoModel.INFANT)
            {
                data.add(passanger);
            }
        }

        adapter = new SeatAssignAdapter(context, R.layout.assign_seat_content, data);
        list_view_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public interface finishCallBack
    {
        public void passengerDidPressed(MemberInfoModel obj,SeatMapModel model,boolean assign);
    }
}