package com.airasia.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.airasia.adapter.SeatRejectAdapter;
import com.airasia.mobile.R;
import com.airasia.model.SeatRejectModel;

import java.util.ArrayList;

/**
 * Created by NUappz-2 on 7/27/2015.
 */
public class SeatMapRejectDialog extends Dialog {

    Context context;
    ListView list_view_list;

    EditText oldPassword, newPassword, confirmPassword;
    ArrayList<SeatRejectModel> data = new ArrayList<SeatRejectModel>();
    SeatRejectAdapter adapter;
    Button button1;


    RelativeLayout rlProgress;

    public SeatMapRejectDialog(Context ctx) {
        super(ctx);
        this.context = ctx;
        // TODO Auto-generated constructor stub
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seat_reject);
        list_view_list = (ListView) findViewById(R.id.customerNameList);
        button1 = (Button) findViewById(R.id.rejectButton);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        setList();
        list_view_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3) {
                View view = list_view_list.getChildAt(position);
                int desiredBackgroundColor = Color.rgb(255, 48, 48);
                ColorDrawable viewColor = (ColorDrawable) view.getBackground();
                if (viewColor == null) {
//                    Intent i = new Intent(SeatAssignActivity.this, SeatAssignActivity.class);
//                    startActivity(i);
                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                    view.setBackgroundColor(desiredBackgroundColor);
                    return;
                }
                int currentColorId = viewColor.getColor();
                if (currentColorId == desiredBackgroundColor) {
                    view.setBackgroundColor(Color.TRANSPARENT);
                } else {
//                    Intent i = new Intent(SeatAssignActivity.this, SeatAssignActivity.class);
//                    startActivity(i);
                    Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                    view.setBackgroundColor(desiredBackgroundColor);
                }

                list_view_list.getChildAt(position).setBackgroundColor(Color.RED);
                Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void setList() {
        data.clear();
        data.add(new SeatRejectModel("Lim Soo Ling"));
        data.add(new SeatRejectModel("Harina Elaine kaur A/P Harji Singh"));
        data.add(new SeatRejectModel("Koo Liew Xing"));
        data.add(new SeatRejectModel("Harina Elaine"));
        data.add(new SeatRejectModel("Lim Soo Ling"));
        data.add(new SeatRejectModel("Harji Singh"));
        adapter = new SeatRejectAdapter(context, R.layout.seat_reject_content, data);
        list_view_list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}