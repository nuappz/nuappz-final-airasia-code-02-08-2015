package com.airasia.util;

import android.content.Context;

import com.airasia.holder.ConstantHolder;
import com.airasia.model.BannerModel;
import com.airasia.model.BookingInfoModel;
import com.airasia.model.ChargeModel;
import com.airasia.model.CheckInModel;
import com.airasia.model.CountryModel;
import com.airasia.model.FareClassUpgrade;
import com.airasia.model.FareModel;
import com.airasia.model.FareTaxesModel;
import com.airasia.model.FareTypeModel;
import com.airasia.model.FlightDetailsModel;
import com.airasia.model.FlightSearchModel;
import com.airasia.model.FlightsModel;
import com.airasia.model.JourneyModel;
import com.airasia.model.MealBundleModel;
import com.airasia.model.MemberInfoModel;
import com.airasia.model.PassportModel;
import com.airasia.model.PayMethodModel;
import com.airasia.model.PaymentOptionModel;
import com.airasia.model.SSRContainer;
import com.airasia.model.SSRItemModel;
import com.airasia.model.SSRMainModel;
import com.airasia.model.SSRModel;
import com.airasia.model.SearchInfoModel;
import com.airasia.model.SeatSummary;
import com.airasia.model.SegmentLegs;
import com.airasia.model.SegmentModel;
import com.airasia.model.StationModel;
import com.airasia.model.TravelDocModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class JSonHelper {
	
	public static int SUCCESS_REPONSE = 1000;

	public static String JSonString(String json, String tag){
		JSONObject jObject;
		String result = "";
		if (json == null){
			return result;
		}
		try {
			jObject = new JSONObject(json);
		
			result = jObject.getString(tag);
			if (result == null || result.length()<0){
				return "";
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static int JSonInt(String json, String tag){
		JSONObject jObject;
		int result = 0;
		if (json == null){
			return result;
		}
		try {
			jObject = new JSONObject(json);
		
			result = jObject.getInt(tag);
			if (result == 0 ){
				return 0;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static int GetResponseStatus(String json){
		int responseCode = 0;
		String response = JSonString(json, "response");
		responseCode = JSonInt(response, "responseCode");
		return responseCode;
	}
	
	public static String GetResponseMessage(String json){
		String responseMessage = "";
		String response = JSonString(json, "response");
		responseMessage = JSonString(response, "responseMessage");
		return responseMessage;
	}
	
	public static JSONObject getData(String response){
		try{
			JSONObject obj = new JSONObject(response);
			JSONObject data = obj.getJSONObject("data");
			return data;
		}catch (Exception e){
			e.printStackTrace();
			return new JSONObject();
		}
	}
	
	public static List<BannerModel> parseBanner(String json){
		List<BannerModel> bannerList = new ArrayList<BannerModel>();
		try {
			String data = JSonString(json, "data");
			JSONArray jarray = new JSONArray(JSonString(data, "banners"));
			for (int i = 0; i < jarray.length(); i ++){
				JSONObject jObj = jarray.getJSONObject(i);
				BannerModel banner = new BannerModel();
				banner.setId(jObj.getInt("bannerID"));
				banner.setUrl(jObj.getString("bannerUrl"));
				banner.setLastModeified(jObj.getInt("lastModified"));
				bannerList.add(banner);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ArrayList<BannerModel>();
		}
		
		return bannerList;
	}
	
	public static List<CountryModel> parseCountryList(String json){
		List<CountryModel> countryList = new ArrayList<CountryModel>();
		try {
			
			
			String data = JSonString(json, "data");
			String countryString =JSonString(data, "countryList");
			JSONArray jArr = new JSONArray(countryString.trim());
			
			for (int i = 0 ; i < jArr.length(); i++){
				JSONObject childObj = jArr.getJSONObject(i);
				CountryModel item = new CountryModel(childObj.getString("CountryName")
						,childObj.getString("CountryCode")
						,childObj.getString("DialingCode"));
				countryList.add(item);
			}

//			while( keys.hasNext() ) {
//			    String key = (String)keys.next();
//			    if ( jObject.get(key) instanceof JSONObject ) {
//			    	JSONObject childObj = jObject.getJSONObject(key);
//		    		CountryModel item = new CountryModel(childObj.getString("CountryName")
//		    									,key
//		    									,childObj.getString("DialingCode"));
//		    		countryList.add(item);
//			    }
//			}
//			JSONObject jObj = new JSONObject(countryString);
//			LogHelper.debug("Start parse Country " + jObj.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return countryList;
	}

	public static List<SSRModel> praseSSRList(String json){
		List<SSRModel> ssrList = new ArrayList<SSRModel>();
		try {
			JSONObject jsonObj = new JSONObject(json);
			JSONObject jdata = jsonObj.getJSONObject("data");
			JSONArray jArr = jdata.getJSONArray("ssrTranslateList");
			
			for (int i = 0 ; i < jArr.length(); i++){
				JSONObject childObj = jArr.getJSONObject(i);
				SSRModel item = new SSRModel();
				item.setDescriptio(childObj.getString("Description"));
				item.setSsrCode(childObj.getString("SSRCode"));
				ssrList.add(item);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ssrList;
	}
	
	public static List<FareClassUpgrade> praseFareClassList(JSONObject jdata){
		List<FareClassUpgrade> fareCList = new ArrayList<FareClassUpgrade>();
		try {
			JSONObject jObj = jdata.getJSONObject("fareClass");
			
			for (int i = 0 ; i < 3; i++){
				JSONObject childObj = jObj.getJSONObject(String.valueOf(i));
				FareClassUpgrade item = new FareClassUpgrade();
				item.setKey(i);
				String title = childObj.getString("class_title");
				String upgradeTitle = "";
				
				if (childObj.has("upgrade_title")){
					upgradeTitle = childObj.getString("upgrade_title");
				}
				
				if (title!=null && title.length()>0){
					item.setTitle(title);
				}else{
					item.setTitle("");
				}
				
				if (upgradeTitle!=null && upgradeTitle.length()>0){
					item.setUpgrade_title(upgradeTitle);
				}else{
					item.setUpgrade_title("");
				}

				List<String> benefits = new ArrayList<String>();
				if (childObj.has("benefit")){
					JSONArray benefitArr = childObj.getJSONArray("benefit");
					if (benefitArr!=null && benefitArr.length()>0){
						for (int x = 0; x <benefitArr.length(); x++ ){
							benefits.add(benefitArr.getString(x));
						}
					}
				}
				
				item.setBenefits(benefits);
				fareCList.add(item);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fareCList;
	}
	
	public static List<CountryModel> parseStateList(JSONObject json){
		List<CountryModel> countryList = new ArrayList<CountryModel>();
		try {
			
			JSONArray resultArray = json.getJSONArray("result");
			
			for (int i = 0 ; i <resultArray.length(); i ++){
				JSONObject childObj = resultArray.getJSONObject(i);
				CountryModel item = new CountryModel(
						childObj.getString("ProvinceStateName"),
										childObj.getString("CountryCode"),
										"",
										childObj.getString("ProvinceStateCode"));
				countryList.add(item);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return countryList;
	}
	
	public static List<StationModel> parseStationList(String json){
		List<StationModel> stationList = new ArrayList<StationModel>();
		try {
			
			
			String data = JSonString(json, "data");
			String countryString =JSonString(data, "stationList");
//			JSONObject jObject = new JSONObject(countryString.trim());
			JSONArray jArr = new JSONArray(countryString.trim());
			for (int i = 0 ; i < jArr.length() ; i++){
		    	JSONObject childObj = jArr.getJSONObject(i);
		    	StationModel item = new StationModel(childObj.getString("StationCode")
						,childObj.getString("StationName")
						,childObj.getString("CountryName")
						,childObj.getString("AirportName"));
		    		stationList.add(item);
			}
			
//			Iterator<?> keys = jObject.keys();
//			while( keys.hasNext() ) {
//			    String key = (String)keys.next();
//			    if ( jObject.get(key) instanceof JSONObject ) {
//			    	JSONObject childObj = jObject.getJSONObject(key);
//		    		StationModel item = new StationModel(key
//		    									,childObj.getString("StationName")
//		    									,childObj.getString("CountryName")
//		    									,childObj.getString("AirportName"));
//		    		stationList.add(item);
//			    }
//			}

//			JSONObject jObj = new JSONObject(countryString);
//			LogHelper.debug("Start parse Country " + jObj.toString());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return stationList;
	}
	
	public static PassportModel parsePassport(JSONObject obj){
		PassportModel model = new PassportModel();
		
		try{
			model.setCountryCode(obj.getString("PassportIssueCountry"));
			model.setDate(obj.getString("ExpiryDate"));
			model.setNumber(obj.getString("PassportNumber"));
			model.setType(obj.getString("PassportType"));
			model.setUserName(obj.getString("username"));
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return model;
	}
	

	public static List<MemberInfoModel> parseFrenFamily(String response){
		List<MemberInfoModel> items = new ArrayList<MemberInfoModel>();
		
		try{
			JSONObject jsonResponse = new JSONObject(response);
			JSONObject data = jsonResponse.getJSONObject("data");
			JSONArray resultArray = new JSONArray(data.getString("result"));
			for (int i = 0 ; i <resultArray.length(); i ++){
				JSONObject childObj = resultArray.getJSONObject(i);
				MemberInfoModel item = new MemberInfoModel();
				item.setFrenCID(childObj.getString("CustomerNumber"));
				item.setDob(childObj.getString("DOB"));
				item.setPassport(childObj.getString("DocNo"));
				item.setExpiredDate(childObj.getString("ExpirationDate"));
				item.setfName(childObj.getString("FirstName"));
				item.setFrenGender(childObj.getString("Gender"));
				item.setFrenId(childObj.getString("Id"));
				item.setIssuingCountry(childObj.getString("IssuingCountry"));
				item.setlName(childObj.getString("LastName"));
				item.setNationality(childObj.getString("Nationality"));
				item.setPaxType(childObj.getString("PaxType"));
				item.setRefUserId(childObj.getString("RefUserId"));
				item.setSeq(childObj.getString("Seq"));
				item.setTitle(childObj.getString("Title"));
				item.setUserName(childObj.getString("UserId"));
				items.add(item);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return items;
	}
	
	public static BookingInfoModel parseBookingInfo(String bookingData, Context c){
		BookingInfoModel item = new BookingInfoModel();
		List<JourneyModel> returnJourney = new ArrayList<JourneyModel>();
		List<JourneyModel> departJourney = new ArrayList<JourneyModel>();
		
		try{
			JSONObject bookData = new JSONObject(bookingData);
			if (bookData == null || bookData.length()<0){
				return item;
			}
			
			JSONObject booking = bookData.getJSONObject("Booking");
			
			if (booking == null || booking.length()<0){
				return item;
			}
			
			item.setId(booking.getString("BookingID"));
			item.setRecordLocator(booking.getString("RecordLocator"));
			item.setCurrencyCode(booking.getString("CurrencyCode"));
			item.setBalanceDue(Float.parseFloat(booking.getString("BalanceDue")));
			item.setTotalCost(Float.parseFloat(booking.getString("TotalCost")));
			item.setPaxCount(booking.getInt("PaxCount"));
			item.setAuthorizedBalanceDue(booking.getInt("AuthorizedBalanceDue"));
			item.setSegmentCount(booking.getInt("SegmentCount"));
			item.setPaidStatus(booking.getString("PaidStatus"));
			item.setBookingStatus(booking.getString("BookingStatus"));
			item.setExpiredDate(booking.getString("ExpiredDate"));
			item.setLastSegmentSTD(booking.getString("LastSegmentSTD"));
			item.setTotalJourney(booking.getInt("TotalJourney"));
			if (booking.has("ConnectingFlights")){
				item.setConnectingFlights(booking.getInt("ConnectingFlights"));
			}
			item.setBookingType(booking.getString("BookingType"));
			
			JSONArray journeyArr = bookData.getJSONArray("JourneyDisplay");
			if (journeyArr.length()>=1){
				JSONArray jChildArr = journeyArr.getJSONArray(0);
				for (int cjs = 0 ; cjs < jChildArr.length(); cjs++){;
					JourneyModel jourItem = new JourneyModel();
					LogHelper.debug("Depart Journey" + cjs + " = " + jChildArr.get(cjs).toString());
					jourItem.setName(jChildArr.get(cjs).toString());
					departJourney.add(jourItem);
				}
				
				item.setDepartJourney(departJourney);
			}
			
			if (journeyArr.length()>=2){
				JSONArray jChildArr = journeyArr.getJSONArray(1);
				for (int cjs = 0 ; cjs < jChildArr.length(); cjs++){;
					JourneyModel jourItem = new JourneyModel();
					LogHelper.debug("Return Journey" + cjs + " = " + jChildArr.get(cjs).toString());
					jourItem.setName(jChildArr.get(cjs).toString());
					returnJourney.add(jourItem);
				}

				item.setReturnJourney(returnJourney);
			}
			
			if (bookData.has("Fare")){
				SQLhelper db = new SQLhelper(c);
				List<FareTaxesModel> taxList = db.getFareTaxesList();
				JSONArray fareArr = bookData.getJSONArray("Fare");
				for (int fi = 0; fi < fareArr.length(); fi++){

					HashMap<String, List<FareTaxesModel>> taxMap = new HashMap<String, List<FareTaxesModel>>();
					JSONObject childObj = fareArr.getJSONObject(fi);	
					
					if (childObj.has("ADT")){
						List<FareTaxesModel> temp = new ArrayList<FareTaxesModel>();
						JSONObject taxObj = childObj.getJSONObject("ADT");
						temp = paraseFareTax(taxObj, taxList);
						taxMap.put("ADT", temp);
					}
					if (childObj.has("CHD")){
						List<FareTaxesModel> temp = new ArrayList<FareTaxesModel>();
						JSONObject taxObj = childObj.getJSONObject("CHD");
						temp = paraseFareTax(taxObj, taxList);
						taxMap.put("CHD", temp);
					}
					if (childObj.has("INF")){
						List<FareTaxesModel> temp = new ArrayList<FareTaxesModel>();
						JSONObject taxObj = childObj.getJSONObject("INF");
						temp = paraseFareTax(taxObj, taxList);
						taxMap.put("INF", temp);
					}
					
					if (fi == 0){
						item.setDepartTaxes(taxMap);
					}else{
						item.setReturnTaxes(taxMap);
					}
				}
				db.close();
				
			}
			
			
			JSONArray segmentJArr = bookData.getJSONArray("Journey");
			List<SegmentModel> departSegment = parseSegment(segmentJArr.getJSONObject(0), c);
			if (bookData.has("SeatSummary")){
				for (int segNum = 0 ; segNum < departSegment.size(); segNum++){
					JSONObject departSeatJson = bookData.getJSONObject("SeatSummary");
					JSONArray departArr = departSeatJson.getJSONArray("Depart");
					List<SeatSummary> seatList = parseSeatSummary(departArr.getJSONArray(segNum));
					departSegment.get(segNum).setSeatList(seatList);
					
				}
			}
			item.setDepartSegments(departSegment);
			
			for(int ds = 0 ; ds <departSegment.size(); ds ++){
				if (departSegment.get(ds).isInternational()){
					item.setInternational(true);
					break;
				}
			}
			
			if (segmentJArr.length()>1){
				List<SegmentModel> returnSegment = parseSegment(segmentJArr.getJSONObject(1),	 c);
				item.setReturnSegments(returnSegment);

				if (bookData.has("SeatSummary")){
					for (int segNum = 0 ; segNum < departSegment.size(); segNum++){
						JSONObject returnSeatJson = bookData.getJSONObject("SeatSummary");
						JSONArray returnArr = returnSeatJson.getJSONArray("Return");
						List<SeatSummary> seatList = parseSeatSummary(returnArr.getJSONArray(segNum));
						returnSegment.get(segNum).setSeatList(seatList);
					}
				}
				
				for(int rs = 0 ; rs <returnSegment.size();  rs++){
					if (returnSegment.get(rs).isInternational()){
						item.setInternational(true);
						break;
					}
				}
				
			}
			
			if (bookData.has("OverallPaxCount")){
				JSONObject jPax = bookData.getJSONObject("OverallPaxCount");
				item.setAdultCount(jPax.getInt("ADT"));
				item.setChildCount(jPax.getInt("CHD"));
				item.setInfantCount(jPax.getInt("INF"));
				
			}
			
			
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return item;
	}
	
	public static List<SegmentModel> parseSegment(JSONObject segmentData, Context c){
		List<SegmentModel> segList = new ArrayList<SegmentModel>();
		SQLhelper db = new SQLhelper(c);
		try {
			JSONArray childArr = segmentData.getJSONArray("Segment");
			for (int i = 0; i < childArr.length(); i ++){
				JSONObject childObj = childArr.getJSONObject(i);
				SegmentModel item = new SegmentModel();
				LogHelper.debug("Segment Json " + childObj.toString());
				item.setDateSTA(childObj.getString("STA"));
				item.setDataSTD(childObj.getString("STD"));
				item.setInternational(childObj.getBoolean("International"));
				item.setArriveStation(childObj.getString("ArrivalStation"));
				item.setDepartStation(childObj.getString("DepartureStation"));
				item.setCarrierCode(childObj.getString("CarrierCode"));
				item.setFlgithNum(childObj.getString("FlightNumber"));
				item.setDateSTD4(childObj.getString("STD_4hr"));
				item.setDateSTDCHECK(childObj.getString("STD_CheckIn"));
				item.setDataSTD24(childObj.getString("STD_24hr"));
				item.setArrTimeZone(childObj.getString("ArrivalTimeZoneCode"));
				item.setDepartTimeZone(childObj.getString("DepartureTimeZoneCode"));
				item.setFareProduct(childObj.getString("FareProductClass"));
				item.setSegmentType(childObj.getInt("SegmentType"));
				item.setEstimateTime(childObj.getString("EstimatedTime"));
				if (childObj.has("TotalSegment")){
					item.setTotalSegment(childObj.getInt("TotalSegment"));
				}
				item.setFareType(childObj.getInt("FareTypeIdentifier"));
				StationModel dStation = db.getStationBaseKey(item.getDepartStation(), 0);
				item.setDepartItem(dStation);
				
				StationModel rStation = db.getStationBaseKey(item.getDepartStation(), 0);
				item.setReturnItem(rStation);

				if (childObj.has("Legs")){
					JSONObject legObj = childObj.getJSONObject("Legs");
					SegmentLegs segLeg = new SegmentLegs();
					segLeg.setDepartStation(legObj.getString("DepartureStation"));
					segLeg.setArrivalStation(legObj.getString("ArrivalStation"));
					segLeg.setSTD(legObj.getString("STD"));
					segLeg.setSTA(legObj.getString("STA"));
					segLeg.setCarrierCode(legObj.getString("CarrierCode"));
					segLeg.setFlightNum(legObj.getString("FlightNumber"));
					item.setSegLegs(segLeg);
				}
				
				segList.add(item);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			db.close();
		}
		
		db.close();
		return segList;
	}
	
	public static List<SeatSummary> parseSeatSummary(JSONArray childArr){
		List<SeatSummary> segList = new ArrayList<SeatSummary>();
		try {
			for (int i = 0; i < childArr.length(); i ++){
				JSONObject childObj = childArr.getJSONObject(i);
				SeatSummary item = new SeatSummary();
				LogHelper.debug("seat Summary " + childObj.getString("name")
						+ " seat " + childObj.getString("seatDesignator"));
//				item.setCurrency(childObj.getString("currency"));
				item.setEquipType(childObj.getString("EquipmentType"));
				item.setEquipSuffix(childObj.getString("EquipmentTypeSuffix"));
				item.setPRBCode(childObj.getString("PRBCCode"));
//				item.setpName(childObj.getString("name"));
//				item.setPrice(childObj.getString("price"));
				item.setSeatNum(childObj.getString("seatDesignator"));
				item.setTypeOfSeat(childObj.getString("typeOfSeat"));
				segList.add(item);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return segList;
	}
	
	public static List<MemberInfoModel> parsePassenger(String bookingData){
		List<MemberInfoModel> passengerList = new ArrayList<MemberInfoModel>();
		try {
			JSONObject bookData = new JSONObject(bookingData);
			JSONArray passengerArr = bookData.getJSONArray("PassengerInfo");
			for (int passX = 0 ; passX < passengerArr.length() ; passX ++){
				JSONObject childPass;
				
				childPass = passengerArr.getJSONObject(passX);
				MemberInfoModel passenger = new MemberInfoModel();
				passenger.setPid(childPass.getInt("PassengerID"));
				String psgNum = childPass.getString("PassengerNumber");
				if (psgNum!= null && psgNum.length()>0){
					int value = Integer.parseInt(psgNum);
					passenger.setPassangerNum(value);
				}
				passenger.setInfant(childPass.getString("IsInfant"));
				passenger.setPaxType(childPass.getString("PaxType"));
				passenger.setPsgType();
				passenger.setfName(childPass.getString("FirstName"));
				passenger.setlName(childPass.getString("LastName"));
				passenger.setDob(childPass.getString("DOB"));
				passenger.setTitle(childPass.getString("Title"));
				passenger.setNationality(childPass.getString("Nationality"));
				passenger.setGenderStr(childPass.getString("Gender"));
				passenger.setBringInfant(childPass.getInt("BringInfant"));
				passenger.setWheelChair(childPass.getString("HaveWheelchair"));
				passengerList.add(passenger);
				
				List<List<CheckInModel>> checkinList = new ArrayList<List<CheckInModel>>();
				
				if (childPass.has("CheckinStatus")){
					JSONArray jsonArray = childPass.getJSONArray("CheckinStatus");
					for (int i = 0 ; i < jsonArray.length(); i ++){
						JSONArray childArray = jsonArray.getJSONArray(i);
						List<CheckInModel> list = new ArrayList<CheckInModel>();
						for (int childId = 0 ; childId < childArray.length(); childId++){
							JSONObject statusObj = childArray.getJSONObject(childId);
							CheckInModel cModel = new CheckInModel();
							cModel.setLiftStatus(statusObj.getString("LiftStatus"));
							cModel.setBoardingSequence(statusObj.getInt("BoardingSequence"));
							list.add(cModel);
						}
						checkinList.add(list);
					}
				}
				
				passenger.setCheckinList(checkinList);
				
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return passengerList;
	}
	
	public static String parseBarCode(String response){
		String result="";
		try{
			JSONObject json = new JSONObject(response);
			JSONObject dataObj = json.getJSONObject("data");
			result = dataObj.getString("barcode");
			
		}catch (Exception e){
			e.printStackTrace();
			return "";
		}
		return result;
	}
	
	public static String removeNull(byte[] responseBye){
		String response = new String(responseBye);
		response = response.replace("null", "\"\"");
		return response;
		
	}
	
	public static void parseTravelDocStatus(String response, BookingInfoModel booking){

		try{
			JSONObject json = new JSONObject(response);
			JSONArray dataArr = json.getJSONArray("data");
			List<MemberInfoModel> members = booking.getMemberList();
			
			for (int jSize = 0; jSize < dataArr.length(); jSize++){
				JSONArray jourArr = dataArr.getJSONArray(jSize);
				for (int sSize = 0 ; sSize < jourArr.length(); sSize++){
					JSONArray segArr = jourArr.getJSONArray(sSize);
					for (int cSize = 0 ; cSize < segArr.length(); cSize++){
						JSONObject childObj = segArr.getJSONObject(cSize);
						int passNum = childObj.getInt("PassengerNumber");
						TravelDocModel doc = new TravelDocModel();
						doc.setMsg(childObj.getString("verifyTravelDocMsg"));
						doc.setStatus(childObj.getString("verifyTravelDocStatus"));
						for (int i = 0; i <members.size();i++){
							if (members.get(i).getPassangerNum() == passNum){
								if (members.get(i).isDepart()){
									booking.getMemberList().get(cSize).getDepartDoc().add(doc);
								}else{
									booking.getMemberList().get(cSize).getReturnDoc().add(doc);
								}
							}
						}
					}
				}
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static List<FlightsModel> parseFlightInfo(JSONObject data){
		List<FlightsModel> items = new ArrayList<FlightsModel>();
		
		try{
			JSONArray result = data.getJSONArray("result");
			for (int i = 0 ; i <result.length(); i ++){
				JSONObject resultObj = result.getJSONObject(i);
				FlightsModel model = new FlightsModel();
				model.setFligthNum(resultObj.getString("FlightNumber"));
				model.setFromCity(resultObj.getString("FromCity"));
				model.setToCity(resultObj.getString("ToCity"));
				model.setRecordLocator(resultObj.getString("RecordLocator"));
				model.setBookStatus(resultObj.getString("BookingStatus"));
				
				items.add(model);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return new ArrayList<FlightsModel>();
		}
		
		return items;
	}
	
	public static List<StationModel> parseDestination(String response){
		List<StationModel> stationList = new ArrayList<StationModel>();
		
		try {
			
			JSONArray jArr = getData(response).getJSONArray("fullStationList");
			for (int i = 0; i<jArr.length();i++){
				JSONObject childObj = jArr.getJSONObject(i);
				StationModel item = new StationModel();
				item.setCountryName(childObj.getString("Country"));
				item.setCountryCode(childObj.getString("CurrencyCode"));
				item.setStationName(childObj.getString("StationName"));
				item.setKey(childObj.getString("Station"));
				
				List<String> destiList = new ArrayList<String>();
				JSONArray arrArr = childObj.getJSONArray("arrivalStationList");
				for (int x = 0 ; x < arrArr.length(); x++){
					destiList.add(arrArr.getString(x));
				}
				item.setDestinationKey(destiList);
				stationList.add(item);
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return stationList;
	}
	
	public static SearchInfoModel parseFlight(String response
								, String dDate
								, String reDate
								, SearchInfoModel searchItem){
		FlightSearchModel departModel = searchItem.getFlightByDate(dDate);

		FlightSearchModel returnModel = new FlightSearchModel();
		if (reDate != null && reDate.length()>0){
			returnModel = searchItem.getFlightByDate(reDate);
		}
		List<FlightDetailsModel> flightList = new ArrayList<FlightDetailsModel>();

		try{
			JSONObject jResponse = new JSONObject(response);
			JSONObject data = jResponse.getJSONObject("data");

			searchItem.setUserCurrency(data.getString("userCurrencyCode"));
			searchItem.setDisplayCurrency(data.getString("displayCurrencyCode"));
			String exchangeRate = data.getString("exchangeRate");
			searchItem.setExchangeRate(Float.valueOf(exchangeRate));
			searchItem.setDepartureCurrency(data.getString("departureCurrencyCode"));
			searchItem.setFareDiscount(data.getInt("FareDiscountAvailable"));
			
			JSONArray journeyArr = data.getJSONArray("JourneyDisplay");
			for (int jA = 0; jA < journeyArr.length(); jA++){
				JSONArray cjArr = journeyArr.getJSONArray(jA);
				List<String> journeyList = new ArrayList<String>();
				for (int cjA = 0; cjA < cjArr.length(); cjA++){
					journeyList.add(cjArr.getString(cjA));
				}
				
				if (jA == 0){
					searchItem.setDepartList(journeyList);
				}else{
					searchItem.setReturnList(journeyList);
				}
			}
			
			
			JSONArray lowFArr = data.getJSONArray("LowestFareArr");
			LogHelper.debug("Lowest Fare " + lowFArr.toString());
			for (int li = 0; li < lowFArr.length(); li ++){
				JSONObject lowObj = lowFArr.getJSONObject(li);
				Iterator<String> iter = lowObj.keys();
				while (iter.hasNext()) {
				    String key = iter.next();
				    try {
				    	
				    	String lowFare = "0.00";
				    	if (lowObj.has(key)){
				    		lowFare = lowObj.getString(key);
				    	}
				    	if (lowFare == null || lowFare.length()<=0 || lowFare.equalsIgnoreCase("null")){
				    		LogHelper.debug("Detect Null");
				    		lowFare = "0.00";
				    	}
				    	if (li == 0){
				    		searchItem.getFlightByDate(key).setLowestFareDepart(lowFare);
				    	}else{
				    		searchItem.getFlightByDate(key).setLowestFareReturn(lowFare);
				    	}
				    	
				    } catch (JSONException e) {
				        // Something went wrong!
				    	e.printStackTrace();
				    }
				}
			}
			

			JSONArray flightData = data.getJSONArray("FlightSearch");
			
			for (int i = 0; i < flightData.length(); i ++){
				JSONArray childArr = flightData.getJSONArray(i);
				flightList = new ArrayList<FlightDetailsModel>();
				for (int x = 0; x < childArr.length(); x++){
					JSONObject cObj = childArr.getJSONObject(x);
					FlightDetailsModel item = new FlightDetailsModel();
					item.setFlightStatus((cObj.getInt("FlightStatus")==1));
					
					if (item.isFlightStatus()){
						item.setCityIsland(cObj.getBoolean("CityIslandTransfer"));
						if (item.isCityIsland()){
							item.setTransitInfo(cObj.getString("TransitInfoContent"));
						}
						
						item.setJourneyKey(cObj.getString("JourneySellKey"));
						item.setDiscount(cObj.getDouble("JourneyDiscount"));
						item.setLowestFare(cObj.getDouble("LowestJourneyFare"));
						
						JSONArray segmentArr = cObj.getJSONArray("Segment");
						List<SegmentModel> segmentList = new ArrayList<SegmentModel>();
						for (int sc = 0; sc < segmentArr.length(); sc++){
							JSONObject segChild = segmentArr.getJSONObject(sc);
							SegmentModel seg = new SegmentModel();
							seg.setCarrierCode(segChild.getString("CarrierCode"));
							seg.setAirLineCompany(segChild.getString("AirlineCompany"));
							if (segChild.has("InftAvailability")){
								seg.setInftSeat(segChild.getInt("InftAvailability"));
							}
							seg.setDateSTA(segChild.getString("STA"));
							seg.setDataSTD(segChild.getString("STD"));
							seg.setDepartStation(segChild.getString("DepartureStation"));
							seg.setArriveStation(segChild.getString("ArrivalStation"));
							seg.setFlgithNum(segChild.getString("FlightNumber"));
							seg.setEstimateTime(segChild.getString("EstimatedTime"));
							seg.setInternational(segChild.getBoolean("International"));
							seg.setSegmentType(segChild.getInt("SegmentType"));
							segmentList.add(seg);
						}
	
						JSONObject productObj = cObj.getJSONObject("FareProductClassList");
						JSONObject fareTypeObj = cObj.getJSONObject("TotalJourneyAmount");
						FareTypeModel fareTypeModel = new FareTypeModel();
						
						fareTypeModel = parseFareType(fareTypeObj, productObj, "lowfare");
						if (fareTypeModel != null){
							LogHelper.debug("Add Low Fare");
							fareTypeModel.setSegList(segmentList);
							fareTypeModel.setFareType(0);
							item.addLowFareList(fareTypeModel);
						}
						
						fareTypeModel = parseFareType(fareTypeObj, productObj, "premiumflex");
						if (fareTypeModel != null){
							LogHelper.debug("Add Premium Fare");
							fareTypeModel.setSegList(segmentList);
							fareTypeModel.setFareType(1);
							item.addPremiumList(fareTypeModel);
						}
						
						fareTypeModel = parseFareType(fareTypeObj, productObj, "businessclass");
						if (fareTypeModel != null){
							LogHelper.debug("Add Business Fare");
							fareTypeModel.setSegList(segmentList);
							fareTypeModel.setFareType(2);
							item.addBusinessList(fareTypeModel);
						}
	
					}
					flightList.add(item);
				}

				if (i == 0){
					departModel.setFlightList(flightList, i);
					searchItem.updateModel(departModel);
				}else{
					returnModel.setFlightList(flightList, i);
					searchItem.updateModel(returnModel);
				}
			}
			
			departModel.hasDepart = true;
			returnModel.hasReturn = true;
			
			
			
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		
		return searchItem;
		
	}
	
	public static FareTypeModel parseFareType(JSONObject fareObj, JSONObject productObj, String key){
		FareTypeModel item = new FareTypeModel();
		try{
			JSONObject childObj = fareObj.getJSONObject("FareType");
			JSONObject child = childObj.getJSONObject(productObj.getString(key));
			item.setMinAvailableSeats(child.getInt("MinAvailableSeats"));
			item.setId(child.getString("FareUniqueID"));
			FareModel model = new FareModel();
			model = parseFareModel(child, ConstantHolder.FARE_ADT);
			if (model !=null){
				item.addFareModel( ConstantHolder.FARE_ADT, model);
			}
			model = parseFareModel(child, ConstantHolder.FARE_CHD);
			if (model !=null){
				item.addFareModel(ConstantHolder.FARE_CHD, model);
			}
			model = parseFareModel(child, ConstantHolder.FARE_INF);
			if (model !=null){
				item.addFareModel(ConstantHolder.FARE_INF, model);
			}
			
			
		}catch (Exception e){
			LogHelper.debug("parseFareType Error  " + key);
			return null;
		}
		
		return item;
	}
	
	public static FareModel parseFareModel(JSONObject jObj, String key){
		FareModel item = new FareModel();
		try{
			if (jObj.has(key)){
				JSONObject child = jObj.getJSONObject(key);
				item.setOriginalFare(child.getDouble("OriginalFare"));
				item.setFinalFare(child.getDouble("FinalFare"));
				item.setCurrencyCode(child.getString("CurrencyCode"));
			}else{
				item = null;
			}
		}catch(Exception e){
			LogHelper.debug("parseFareModel Error  " + key);
			return null;
		}
		
		return item;
	}
	
	public static List<FareTaxesModel> paraseFareTax(JSONObject jObj, List<FareTaxesModel> list){
		List<FareTaxesModel> mTaxes = new ArrayList<FareTaxesModel>();
		try{
			for (int i = 0 ; i < list.size(); i ++){
				if (jObj.has(list.get(i).getKey())){
					FareTaxesModel tax = new FareTaxesModel(list.get(i).getKey(),
												list.get(i).getTitle());
					tax.setAmount(jObj.getDouble(tax.getKey()));
					LogHelper.debug(jObj.getDouble(tax.getKey()) + " Get Taxes Amount " + tax.getAmount());
					mTaxes.add(tax);
				}
			}
		}catch(Exception e){
			return null;
		}
		
		return mTaxes;
	}
	
	public static List<FareTaxesModel> parseTaxes(JSONObject jData){
		List<FareTaxesModel> taxList = new ArrayList<FareTaxesModel>();
		try{

			JSONArray jArr = jData.getJSONArray("fareTypes");
			for (int i = 0 ; i < jArr.length(); i++){
				JSONObject cObj = jArr.getJSONObject(i);
				FareTaxesModel tax = new FareTaxesModel();
				tax.setKey(cObj.getString("fare_type_key"));
				tax.setTitle(cObj.getString("fare_type_title"));
				taxList.add(tax);
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return taxList;
		
	}
	
	public static List<CountryModel> parseExchangeRare(JSONObject jData){
		List<CountryModel> cList = new ArrayList<CountryModel>();
		try{
			JSONArray jArr = jData.getJSONArray("fullCurrencyList");
			
			for(int i = 0; i < jArr.length(); i++){
				JSONObject cObj = jArr.getJSONObject(i);
				CountryModel cModel = new CountryModel();
				cModel.setCode(cObj.getString("QuotedCurrency"));
				JSONArray cArr = cObj.getJSONArray("CollectedCurrencyList");
				for (int x = 0 ; x < cArr.length(); x++){
					JSONObject exObj = cArr.getJSONObject(x);
					String value = exObj.getString("CollectedCurrencyRate");
					cModel.addCurrency(exObj.getString("CollectedCurrency"),
								Float.valueOf(value));
				}
				
				cList.add(cModel);
				
				
			}
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return cList;
		
	}
	
	public static SSRMainModel parseSSRMainModel(JSONObject data){
		SSRMainModel mModel = new SSRMainModel();
		
		try{
			if (data.getString("InsurancePlan").equalsIgnoreCase("false")){
				mModel.setHasInsurance(false);
			}else{
				mModel.setHasInsurance(true);
				JSONObject jInsura = data.getJSONObject("InsurancePlan");
				mModel.setPlanCode(jInsura.getString("PlanCode"));
				mModel.setFeeCode(jInsura.getString("SSRFeeCode"));

				List<MemberInfoModel> passagers = new ArrayList<MemberInfoModel>();
				
				JSONArray planArr = jInsura.getJSONArray("PassengerPlan");
				
				for (int i = 0; i < planArr.length(); i++){
					JSONObject jChild = planArr.getJSONObject(i);
					MemberInfoModel mPerson = new MemberInfoModel();
					mPerson.setfName(jChild.getString("FirstName"));
					mPerson.setlName(jChild.getString("LastName"));
					mPerson.setDob(jChild.getString("DOB"));
					mPerson.setPassangerNum(jChild.getInt("IdentityNo"));
					mPerson.setQualified(jChild.getBoolean("IsPassengerQualified"));
					mPerson.setCountryCode(jChild.getString("CurrencyCode"));
					String premiumAmout = jChild.getString("PassengerPremiumAmount");
					if (premiumAmout!=null && premiumAmout.length()>0){
						mPerson.setPremiumAmount(Float.valueOf(premiumAmout));
					}else{
						mPerson.setPremiumAmount(0.00f);
					}
					passagers.add(mPerson);
				}
				
				mModel.setPassangers(passagers);
			}
			
			JSONArray jSSR = data.getJSONArray("OverallSsrList");
			List<SSRContainer> ssrContainers = new ArrayList<SSRContainer>();
			
			for (int i = 0; i <jSSR.length(); i++){
				ssrContainers = new ArrayList<SSRContainer>();
				JSONArray childArr = jSSR.getJSONArray(i);
//				LogHelper.debug("Child Arr " + childArr.toString());
				for (int childInd = 0; childInd < childArr.length(); childInd++){
					SSRContainer container = new SSRContainer();
					JSONObject jChild = childArr.getJSONObject(childInd);
					container.setCarrierCode(jChild.getString("CarrierCode"));
					container.setFlightNum(jChild.getString("FlightNumber"));
					container.setOpSuffix(jChild.getString("OpSuffix"));
					container.setDepartdate(jChild.getString("DepartureDate"));
					container.setDepartCode(jChild.getString("DepartureStation"));
					container.setArrivalCode(jChild.getString("ArrivalStation"));
					container.setDefaultBaggage(jChild.getString("DefaultBaggageSelect"));
					container.setFreeBaggage(jChild.getBoolean("FreeBaggageAvailable"));
					container.setMeal(jChild.getBoolean("ComplimentaryMeal"));
					JSONObject ssrObj = jChild.getJSONObject("SsrList");
					
					List<SSRItemModel> wheelList = new ArrayList<SSRItemModel>();
					JSONArray jWheel = ssrObj.getJSONArray("WheelChair");
					for (int wi = 0 ; wi < jWheel.length(); wi ++){
						JSONObject wheelObj = jWheel.getJSONObject(wi);
						SSRItemModel mWheel = new SSRItemModel();
						mWheel.setAvailable(wheelObj.getString("Available"));
						mWheel.setFeeCode(wheelObj.getString("FeeCode"));
						mWheel.setSsrNumber(wheelObj.getString("SSRNumber"));
						mWheel.setDescripion(wheelObj.getString("Note"));
						mWheel.setAmountTotal(wheelObj.getDouble("AmountTotal"));
						if (wheelObj.has("FeeNumber")){
							mWheel.setFeeNumber(wheelObj.getString("FeeNumber"));
						}else{
							mWheel.setFeeNumber("0");
						}
						mWheel.setSsrCode(wheelObj.getString("SSRCode"));

						mWheel.setCategory("WheelChair");
						
						List<ChargeModel> charges = new ArrayList<ChargeModel>();
						JSONArray chargeArr = wheelObj.getJSONArray("Charge");
						for (int chargeI = 0 ; chargeI < chargeArr.length(); chargeI++){
							JSONObject chargeObj = chargeArr.getJSONObject(chargeI);
							ChargeModel charge = new ChargeModel();
							charge.setChargeCode(chargeObj.getString("ChargeCode"));
							charge.setChargeType(chargeObj.getString("ChargeType"));
							charge.setCurrencyCode(chargeObj.getString("CurrencyCode"));
							charge.setAmount(chargeObj.getDouble("Amount"));
							charge.setDetails(chargeObj.getString("ChargeDetail"));
							charges.add(charge);
						}
						mWheel.setCharges(charges);
                        
						wheelList.add(mWheel);
					}
					container.setWheelList(wheelList);
					
					List<SSRItemModel> baggageList = new ArrayList<SSRItemModel>();
					SSRItemModel zeroBaggage = new SSRItemModel();
					zeroBaggage.setAmountTotal(0.00);
					zeroBaggage.setDescripion("0kg");
					zeroBaggage.setSsrCode("0000");
					List<ChargeModel> zeroChargeModels = new ArrayList<ChargeModel>();
					zeroBaggage.setCharges(zeroChargeModels);
					baggageList.add(zeroBaggage);
					
					JSONArray jBaggage = ssrObj.getJSONArray("Baggage");
					LogHelper.debug("Baggage " + jBaggage.toString());
					for (int bi = 0; bi < jBaggage.length(); bi++){
						JSONObject baggageObj = jBaggage.getJSONObject(bi);
						SSRItemModel mBaggage = new SSRItemModel();
						mBaggage.setAvailable(baggageObj.getString("Available"));
						mBaggage.setFeeCode(baggageObj.getString("FeeCode"));
						mBaggage.setSsrNumber(baggageObj.getString("SSRNumber"));
						mBaggage.setDescripion(baggageObj.getString("Note"));
						mBaggage.setAmountTotal(baggageObj.getDouble("AmountTotal"));
						mBaggage.setSsrCode(baggageObj.getString("SSRCode"));
						if (baggageObj.has("FeeNumber")){
							mBaggage.setFeeNumber(baggageObj.getString("FeeNumber"));
						}else{
							mBaggage.setFeeNumber("0");
						}
						mBaggage.setBaggageOrder(baggageObj.getInt("BaggageOrderNumber"));
						mBaggage.setCategory("Baggage");
						
						List<ChargeModel> charges = new ArrayList<ChargeModel>();
						JSONArray chargeArr = baggageObj.getJSONArray("Charge");
						for (int chargeI = 0 ; chargeI < chargeArr.length(); chargeI++){
							JSONObject chargeObj = chargeArr.getJSONObject(chargeI);
							ChargeModel charge = new ChargeModel();
							charge.setChargeCode(chargeObj.getString("ChargeCode"));
							charge.setChargeType(chargeObj.getString("ChargeType"));
							charge.setCurrencyCode(chargeObj.getString("CurrencyCode"));
							charge.setAmount(chargeObj.getDouble("Amount"));
							charge.setDetails(chargeObj.getString("ChargeDetail"));
							charges.add(charge);
						}
						mBaggage.setCharges(charges);
                        
						baggageList.add(mBaggage);
					}
					
					container.setBaggageList(baggageList);
					
					List<SSRItemModel> mealList = new ArrayList<SSRItemModel>();
					if (ssrObj.has("Meal")){
						JSONArray jMeal = ssrObj.getJSONArray("Meal");
						LogHelper.debug("Meal json " + jMeal);
						for (int mi = 0; mi < jMeal.length(); mi++){
							JSONObject mealObj = jMeal.getJSONObject(mi);
							SSRItemModel mMeal = new SSRItemModel();
							mMeal.setAvailable(mealObj.getString("Available"));
							mMeal.setFeeCode(mealObj.getString("FeeCode"));
							mMeal.setSsrNumber(mealObj.getString("SSRNumber"));
							mMeal.setDescripion(mealObj.getString("Note"));
							mMeal.setAmountTotal(mealObj.getDouble("AmountTotal"));
							mMeal.setSsrCode(mealObj.getString("SSRCode"));
							mMeal.setImage(mealObj.getString("ImagePath"));
							if (mealObj.has("FeeNumber")){
								mMeal.setFeeNumber(mealObj.getString("FeeNumber"));
							}else{
								mMeal.setFeeNumber("0");
							}
							mMeal.setCategory("Meal");
							
							List<ChargeModel> charges = new ArrayList<ChargeModel>();
							JSONArray chargeArr = mealObj.getJSONArray("Charge");
							for (int chargeI = 0 ; chargeI < chargeArr.length(); chargeI++){
								JSONObject chargeObj = chargeArr.getJSONObject(chargeI);
								ChargeModel charge = new ChargeModel();
								charge.setChargeCode(chargeObj.getString("ChargeCode"));
								charge.setChargeType(chargeObj.getString("ChargeType"));
								charge.setCurrencyCode(chargeObj.getString("CurrencyCode"));
								charge.setAmount(chargeObj.getDouble("Amount"));
								charge.setDetails(chargeObj.getString("ChargeDetail"));
								charges.add(charge);
							}
							mMeal.setCharges(charges);
							
							if (i == 0){
								mMeal.setDepart(true);
							}
							
							mealList.add(mMeal);
						}
					}
					container.setMealList(mealList);

					if (ssrObj.has("MealBundle")){
						JSONArray jMealBundle = ssrObj.getJSONArray("MealBundle");
						LogHelper.debug("Meal Bundle Info " + jMealBundle.toString());
						List<MealBundleModel> mBundleList = new ArrayList<MealBundleModel>();
						for (int mbIndex = 0; mbIndex < jMealBundle.length(); mbIndex ++){
							JSONObject bundleObj = jMealBundle.getJSONObject(mbIndex);
							MealBundleModel mealBundle = new MealBundleModel();
							mealBundle.setImage(bundleObj.getString("ImagePath"));
							mealBundle.setAvailable(bundleObj.getString("Available"));
							mealBundle.setFeeCode(bundleObj.getString("FeeCode"));
							mealBundle.setSsrNumber(bundleObj.getString("SSRNumber"));
							mealBundle.setDescripion(bundleObj.getString("Note"));
							mealBundle.setAmountTotal(bundleObj.getDouble("AmountTotal"));
							mealBundle.setSsrCode(bundleObj.getString("SSRCode"));
							if (bundleObj.has("FeeNumber")){
								mealBundle.setFeeNumber(bundleObj.getString("FeeNumber"));
							}else{
								mealBundle.setFeeNumber("0");
							}

							JSONArray chargeArr = bundleObj.getJSONArray("Charge");
							List<ChargeModel> bundlerCharges = new ArrayList<ChargeModel>();
							for (int chargeIndex = 0; chargeIndex < chargeArr.length(); chargeIndex++){
								JSONObject chargeObj = chargeArr.getJSONObject(chargeIndex);
								ChargeModel charge = new ChargeModel();
								charge.setChargeType(chargeObj.getString("ChargeType"));
								charge.setChargeCode(chargeObj.getString("ChargeCode"));
								charge.setCurrencyCode(chargeObj.getString("CurrencyCode"));
								charge.setAmount(chargeObj.getDouble("Amount"));
								charge.setDetails(chargeObj.getString("ChargeDetail"));
								bundlerCharges.add(charge);
							}
							mealBundle.setCharges(bundlerCharges);
							mBundleList.add(mealBundle);
						}
						container.setMealBundle(mBundleList);

					}
					
					if (i == 0){
						container.setDepart(true);
					}else{
						container.setDepart(false);
					}
					
					ssrContainers.add(container);
				}
				if (i == 0){
					mModel.setDepartContainer(ssrContainers);
				}else{
					mModel.setReturnContainer(ssrContainers);
				}
			}
			
			LogHelper.debug("SSR Lory Container " + ssrContainers.size());
			mModel.setSsrContainers(ssrContainers);
			
//			JSONObject jCateg = data.getJSONObject("SsrCategoryList");

//			LogHelper.debug("JSsr " + jCateg.toString());
//			for (int i = 0; i <jCateg.length(); i++){
//				
//			}
		}catch (Exception e){
			e.printStackTrace();
		}
		
		return mModel;
	}

	public static List<PaymentOptionModel> parsePaymentOption(JSONObject data){
		List<PaymentOptionModel> optionsList = new ArrayList<PaymentOptionModel>();
		try{

			JSONArray payArr = data.getJSONArray("paymentMethod");

			for (int i = 0; i < payArr.length(); i++){
				JSONObject childObj = payArr.getJSONObject(i);
				PaymentOptionModel option = new PaymentOptionModel();
				option.setPayId(childObj.getInt("paymentID"));
				option.setChannel(childObj.getString("channel"));
				JSONArray methodArr = childObj.getJSONArray("items");
				List<PayMethodModel> methodList = new ArrayList<PayMethodModel>();

				for (int iMethod = 0; iMethod < methodArr.length(); iMethod++){
					JSONObject methodObj = methodArr.getJSONObject(iMethod);
					PayMethodModel methodItem = new PayMethodModel();
					if (methodObj.has("bankID")){
						methodItem.setBankId(methodObj.getString("bankID"));
					}

					if(methodObj.has("cardBrand")) {
						methodItem.setBrand(methodObj.getString("cardBrand"));
					}

					methodItem.setBrandName(methodObj.getString("name"));
					methodItem.setBrandLogo(methodObj.getString("logo"));
					methodItem.setProcessFee(methodObj.getDouble("processingFee"));
					methodItem.setBrandCurrency(methodObj.getString("basedCurrency"));
					methodList.add(methodItem);
				}
				option.setPayMethods(methodList);

				optionsList.add(option);
			}

		}catch(Exception e){
			e.printStackTrace();
		}

		LogHelper.debug("Option size " + optionsList.size());

		return optionsList;
	}
	
	
}
