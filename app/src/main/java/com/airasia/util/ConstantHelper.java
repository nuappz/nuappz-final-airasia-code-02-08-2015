package com.airasia.util;

import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

public class ConstantHelper {

	public static String dateForDayMonthYear(Date date)
	{
		if(date==null)
			return " ";
		
		SimpleDateFormat dateformate = new SimpleDateFormat("dd MMM yyyy");

        String currentDateStr = dateformate.format(date);
        
        return currentDateStr;
	}
	
	public static boolean isAlphaNumeric(String s){
	    String pattern= "^[a-zA-Z0-9]*$";
	    Pattern p = Pattern.compile("(?=.*[0-9])" // There is a digit 
	    		+ "(?=.*[a-z])"
	    		+ "(?=.*[A-Z])"// there is a letter 
	    		+ "[a-zA-Z0-9]*");
//	        if(s.matches(pattern)){
	    if (p.matcher(s).matches()){
	    	    LogHelper.debug("Is alphanumeric");
	            return true;
        }
        return false;   
	}
	
	public static String sha256(String base) {
        try{
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(base.getBytes());
            byte[] hash = digest.digest();
//            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

//            for (int i = 0; i < hash.length; i++) {
//                String hex = Integer.toHexString(0xff & hash[i]);
//                if(hex.length() == 1) hexString.append('0');
//                hexString.append(hex);
//            }
            
            for (int i = 0; i < hash.length; i++) {
            	hexString.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
           }

	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}
	
	
	//ddMMyyyy
	public static String getDateFromddMMyyyy(String dob){
		String dateStr = "";
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		try {
			date = format.parse(dob);
			format = new SimpleDateFormat("dd MMM yyyy");
			dateStr = format.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dateStr = "";
		}
		return dateStr;
	}
	
	//from yyyy-MM-dd'T'hh:mm:ss to hhmm
	public static String gethhmmFromDateString(String inString){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
		Date date;
		try {
			date = format.parse(inString);
			format = new SimpleDateFormat("kkmm");
			String result =  format.format(date);
			return result;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "0000";
		}
	}

	
	public static String changeDateFormat(String input ,SimpleDateFormat formatFrom, SimpleDateFormat formatTo){
		Date date;
		String result="";
		if (input==null || input.length()<=0){
			return "";
		}
		try {
			date = formatFrom.parse(input);
			result = formatTo.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		return result;
	}
	
	public static String changeDateFormat(String input ,String _formatFrom, String _formatTo){
		SimpleDateFormat formatFrom = new SimpleDateFormat(_formatFrom);
		SimpleDateFormat formatTo = new SimpleDateFormat(_formatTo);
		Date date;
		String result="";
		if (input==null || input.length()<=0){
			return "";
		}
		try {
			date = formatFrom.parse(input);
			result = formatTo.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		return result;
	}
	
	public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
	
	public static boolean sameday(Date d1, Date d2){
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(d1);
		cal2.setTime(d2);
		boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
		                  cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
		return sameDay;
	}
	
	public static String calculateEstimateTime(String dateStart, String dateStop, String pattern){
		// Custom date format
		SimpleDateFormat format = new SimpleDateFormat(pattern);  
		String result="";
		Date d1 = null;
		Date d2 = null;
		try {
		    d1 = format.parse(dateStart);
		    d2 = format.parse(dateStop);
		} catch (ParseException e) {
		    e.printStackTrace();
		}    

		// Get msec from each, and subtract.
		long diff = d2.getTime() - d1.getTime();
//		long diffSeconds = diff / 1000;         
		long diffMinutes = diff / (60 * 1000);         
		long diffHours = diff / (60 * 60 * 1000);
		diffMinutes = diffMinutes - (diffHours * 60);
		
		result = diffHours +"H"+diffMinutes+"M";
		
		return result;
	}
	
	public static int calculateTimeRangeHour(String returnDate, String departDate, String pattern){
		// Custom date format
		SimpleDateFormat format = new SimpleDateFormat(pattern);  
		int result=0;
		Date d1 = null;
		Date d2 = null;
		try {
		    d1 = format.parse(departDate);
		    d2 = format.parse(returnDate);
		} catch (ParseException e) {
		    e.printStackTrace();
		}    

		// Get msec from each, and subtract.
		long diff = d2.getTime() - d1.getTime();
		long diffHours = diff / (60 * 60 * 1000);
		LogHelper.debug("Different Hour " + diffHours);
		result = (int) diffHours;
		
		return result;
	}
	
	public static int getYearDiff(Date date1,Date date2){ 
	      SimpleDateFormat simpleDateformat=new SimpleDateFormat("yyyy");
	      Integer.parseInt(simpleDateformat.format(date1));

	      return Integer.parseInt(simpleDateformat.format(date2))- Integer.parseInt(simpleDateformat.format(date1));

    }
	
}
