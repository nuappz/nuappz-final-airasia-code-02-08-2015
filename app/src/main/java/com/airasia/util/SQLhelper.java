package com.airasia.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import com.airasia.holder.SQLHolder;
import com.airasia.model.CountryModel;
import com.airasia.model.FareClassUpgrade;
import com.airasia.model.FareTaxesModel;
import com.airasia.model.SSRModel;
import com.airasia.model.StationModel;
@SuppressLint("SdCardPath")
public class SQLhelper extends SQLiteOpenHelper {

	public final static String DATABASE_NAME = "com_airasia_db_v1.sqlite";
	private final static int DATABASE_VERSION = 5;
	public final static int DB_NOT_EXIST = -2;
	private static String DB_PATH = "";
	SQLiteDatabase database;
	Context mContext;

//	public final static String CREATE_CUSTOMER_TABLE = "";
//			"CREATE TABLE " + CUSTOMER_TABLE_NAME_TEST + 
//			" ( " + CUSTOMER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ DATE +" TEXT, " + UPDATE_DATE + " LONG, "+CUSTOMER_TITLE +" TEXT, " + CUSTOMER_NAME+ " TEXT, " 
//			+ CUSTOMER_CONTACT + " TEXT, "+ CUSTOMER_EMAIL+" TEXT, "+ CUSTOMER_MODEL+" TEXT, " +CUSTOMER_SERIES +" TEXT, "
//			+ CUSTOMER_ADDRESS_1 + " TEXT, " + CUSTOMER_ADDRESS_2 +" TEXT, " + CUSTOMER_OWNCAR + " TEXT, "
//			+ CUSTOMER_OWNMODEL +" TEXT, " + CUSTOMER_OWNMAKE +" TEXT, " + CUSTOMER_PLAN +" TEXT, "+ CUSTOMER_IMAGE + " TEXT ); ";
//	
	
	public SQLhelper(Context context){
		super(context, DATABASE_NAME, null , DATABASE_VERSION);
		DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
		this.mContext = context;
		try {
			if(android.os.Build.VERSION.SDK_INT >= 17){
			       DB_PATH = context.getApplicationInfo().dataDir + "/databases/";         
			}
			createDataBase();
		}catch (IOException e){
			LogHelper.debug("Fail to Create database!");
		}
	}
	
	public SQLhelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		database = db;
		db.execSQL(SQLHolder.CREATE_COUNTRY_TABLE);	
		db.execSQL(SQLHolder.CREATE_STAION_TABLE);		
		db.execSQL(SQLHolder.CREATE_STAION_DEPART_TABLE);
		db.execSQL(SQLHolder.CREATE_SSR_TABLE);
		db.execSQL(SQLHolder.CREATE_FAREC_TABLE);
		db.execSQL(SQLHolder.CREATE_FARET_TABLE);
		db.execSQL(SQLHolder.CREATE_CURRENCY_TABLE);
	}
	
	public void createDataBase() throws IOException {
		boolean mDBExist = checkDataBase();
		if (!mDBExist){
			LogHelper.debug("New Database Create");
			this.getWritableDatabase();
			this.close();
			try{
//				copyDataBase();
				File file = new File(DB_PATH+DATABASE_NAME);
				if (!file.exists()){
					file.mkdirs();
					file.createNewFile();
				}				
				LogHelper.debug("Success Create new");
			} catch (IOException mIOException){
				throw new Error("Error Copy DB " + mIOException.getMessage());
			}
		}else {
			LogHelper.debug("Database Exist");
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.COUNTRY_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.SSR_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.STATION_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.FAREC_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.DEPART_STATION_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.FARET_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + SQLHolder.CURRENCY_TABLE);
		onCreate(db);
	}
	
	public boolean checkDataBase(){
		File dbFile = new File(DB_PATH + DATABASE_NAME);
		return dbFile.exists();
	}
	
	public void copyDataBase() throws IOException{
//		InputStream mInput = mContext.getAssets().open(DATABASE_NAME);
//		String outFileName = DB_PATH + DATABASE_NAME;
//		File file = new File(DB_PATH);
//		if (!file.exists()){
//			file.mkdirs();
//			file.createNewFile();
//		}
//		Logger.debug(outFileName);
//		OutputStream mOutput = new FileOutputStream(outFileName);
//		byte[] mBuffer = new byte[1024];
//		int mLength;
//		while((mLength = mInput.read(mBuffer))>0){
//			mOutput.write(mBuffer, 0 , mLength);
//		}
//		mOutput.flush();
//		mOutput.close();
//		mInput.close();
	}
	
	public Cursor select(String tableName){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor =  db.query(tableName, null, null, null, null, null,
				null);//UPDATE_DATE + " ASC");
		db.close();
		return cursor;
	}
	
	public boolean insertCountry(CountryModel country){
	String tableName = getCountryTableName();
		try{
			SQLiteDatabase db = this.getReadableDatabase();
			if (!CheckIsCountryExist(country)){
				db.insert(tableName, null, getContentValueCountry(country, false));
//				LogHelper.debug("country inserted success");
			}else{
//				LogHelper.debug("country exist");
				updateCountry(country, false);
			}
				db.close();
		}catch (Exception e){
			
		}
	
		return true;	
	}
	
	public boolean insertStation(StationModel station){
		String tableName = getStationTableName(0);
			try{
				SQLiteDatabase db = this.getReadableDatabase();
				if (!CheckIsStationExist(station, 0)){
//					LogHelper.debug("station inserted success");
					db.insert(tableName, null, getContentValueStation(station, false));
				}else{
//					LogHelper.debug("station exist");
					updateStation(station, false, 0);
				}
					db.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		
			return true;	
		}
	
	public boolean insertDepartStation(List<StationModel> stationList){
		String tableName = getStationTableName(1);
		if (stationList == null || stationList.size()<=0){
			return true;
		}
			try{
				SQLiteDatabase db = this.getReadableDatabase();
				
				for (int i=0; i<stationList.size();i++){
					StationModel station = stationList.get(i);
					if (!CheckIsStationExist(station, 1)){
						db.insert(tableName, null, getContentValueStation(station, false));
//						LogHelper.debug("station inserted success");
					}else{
//						LogHelper.debug("country exist");
						updateStation(station, false, 1);
					}
				}
				db.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		
			return true;	
		}
	
	public boolean insertSSRList(List<SSRModel> ssrList){
		String tableName = getSSRTableName();
		if (ssrList == null || ssrList.size()<=0){
			return true;
		}
			try{
				SQLiteDatabase db = this.getReadableDatabase();
				
				for (int i=0; i<ssrList.size();i++){
					SSRModel ssr = ssrList.get(i);
					LogHelper.debug("Insert SSR " + ssr.getSsrCode() + " " + ssr.getDescriptio());
					if (!CheckIsSSRExist(ssr)){
						db.insert(tableName, null, getContentValueSSR(ssr, false));
					}else{
//						LogHelper.debug("country exist");
						updateSSR(ssr, false, 1);
					}
				}
				db.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		
			return true;	
		}
	
	public boolean insertFareClassList(List<FareClassUpgrade> fcList){
		String tableName = getFareCTableName();
		if (fcList == null || fcList.size()<=0){
			return true;
		}
			try{
				SQLiteDatabase db = this.getReadableDatabase();
				for (int i=0; i<fcList.size();i++){
					FareClassUpgrade fare = fcList.get(i);
					if (!CheckIsFareExist(fare)){
						db.insert(tableName, null, getContentValueFareClass(fare, false));
					}else{
						updateFare(fare, false, 1);
					}
				}
				db.close();
			}catch (Exception e){
				e.printStackTrace();
			}
		
			return true;
		
	}
	
	public boolean insertFareTaxesList(List<FareTaxesModel> taxes){
		String tableName = getFareTTableName();
		if (taxes == null || taxes.size()<=0){
			return false;
		}
			try{
				SQLiteDatabase db = this.getReadableDatabase();
				for (int i=0; i<taxes.size();i++){
					FareTaxesModel fare = taxes.get(i);
					if (!CheckIsFareTaxExist(fare)){
						db.insert(tableName, null, getContentValueFareTaxes(fare, false));
					}else{
						updateFareTaxes(fare, false, 1);
					}
				}
				db.close();
			}catch (Exception e){
				e.printStackTrace();
				return false;
			}
		
		return true;
		
	}
	
	public boolean insertCurrencyList(List<CountryModel> currency){
		String tableName = getCurrencyTableName();
		if (currency == null || currency.size()<=0){
			return false;
		}
			try{
				SQLiteDatabase db = this.getReadableDatabase();
				for (int i=0; i<currency.size();i++){
					CountryModel cModel = currency.get(i);
					if (!CheckIsCurrencyExist(cModel)){
						db.insert(tableName, null, getContentValueCurrency(cModel, false));
					}else{
						updateCurrency(cModel, false);
					}
				}
				db.close();
			}catch (Exception e){
				e.printStackTrace();
				return false;
			}
		
		return true;
	}
	
	/*SQL Check*/
	
	public boolean CheckIsCountryExist(CountryModel country) {
	    String Query = "Select * from " + getCountryTableName() + " where " + SQLHolder.COUNTRY_CODE + " == \"" + country.getCode()+"\"";
//	    LogHelper.debug("Query = " + Query);
	    SQLiteDatabase db = this.getWritableDatabase();
	    Cursor cursor = db.rawQuery(Query, null);
//		Cursor cursor = db.query(getCountryTableName(), null, SQLHolder.COUNTRY_CODE
//				+ "=" + country.getCode(), null, null, null, null);
		boolean exist = (cursor.getCount() >0);
//	    try{
//		     cursor = db.rawQuery(Query, null);
//		        if(cursor ==null || cursor.getCount() <= 0){
//		    	if(!cursor.moveToFirst()){
//		            cursor.close();
//		            LogHelper.debug("No Country found for " + country.getCode());
//		            return false;
//		        }
//	    }catch (Exception e){
//	    	return false;
//	    }
	    cursor.close();
	    return exist;
	}
	
	public boolean CheckIsFareExist(FareClassUpgrade fare) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(getFareCTableName(), null, SQLHolder.FAREC_KEY
				+ "=\"" + fare.getKey()+"\"", null, null, null, null);
		boolean exist = (cursor.getCount() >0);
		return exist;
	}
	
	public boolean CheckIsFareTaxExist(FareTaxesModel taxes) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(getFareTTableName(), null, SQLHolder.FARET_KEY
				+ "=\"" + taxes.getKey()+"\"", null, null, null, null);
		boolean exist = (cursor.getCount() >0);
		return exist;
	}

	public boolean CheckIsCurrencyExist(CountryModel currency) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(getCurrencyTableName(), null, SQLHolder.CURRENCY_COUNTRY
				+ "=\"" + currency.getCode()+"\"", null, null, null, null);
		boolean exist = (cursor.getCount() >0);
		return exist;
	}

	public boolean CheckIsSSRExist(SSRModel ssr) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(getSSRTableName(), null, SQLHolder.SSR_CODE
				+ "=\"" + ssr.getSsrCode()+"\"", null, null, null, null);
		boolean exist = (cursor.getCount() >0);
		return exist;
	}
	
	public boolean CheckIsStationExist(StationModel station, int type) {
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.query(getStationTableName(type), null, SQLHolder.STATION_KEY
				+ "=\"" + station.getKey()+"\"", null, null, null, null);
		boolean exist = (cursor.getCount() >0);
		return exist;
	}
	
	private ContentValues getContentValueCountry(CountryModel country, boolean flag){
		ContentValues cv = new ContentValues();
		cv.put(SQLHolder.COUNTRY_CODE, country.getCode());
		cv.put(SQLHolder.COUNTRY_NAME, country.getName()); 
		cv.put(SQLHolder.COUNTRY_DIALING_CODE, country.getDialingCode());
		return cv;
	}
	
	private ContentValues getContentValueFareClass(FareClassUpgrade fare, boolean flag){
		ContentValues cv = new ContentValues();
		cv.put(SQLHolder.FAREC_KEY, fare.getKey());
		cv.put(SQLHolder.FAREC_TITLE, fare.getTitle()); 
		cv.put(SQLHolder.FAREC_UPGRADE, fare.getUpgrade_title());
		
		if (fare.getBenefits()!=null && fare.getBenefits().size()>0){
			String appendText = "/";
			List<String> listStr = fare.getBenefits();
			for (int i = 0; i < listStr.size(); i++){
				if (i!=(listStr.size()-1)){
					appendText += listStr.get(i) + "/";
				}else{
					appendText += listStr.get(i);
				}
			}
			LogHelper.debug("Insert Benefit " + appendText);
			cv.put(SQLHolder.FAREC_BENEFIT, appendText);
		}
		
		return cv;
	}
	
	private ContentValues getContentValueFareTaxes(FareTaxesModel fare, boolean flag){
		ContentValues cv = new ContentValues();
		cv.put(SQLHolder.FARET_KEY, fare.getKey());
		cv.put(SQLHolder.FARET_TITLE, fare.getTitle()); 
		
		return cv;
	}
	
	private ContentValues getContentValueCurrency(CountryModel currency, boolean flag){
		ContentValues cv = new ContentValues();
		cv.put(SQLHolder.CURRENCY_COUNTRY, currency.getCode());
		cv.put(SQLHolder.CURRENCY_PRICE_LIST, currency.getCurrencyBytes());

		
		return cv;
	}
	
	private ContentValues getContentValueStation(StationModel station, boolean flag){
		ContentValues cv = new ContentValues();
		cv.put(SQLHolder.STATION_KEY, station.getKey());
		cv.put(SQLHolder.STATION_NAME, station.getStationName()); 
		cv.put(SQLHolder.STATION_COUNTRY, station.getCountryName());
		cv.put(SQLHolder.STATION_AIRPORT, station.getAirportName());
		if (station.getDestinationKey()!=null && station.getDestinationKey().size()>0){
			String appendText = "/";
			List<String> listStr = station.getDestinationKey();
			for (int i = 0; i < listStr.size(); i++){
				if (i!=(listStr.size()-1)){
					appendText += listStr.get(i) + "/";
				}else{
					appendText += listStr.get(i);
				}
			}
			cv.put(SQLHolder.STATION_DESTINATION, appendText);
		}
		return cv;
	}
	
	private ContentValues getContentValueSSR(SSRModel ssr, boolean flag){
		ContentValues cv = new ContentValues();
		cv.put(SQLHolder.SSR_CODE, ssr.getSsrCode());
		cv.put(SQLHolder.SSR_DESCRIP, ssr.getDescriptio()); 
		return cv;
	}
	
	public List<CountryModel> getCountryModels(){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(getCountryTableName(), null, null, null, null, null, SQLHolder.COUNTRY_NAME + " ASC");
		List<CountryModel> mArray = new ArrayList<CountryModel>();
		if (cursor == null)
			return mArray;
		else if (cursor.getCount()<=0)
			return mArray;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			CountryModel cc = getCountry(cursor);
			mArray.add(cc);
		}
		cursor.close();
		return mArray;
	}	
	
	/*SQL Get*/
	
	public List<String> getCountryNameList(){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(getCountryTableName(), null, null, null, null, null, SQLHolder.COUNTRY_NAME + " ASC");
		List<String> mArray = new ArrayList<String>();
		if (cursor == null)
			return mArray;
		else if (cursor.getCount()<=0)
			return mArray;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			String cc = getCountryName(cursor);
			mArray.add(cc);
		}
		cursor.close();
		return mArray;
	}
	
	public List<FareTaxesModel> getFareTaxesList(){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(getFareTTableName(), null, null, null, null, null, SQLHolder.FARET_KEY + " ASC");
		List<FareTaxesModel> mArray = new ArrayList<FareTaxesModel>();
		if (cursor == null)
			return mArray;
		else if (cursor.getCount()<=0)
			return mArray;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			FareTaxesModel fare = getFareTaxModel(cursor);
			mArray.add(fare);
		}
		cursor.close();
		return mArray;
	}
	
	public StationModel getStationBaseKey(String code, int type){
		SQLiteDatabase db = this.getReadableDatabase();
		String where = SQLHolder.STATION_KEY + "=?";
		String[] column = getStationColumn(type);
		Cursor cursor = db.query(getStationTableName(type), column,
				where , new String[]{code}, 
				null, null, null);
		StationModel model = new StationModel();
		if (cursor == null){
			return model;
		}else{
			cursor.moveToFirst();
		}
		
		model = getStation(cursor);
//		LogHelper.debug("Get Station name = " + model.getStationName() + " country " + model.getCountryName() + " " + model.getCountryCode() + " " + model.getKey());
		cursor.close();
		return model;
	}
	
	public SSRModel getSSRBaseKey(String key){
		SQLiteDatabase db = this.getReadableDatabase();
		String where = SQLHolder.SSR_CODE + "=?";
		Cursor cursor = db.query(getSSRTableName(), new String[]{SQLHolder.SSR_CODE, SQLHolder.SSR_DESCRIP},
				where , new String[]{key}, 
				null, null, null);
		SSRModel model = new SSRModel();
		if (cursor == null){
			return model;
		}else{
			cursor.moveToFirst();
		}
		
		model = getSSR(cursor);
		LogHelper.debug("Get ssr name = " + model.getSsrCode() + " description " + model.getDescriptio());
		cursor.close();
		return model;
	}
	
	public FareClassUpgrade getFareUpgrade(int type){
		SQLiteDatabase db = this.getReadableDatabase();
		String where = SQLHolder.FAREC_KEY + "=?";
		String[] column = getFareColumn();
		Cursor cursor = db.query(getFareCTableName(), column,
				where , new String[]{type+""}, 
				null, null, null);
		FareClassUpgrade model = new FareClassUpgrade();
		if (cursor == null){
			return model;
		}else{
			cursor.moveToFirst();
		}
		
		model = getFareClass(cursor);
//		LogHelper.debug("Get Station name = " + model.getStationName() + " country " + model.getCountryName() + " " + model.getCountryCode() + " " + model.getKey());
		cursor.close();
		return model;
	}	
	
	private String[] getStationColumn(int type){
		if (type == 0){
			return new String[]{SQLHolder.STATION_KEY, SQLHolder.STATION_NAME, SQLHolder.STATION_COUNTRY, SQLHolder.STATION_AIRPORT};
		}else{
			return new String[]{SQLHolder.STATION_KEY, SQLHolder.STATION_NAME, SQLHolder.STATION_COUNTRY, SQLHolder.STATION_AIRPORT, SQLHolder.STATION_DESTINATION};
		}
	}
	
	private String[] getCountryColumn(){
		return new String[]{SQLHolder.COUNTRY_CODE, SQLHolder.COUNTRY_NAME, SQLHolder.COUNTRY_DIALING_CODE};
	}
	
	private String[] getFareColumn(){
		
		return new String[]{SQLHolder.FAREC_KEY, SQLHolder.FAREC_TITLE, SQLHolder.FAREC_UPGRADE, SQLHolder.FAREC_BENEFIT};
		
	}
	
	public List<StationModel> getStationList(int type){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(getStationTableName(type), null, null, null, null, null, SQLHolder.STATION_NAME + " ASC");
		List<StationModel> mArray = new ArrayList<StationModel>();
		if (cursor == null)
			return mArray;
		else if (cursor.getCount()<=0)
			return mArray;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			StationModel cc = getStation(cursor);
			if (!cc.getStationName().isEmpty()){
				mArray.add(cc);
			}
		}
		cursor.close();
		return mArray;
	}	
	
	public List<StationModel> getDepartStation(){
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(getStationTableName(1), null, null, null, null, null, SQLHolder.STATION_NAME + " ASC");
		List<StationModel> mArray = new ArrayList<StationModel>();
		if (cursor == null)
			return mArray;
		else if (cursor.getCount()<=0)
			return mArray;
		for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
			StationModel cc = getStation(cursor);
			if (!cc.getStationName().isEmpty()){
				mArray.add(cc);
			}
		}
		cursor.close();
		return mArray;
	}	
	
	public StationModel getStation(Cursor cursor){
		StationModel station = new StationModel();
		try{
			station = new StationModel(cursor.getString(cursor.getColumnIndex(SQLHolder.STATION_KEY)),
							cursor.getString(cursor.getColumnIndex(SQLHolder.STATION_NAME)),
							cursor.getString(cursor.getColumnIndex(SQLHolder.STATION_COUNTRY)),
							cursor.getString(cursor.getColumnIndex(SQLHolder.STATION_AIRPORT)));
			String destination = cursor.getString(cursor.getColumnIndex(SQLHolder.STATION_DESTINATION));
			if (destination!=null && destination.length()>0){
				String[] destiArr = destination.split("/");
				List<String> arr = new ArrayList<String>(Arrays.asList(destiArr));
				station.setDestinationKey(arr);
			}
		} catch (Exception e){
			
		}
		return station;
	}
	
	public CountryModel getCountry(Cursor cursor){
		CountryModel country = new CountryModel();
		try{
			country.setName(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_NAME)));
			country.setCode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_CODE)));
			country.setDialingDode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_DIALING_CODE)));
		} catch (Exception e){
			
		}
		return country;
	}
	
	public FareClassUpgrade getFareClass(Cursor cursor){
		FareClassUpgrade fare = new FareClassUpgrade();
		try{
			fare.setKey(cursor.getInt(cursor.getColumnIndex(SQLHolder.FAREC_KEY)));
			fare.setTitle(cursor.getString(cursor.getColumnIndex(SQLHolder.FAREC_TITLE)));
			fare.setUpgrade_title(cursor.getString(cursor.getColumnIndex(SQLHolder.FAREC_UPGRADE)));
			String benefits = cursor.getString(cursor.getColumnIndex(SQLHolder.FAREC_BENEFIT));
			if (benefits!=null && benefits.length()>0){
				String[] destiArr = benefits.split("/");
				LogHelper.debug("Get Benefits " + benefits);
				List<String> arr = new ArrayList<String>(Arrays.asList(destiArr));
				fare.setBenefits(arr);
			}else{
				fare.setBenefits(new ArrayList<String>());
			}
		} catch (Exception e){
			
		}
		return fare;
	}
	
	public SSRModel getSSR(Cursor cursor){
		SSRModel ssr = new SSRModel();
		try{
			ssr.setSsrCode(cursor.getString(cursor.getColumnIndex(SQLHolder.SSR_CODE)));
			ssr.setDescriptio(cursor.getString(cursor.getColumnIndex(SQLHolder.SSR_DESCRIP)));
		}catch (Exception e){
			e.printStackTrace();
		}
		return ssr;
	}
	
	public String getCountryName(Cursor cursor){
		String name= "";
		try{
			name = cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_NAME));
		} catch (Exception e){
			
		}
		return name;
	}
	
	public FareTaxesModel getFareTaxModel(Cursor cursor){
		FareTaxesModel fare = new FareTaxesModel();
		try{
			fare.setTitle(cursor.getString(cursor.getColumnIndex(SQLHolder.FARET_TITLE)));
			fare.setKey(cursor.getString(cursor.getColumnIndex(SQLHolder.FARET_KEY)));
		} catch (Exception e){
			
		}
		return fare;
	}
	
	/*SQL Update*/
	
	public boolean updateCountry(CountryModel country, boolean flag){
		if (country == null){
			LogHelper.debug("No Country found");
			return false;
		}
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			String where = SQLHolder.COUNTRY_CODE + "=?";
			String[] whereValue = { country.getCode()};
//			LogHelper.debug("Update Code " + country.getCode());
			db.update(getCountryTableName(), getContentValueCountry(country, flag), where , whereValue);
			db.close();
//			LogHelper.debug("Update Successfull");
			return true;
		}catch (Exception e){
			LogHelper.debug("Error Update DB " + e.getLocalizedMessage());
			return false;
		}
	}
	
	public boolean updateStation(StationModel station, boolean flag, int type){
		if (station == null){
			LogHelper.debug("No Station found");
			return false;
		}
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			String where = SQLHolder.STATION_KEY + "=?";
			String[] whereValue = { station.getKey()};
//			LogHelper.debug("Update Code " + station.getKey());
			db.update(getStationTableName(type), getContentValueStation(station, flag), where , whereValue);
			db.close();
//			LogHelper.debug("Update Successfull");
			return true;
		}catch (Exception e){
			LogHelper.debug("Error Update DB " + e.getLocalizedMessage());
			return false;
		}
	}
	
	public boolean updateFare(FareClassUpgrade fare, boolean flag, int type){
		if (fare == null){
			LogHelper.debug("No Station found");
			return false;
		}
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			String where = SQLHolder.FAREC_KEY + "=?";
			String[] whereValue = { ""+fare.getKey()};
			db.update(getFareCTableName(), getContentValueFareClass(fare, flag), where , whereValue);
			db.close();
			return true;
		}catch (Exception e){
			LogHelper.debug("Error Update DB " + e.getLocalizedMessage());
			return false;
		}
	}
	
	public boolean updateFareTaxes(FareTaxesModel taxes, boolean flag, int type){
		if (taxes == null){
			LogHelper.debug("No Station found");
			return false;
		}
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			String where = SQLHolder.FARET_KEY + "=?";
			String[] whereValue = { ""+taxes.getKey()};
			db.update(getFareTTableName(), getContentValueFareTaxes(taxes, flag), where , whereValue);
			db.close();
			return true;
		}catch (Exception e){
			LogHelper.debug("Error Update DB " + e.getLocalizedMessage());
			return false;
		}
	}
	
	public boolean updateCurrency(CountryModel currency, boolean flag){
		if (currency == null){
			LogHelper.debug("No Station found");
			return false;
		}
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			String where = SQLHolder.CURRENCY_COUNTRY + "=?";
			String[] whereValue = { ""+currency.getCode()};
			db.update(getCurrencyTableName(), getContentValueCurrency(currency, flag), where , whereValue);
			db.close();
			return true;
		}catch (Exception e){
			LogHelper.debug("Error Update DB " + e.getLocalizedMessage());
			return false;
		}
	}
	
	public boolean updateSSR(SSRModel ssr, boolean flag, int type){
		if (ssr == null){
			LogHelper.debug("No Station found");
			return false;
		}
		try {
			SQLiteDatabase db = this.getWritableDatabase();
			String where = SQLHolder.SSR_CODE + "=?";
			String[] whereValue = { ssr.getSsrCode()};
//			LogHelper.debug("Update Code " + station.getKey());
			db.update(getSSRTableName(), getContentValueSSR(ssr, flag), where, whereValue);
//			LogHelper.debug("Update Successfull");
			return true;
		}catch (Exception e){
			LogHelper.debug("Error Update DB " + e.getLocalizedMessage());
			return false;
		}
	}
	
	public CountryModel getCountryBaseCode(String code){
		try{
			SQLiteDatabase db = this.getReadableDatabase();
			String where = SQLHolder.COUNTRY_CODE + "=?";
			Cursor cursor = db.query(getCountryTableName(), null,
					where , new String[]{code}, 
					null, null, null);
			CountryModel country = new CountryModel();
			if (cursor !=null)
				cursor.moveToFirst();
			country.setName(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_NAME)));
			country.setCode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_CODE)));
			country.setDialingDode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_DIALING_CODE)));
		
			cursor.close();
			db.close();
			return country;
		} catch (Exception e){
			e.printStackTrace();		
			return null;
		}
	}
	
	public String getCountryCode(String name){
		try{
			SQLiteDatabase db = this.getReadableDatabase();
			String where = SQLHolder.COUNTRY_NAME + "=?";
			String[] column = {SQLHolder.COUNTRY_CODE, SQLHolder.COUNTRY_NAME, SQLHolder.COUNTRY_DIALING_CODE};
			Cursor cursor = db.query(getCountryTableName(), null,
					where , new String[]{name}, 
					null, null, null);
			CountryModel country = new CountryModel();
			if (cursor !=null)
				cursor.moveToFirst();
//			country.setName(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_NAME)));
//			country.setCode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_CODE)));
//			country.setDialingDode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_DIALING_CODE)));
			String code = cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_CODE));
			cursor.close();
			db.close();
			return code;
		} catch (Exception e){
			e.printStackTrace();		
			return null;
		}
	}
	
	public String getCountryName(String code){
		try{
			SQLiteDatabase db = this.getReadableDatabase();
			String where = SQLHolder.COUNTRY_CODE + "=?";
			String[] column = {SQLHolder.COUNTRY_CODE, SQLHolder.COUNTRY_NAME, SQLHolder.COUNTRY_DIALING_CODE};
			Cursor cursor = db.query(getCountryTableName(), null,
					where , new String[]{code}, 
					null, null, null);
			CountryModel country = new CountryModel();
			if (cursor !=null)
				cursor.moveToFirst();
//			country.setName(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_NAME)));
//			country.setCode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_CODE)));
//			country.setDialingDode(cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_DIALING_CODE)));
			String name = cursor.getString(cursor.getColumnIndex(SQLHolder.COUNTRY_NAME));
			cursor.close();
			db.close();
			return name;
		} catch (Exception e){
			e.printStackTrace();		
			return null;
		}
	}
	
	public List<StationModel> queryStation(String query, int type){
		try{
			List<StationModel> stationList = new ArrayList<StationModel>();
			SQLiteDatabase db = this.getReadableDatabase();

			
			String[] column = getStationColumn(type);
			Cursor cursor = db.query(getStationTableName(type), column,
					SQLHolder.STATION_AIRPORT + " LIKE '%" + query +"%' OR "
							+SQLHolder.STATION_KEY + " LIKE '%" +query + "%' OR "
							+SQLHolder.STATION_COUNTRY + " LIKE '%" +query + "%'",
					null,
                    null, null, SQLHolder.STATION_AIRPORT + " ASC", null);
			LogHelper.debug("Station Count " + cursor.getCount());
			if (cursor !=null && cursor.getCount()>0){
				for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
					StationModel ss = getStation(cursor);
					if (ss.getStationName()!=null && !ss.getStationName().isEmpty()){
						LogHelper.debug("StationName " + ss.getStationName());
						stationList.add(ss);
					}
				}
			}
			cursor.close();
			db.close();
			return stationList;
		} catch (Exception e){
			e.printStackTrace();		
			return null;
		}
	}
	
	public List<CountryModel> queryCountry(String query){
		try{
			List<CountryModel> cList = new ArrayList<CountryModel>();
			SQLiteDatabase db = this.getReadableDatabase();

			
			String[] column = getCountryColumn();
			Cursor cursor = db.query(getCountryTableName(), column,
					SQLHolder.COUNTRY_CODE + " LIKE '%" + query +"%' OR "
							+SQLHolder.COUNTRY_DIALING_CODE + " LIKE '%" +query + "%' OR "
							+SQLHolder.COUNTRY_NAME + " LIKE '%" +query + "%'",
					null,
                    null, null, SQLHolder.COUNTRY_NAME + " ASC", null);
			LogHelper.debug("Station Count " + cursor.getCount());
			if (cursor !=null && cursor.getCount()>0){
				for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){
					CountryModel cc = getCountry(cursor);
					if (cc.getName()!=null && !cc.getName().isEmpty()){
						cList.add(cc);
					}
				}
			}
			cursor.close();
			db.close();
			return cList;
		} catch (Exception e){
			e.printStackTrace();		
			return null;
		}
	}

	public CountryModel queryCurrency(String currencyCode){
		try{
			SQLiteDatabase db = this.getReadableDatabase();
			String where = SQLHolder.CURRENCY_COUNTRY + "=?";
			Cursor cursor = db.query(getCurrencyTableName(), null,
					where, new String[]{currencyCode},
					null, null, null);
			CountryModel country = new CountryModel();
			if (cursor !=null)
				cursor.moveToFirst();
			country.setCode(cursor.getString(cursor.getColumnIndex(SQLHolder.CURRENCY_COUNTRY)));
			byte[] bytes = cursor.getBlob(cursor.getColumnIndex(SQLHolder.CURRENCY_PRICE_LIST));
			country.setCurrency(bytes);

			cursor.close();
			db.close();
			return country;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	//Table Name Part
	public static String getCountryTableName() {
		return SQLHolder.COUNTRY_TABLE;
	}
	
	public String getStationTableName(int type) {
		if (type == 0){
			return SQLHolder.STATION_TABLE;
		}else{
			return SQLHolder.DEPART_STATION_TABLE;
		}
	}
	
	public String getSSRTableName(){
		return SQLHolder.SSR_TABLE;
	}
	
	public String getFareCTableName(){
		return SQLHolder.FAREC_TABLE;
	}
	
	public String getFareTTableName(){
		return SQLHolder.FARET_TABLE;
	}
	
	public String getCurrencyTableName(){
		return SQLHolder.CURRENCY_TABLE;
	}
	
}






