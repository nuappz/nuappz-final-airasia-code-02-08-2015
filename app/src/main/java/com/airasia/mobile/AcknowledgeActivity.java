package com.airasia.mobile;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;

import com.androidquery.AQuery;

public class AcknowledgeActivity extends ActionBarActivity {

	AQuery aq;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setupActionBar();
		setContentView(R.layout.activity_acknowledge);
		aq = new AQuery(this);
		aq.id(R.id.acknowledge_text).text(R.string.signup_acknowledgement_1);
	}


	private void setupActionBar() {
		
		
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);
        
        AQuery abaq = new AQuery(v);
        abaq.id(R.id.leftButton).gone();

        ab.setCustomView(v);
        
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
		Toolbar parent =(Toolbar) ab.getCustomView().getParent();
		parent.setContentInsetsAbsolute(0, 0);
    }
	
	


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (aq!=null){
			aq = null;
		}
		super.onDestroy();
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		this.finish();
		super.onBackPressed();
	}
	
	
	
	
}
