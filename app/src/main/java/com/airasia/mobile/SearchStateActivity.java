package com.airasia.mobile;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;

import com.airasia.adapter.PopUpListView;
import com.airasia.model.CountryModel;
import com.airasia.model.StationModel;
import com.airasia.util.CustomHttpClient;
import com.airasia.util.JSonHelper;
import com.airasia.util.LogHelper;
import com.airasia.util.SQLhelper;
import com.androidquery.AQuery;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SearchStateActivity extends ActionBarActivity {

	AQuery aq;
	int viewId, type;
	List<StationModel> stationList;
	String departCode = "";
	SQLhelper db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_view);
		db = new SQLhelper(SearchStateActivity.this);
		aq = new AQuery(this);
		aq.id(R.id.check_in_search_container).visible();
		Bundle extras = getIntent().getExtras();
		if (extras != null){
			type = extras.getInt("type", 0);
			viewId = extras.getInt("viewId", 0);
			departCode = extras.getString("departCode", "");
		}
		
		if (type == 1){
			List<CountryModel> cList = db.getCountryModels();
			setupCountryList(cList);
		}else{
			if(departCode!=null && departCode.length()>0){
				setupStationBaseDepart(departCode);
			}else{
				stationList = db.getDepartStation();
				setupStationList(stationList, 0);
			}
		}
		db.close();
	}

	public void setupStationList(List<StationModel> list, final int type){
		PopUpListView adapter = new PopUpListView(aq.id(R.id.list_view_container).getView(),
				list,
				this,
				PopUpListView.FLIGHT_STATION); 
		aq.id(R.id.list_view_container).getListView().setAdapter(adapter);
		aq.id(R.id.list_view_container).getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
		    @Override
		     public void onItemClick(AdapterView<?> arg0, View view,
		        int position, long arg3) {
		         // TODO Auto-generated method stub
		    		PopUpListView adp = (PopUpListView) arg0.getAdapter();
		    		StationModel item = (StationModel) adp.getItem(position);
		    		
		    		Intent intent = new Intent();
		    		Bundle extras = new Bundle();
		    		extras.putString("key", item.getKey());
		    		extras.putString("name", item.getStationName());
		    		extras.putInt("viewId", viewId);
		            intent.putExtras(extras);
		    		setResult(RESULT_OK, intent);
		    		finish();
		    	}
		});
		
		aq.id(R.id.check_in_search_text).getEditText().addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				List<StationModel> tempList = new ArrayList<StationModel>();
				if (s.length()<=0){
					if (type == 0){
						LogHelper.debug("Get From db");
						tempList = db.getStationList(type);
					}else{
						LogHelper.debug("Get From conn " + stationList.size());
						tempList.addAll(stationList);
					}
				}else{ 
					if (type == 0){
						LogHelper.debug("Get From db (Filter)");
						tempList = db.queryStation(String.valueOf(s), type);
					}else{
						LogHelper.debug("Get From conn (filter)");
						tempList = filterStation(stationList, String.valueOf(s));
					}
				}
				
				if (tempList!=null && tempList.size()>0){
					PopUpListView adapter = (PopUpListView) aq.id(R.id.list_view_container).getListView().getAdapter();
					adapter.updateStation(tempList);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	 }
	
	public void setupCountryList(List<CountryModel> list){
		PopUpListView adapter = new PopUpListView(aq.id(R.id.list_view_container).getView(),
				this,
				list,
				PopUpListView.COUNTRY); 
		aq.id(R.id.list_view_container).getListView().setAdapter(adapter);
		aq.id(R.id.list_view_container).getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
		    @Override
		     public void onItemClick(AdapterView<?> arg0, View view,
		        int position, long arg3) {
		         // TODO Auto-generated method stub
		    		PopUpListView adp = (PopUpListView) arg0.getAdapter();
		    		CountryModel item = (CountryModel) adp.getItem(position);
		    		
		    		Intent intent = new Intent();
		    		Bundle extras = new Bundle();
		    		extras.putString("key", item.getCode());
		    		extras.putString("name", item.getName());
		    		extras.putInt("viewId", viewId);
		            intent.putExtras(extras);
		    		setResult(RESULT_OK, intent);
		    		finish();
		    	}
		});
		
		aq.id(R.id.check_in_search_text).getEditText().addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				List<CountryModel> tempList = new ArrayList<CountryModel>();
				if (s.length()<=0){
					LogHelper.debug("Get Country List From db");
					tempList = db.getCountryModels();
				}else{ 
					LogHelper.debug("Get From db (Filter)");
					tempList = db.queryCountry(String.valueOf(s));
				}
				
				if (tempList!=null && tempList.size()>0){
					PopUpListView adapter = (PopUpListView) aq.id(R.id.list_view_container).getListView().getAdapter();
					adapter.updateCountry(tempList);
				}
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
	 }
	
	private void setupStationBaseDepart(String code){
		if(db!=null){
			StationModel model = db.getStationBaseKey(code, 1);
			LogHelper.debug("Code = " + code + " model " +  model.getCountryName());
			stationList = new ArrayList<StationModel>();
			for (int i = 0 ; i < model.getDestinationKey().size() ; i++){
				StationModel item = db.getStationBaseKey(model.getDestinationKey().get(i), 1);
				if (item.getStationName() !=null
						&& item.getStationName().length()>0
						&& !item.getStationName().equals("null")){
					stationList.add(item);
				}
			}
			setupStationList(stationList, 1);
		}
		
	}
	
//	
//	private void connGetDestination(){
//		RequestParams params = new RequestParams();
////		params.put("departureStation", code);
//		CustomHttpClient.postWithHash(CustomHttpClient.GET_STATION, params, new AsyncHttpResponseHandler() {
//			
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//				// TODO Auto-generated method stub
//				String response = new String(responseBody);
//				LogHelper.debug("Destination Response " + response);
//				
//				int responseCode = JSonHelper.GetResponseStatus(response);
//				if (responseCode == JSonHelper.SUCCESS_REPONSE){
//					stationList = JSonHelper.parseDestination(response);
//					SQLhelper db = new SQLhelper(SearchStateActivity.this);
//					
//					setupStationList(stationList, 1);
//				}else{
//					
//				}
//			}
//			
//			@Override
//			public void onFailure(int statusCode, Header[] headers,
//					byte[] responseBody, Throwable error) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
//		
//	}
	
	public List<StationModel> filterStation(List<StationModel> _list, String text){
		List<StationModel> tempList = new ArrayList<StationModel>();
		
		String lowerCase = text.toLowerCase();
		
		for (int i =0; i <_list.size(); i++){
			StationModel model = _list.get(i);
			if((model.getCountryCode() != null && model.getCountryCode().toLowerCase().contains(lowerCase))
				|| (model.getStationName()!=null && model.getStationName().toLowerCase().contains(lowerCase))
				|| (model.getCountryName()!=null && model.getCountryName().toLowerCase().contains(lowerCase))
				|| (model.getKey()!=null && model.getKey().toLowerCase().contains(lowerCase))){
				tempList.add(model);
			}
		}
		
		return tempList;
	}
	
}
