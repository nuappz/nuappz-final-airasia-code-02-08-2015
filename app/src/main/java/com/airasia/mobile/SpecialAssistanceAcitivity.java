package com.airasia.mobile;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.airasia.layout.FontableTextView;
import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpecialAssistanceAcitivity extends ActionBarActivity {

    AQuery aq;
    FontableTextView specialGuest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistance);
        setupActionBar();
        specialGuest = (FontableTextView) findViewById(R.id.spe_guest);
        specialGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenDialog();
            }
        });
    }

    private void OpenDialog() {
        View alertView;
        final String[] nameList = new String[]{"Koo Liew Xing", "Lim Soo Ling", "Harina", "Shanmugavel", "Nehru", "Surya", "Parthiban"};
        List<HashMap<String, String>> fillMaps = new
                ArrayList<HashMap<String, String>>();
        int sno = 1;
        fillMaps.clear();
        for (int i = 0; i < nameList.length; i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("sno", String.valueOf(sno));
            map.put("name", nameList[i]);
            fillMaps.add(map);
            sno++;
        }
        String[] from = new String[]{"sno", "name"};
        int[] to = new int[]{R.id.tv_adapter_sno, R.id.tv_adapter_name};
        SimpleAdapter adapter = new SimpleAdapter(SpecialAssistanceAcitivity.this, fillMaps, R.layout.custom_list_adapter_items, from, to);

        AlertDialog.Builder builder = new AlertDialog.Builder(SpecialAssistanceAcitivity.this);
        LayoutInflater inflater = LayoutInflater.from(SpecialAssistanceAcitivity.this);
        alertView = inflater.inflate(R.layout.custom_list_assistance_namelist, null);
        ListView lv = (ListView) alertView.findViewById(R.id.listview1);
        lv.setAdapter(adapter);
        builder.setView(alertView);
        builder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        dialog.show();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = nameList[position];
                //Toast.makeText(SpecialAssistanceAcitivity.this,name,Toast.LENGTH_SHORT).show();
                Intent ints = new Intent(SpecialAssistanceAcitivity.this, SpecialAssistanceOverViewActivity.class);
                ints.putExtra("name", name);
                startActivity(ints);
                SpecialAssistanceAcitivity.this.finish();

            }
        });
    }


    private void setupActionBar() {

        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);

        tempQ.id(R.id.aa_logo).gone();
        tempQ.id(R.id.app_title).text("Special Assistance");
        tempQ.id(R.id.app_title).text("");
        tempQ.id(R.id.leftButton_view).gone();
        tempQ.id(R.id.rightDone).text(R.string.ok_text);
        tempQ.id(R.id.rightButton_done).visible();

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
    }

}
