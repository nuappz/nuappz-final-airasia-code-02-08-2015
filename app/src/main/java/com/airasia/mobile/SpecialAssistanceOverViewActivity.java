package com.airasia.mobile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by NUappz1 on 8/2/2015.
 */
public class SpecialAssistanceOverViewActivity extends ActionBarActivity {

    AQuery aq;

    String sel_Name = "";
    private TextView tv_getsName;
    private TextView tv_chairName;
    private TextView tv_return_chairName;
String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_special__assist_overview_);
        setupActionBar();

        tv_getsName = (TextView) findViewById(R.id.tv_GuestName);
        name= getIntent().getStringExtra("name");
        tv_getsName.setText(name);
        tv_chairName = (TextView) findViewById(R.id.tv_chairName);

        tv_chairName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               OpenDialog(1);

            }
        });
        tv_return_chairName = (TextView) findViewById(R.id.tv_return_chairName);
        tv_return_chairName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenDialog(2);
            }
        });

    }

    private void OpenDialog(final int flag) {
        View alertView;

        final String[] nameList = new String[]{"Unable to walk long distance", "No Wheelchair add", "paraplegic"};

        List<HashMap<String, String>> fillMaps = new
                ArrayList<HashMap<String, String>>();
        int sno = 1;
        fillMaps.clear();
        for (int i = 0; i < nameList.length; i++) {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("sno", String.valueOf(sno));
            map.put("name", nameList[i]);
            fillMaps.add(map);
            sno++;
        }
        String[] from = new String[]{"sno", "name"};
        int[] to = new int[]{R.id.tv_adapter_sno, R.id.tv_adapter_name};
        SimpleAdapter adapter = new SimpleAdapter(SpecialAssistanceOverViewActivity.this, fillMaps, R.layout.custom_list_adapter_items, from, to);

        AlertDialog.Builder builder = new AlertDialog.Builder(SpecialAssistanceOverViewActivity.this);
        LayoutInflater inflater = LayoutInflater.from(SpecialAssistanceOverViewActivity.this);
        alertView = inflater.inflate(R.layout.custom_list_assistance_namelist, null);
        ListView lv = (ListView) alertView.findViewById(R.id.listview1);
        lv.setAdapter(adapter);
        builder.setView(alertView);
        builder.setNegativeButton("cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //sel_Name = nameList[which].toString();
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();
        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.CENTER;
        dialog.show();
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sel_Name = nameList[position];
                if (sel_Name.equals(null) || sel_Name.equals(""))
                    sel_Name = "No Wheelchair add";
                if(flag == 1)
                    tv_chairName.setText(sel_Name);
                else
                    tv_return_chairName.setText(sel_Name);
                dialog.dismiss();
            }
        });
    }


    private void setupActionBar() {

        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);

        tempQ.id(R.id.aa_logo).gone();
        tempQ.id(R.id.app_title).text("Special Assistance");
        tempQ.id(R.id.leftButton_view).gone();
        tempQ.id(R.id.rightDone).text(R.string.ok_text);
        tempQ.id(R.id.rightButton_done).visible();

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
    }

}

