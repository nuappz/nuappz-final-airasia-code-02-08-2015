package com.airasia.mobile;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.airasia.fragment.FragmentBase;
import com.airasia.fragment.FragmentCase;

public class BookingActivity extends Activity{

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_booking);
		switchFragment(FragmentCase.FRAG_BOOK_FLIGHTS);
	}
	
	void switchFragment(int _pos){
		Fragment newFragment = new FragmentBase();
		Bundle args = new Bundle();
		args.putInt("type", _pos);
		newFragment.setArguments(args);
		//initFragment(newFragment);
		
	}
	
//	void initFragment(Fragment frag){
//		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//
//		// Replace whatever is in the fragment_container view with this fragment,
//		// and add the transaction to the back stack if needed
////		transaction.replace(R.id.login_container, frag);
////		transaction.addToBackStack(null);
//		transaction.replace(R.id.booking_container, frag);
//		transaction.addToBackStack(null);
//
//		// Commit the transaction
//		transaction.commit();
//	}
	
}

