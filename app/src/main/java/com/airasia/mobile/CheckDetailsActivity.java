package com.airasia.mobile;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;

import com.airasia.adapter.PopUpListView;
import com.airasia.fragment.FragmentCase;
import com.airasia.fragment.FrenFamilyFragment;
import com.airasia.holder.PrefsHolder;
import com.airasia.model.CountryModel;
import com.airasia.model.StationModel;
import com.airasia.util.LogHelper;
import com.airasia.util.SQLhelper;
import com.androidquery.AQuery;

public class CheckDetailsActivity extends ActionBarActivity{

	AQuery aq;
	String userName, national, dob;
	int pos, parentPos;
	Handler handler;
	AlertDialog dialog;
	SharedPreferences prefs;
	public static final int FREN_PICK = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_check_in_details);
		
		
		AQuery aq = new AQuery(this);
		prefs = getSharedPreferences(PrefsHolder.PREFS, MODE_PRIVATE);
		Bundle extras = getIntent().getExtras();
		if (extras != null){
			userName = extras.getString("name", "");
			national = extras.getString("national", "");
			String passport = extras.getString("passport", "");
			String issue = extras.getString("issue", "");
			String expDate = extras.getString("expDate", "");
			
			dob = extras.getString("dob", "");
			pos = extras.getInt("child", 0);
			parentPos = extras.getInt("parent", 0);
			
			aq.id(R.id.checkin_details_header).getTextView()
					.setText(getString(R.string.check_in_details)+ " " +userName);
			aq.id(R.id.check_in_passport).getTextView().setText(passport);
			
			SimpleDateFormat inPattern = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat outPattern = new SimpleDateFormat("dd MM yyyy");;
			try {
				Date date;
				String dateString;
				
				if (dob != null && !dob.isEmpty()){
					date = inPattern.parse(dob);
					dateString = outPattern.format(date);
					aq.id(R.id.check_in_dob).getButton().setText(dateString);
				}
				
				if (expDate != null && !expDate.isEmpty()){
					date = inPattern.parse(expDate);
					dateString = outPattern.format(date);
					aq.id(R.id.check_in_expiration).getButton().setText(dateString);
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			SQLhelper db = new SQLhelper(this);
			String countryName = db.getCountryName(national);

			aq.id(R.id.check_in_national).getButton().setText(countryName);
			aq.id(R.id.check_in_national).getButton().setTag(national);
			
			if (issue != null && !issue.isEmpty()){
				countryName = db.getCountryName(issue);
				aq.id(R.id.check_in_issue).getButton().setText(countryName);
				aq.id(R.id.check_in_issue).getButton().setTag(issue);
			}

			
		}
		
		if (prefs.getBoolean(PrefsHolder.IS_LOGIN, false)){
			aq.id(R.id.check_in_fren_famly).visible();
		}else{
			aq.id(R.id.check_in_fren_famly).gone();
		}
		
		handler = new Handler();
		
		setupActionBar();
//		updateDetails();
		
	}
	
	
	
	public void donePressed(View v){
		Bundle extras = new Bundle();
		
		String passport =  aq.id(R.id.check_in_passport).getTextView().getText().toString();
		String expDate = aq.id(R.id.check_in_expiration).getButton().getText().toString();
		String dob =  aq.id(R.id.check_in_dob).getButton().getText().toString();
		String national =  (String) aq.id(R.id.check_in_national).getButton().getTag();
		String issue =  (String) aq.id(R.id.check_in_issue).getButton().getTag();
		
    	extras.putString("issue", issue);
		extras.putString("national", national);
		extras.putString("passport", passport);
		extras.putInt("child", pos);
		extras.putInt("parent", parentPos);

		SimpleDateFormat inPattern = new SimpleDateFormat("dd MM yyyy");
		SimpleDateFormat outsPattern = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = inPattern.parse(dob);
			String dateString = outsPattern.format(date);
	    	extras.putString("dob", dateString);
	    	
	    	date = inPattern.parse(expDate);
			dateString = outsPattern.format(date);
	    	extras.putString("expireDate", dateString);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		Intent intent = new Intent();
        intent.putExtras(extras);
		setResult(RESULT_OK, intent);
		this.finish();
	}
	
	private void setupActionBar() {
		
        ActionBar ab = getSupportActionBar();
        ab.setDisplayShowCustomEnabled(true);
        ab.setDisplayShowTitleEnabled(false);
        //ab.setIcon(R.drawable.logo_aa);
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.custom_action_bar, null);

        AQuery tempQ = new AQuery(v);
        
        tempQ.id(R.id.leftButton_view).gone();
        tempQ.id(R.id.rightButton_view).gone();
        tempQ.id(R.id.rightButton_done).visible();
        tempQ.id(R.id.aa_logo).gone();
        tempQ.id(R.id.app_title).text(getString(R.string.check_in_title));

        ab.setCustomView(v);
        ab.setDisplayUseLogoEnabled(false);
        ab.setDisplayShowHomeEnabled(false);
		Toolbar parent =(Toolbar) ab.getCustomView().getParent();
		parent.setContentInsetsAbsolute(0, 0);
        
    }
	
	private void hideButton(int button, boolean hide) {

        ActionBar ab = getSupportActionBar();
        AQuery tempQ = new AQuery(ab.getCustomView());
        if(hide == true){
        	tempQ.id(button).gone();
        }else{
        	tempQ.id(button).visible();
        }
    }
	
	private void updateTitle(String text, boolean hideTitle, boolean hideBanner) {

        ActionBar ab = getSupportActionBar();
        AQuery tempQ = new AQuery(ab.getCustomView());
        
        if (text !=null && text.length()>0 ){
        	tempQ.id(R.id.app_title).getTextView().setText(text);
        }
        

        if(hideTitle == true){
        	tempQ.id(R.id.app_title).gone();
        }else{
        	tempQ.id(R.id.app_title).visible();
        }
        
        if(hideBanner == true){
        	tempQ.id(R.id.aa_logo).gone();
        }else{
        	tempQ.id(R.id.aa_logo).visible();
        }
    }
	
	void updateDetails(){
		handler = new Handler();
		handler.postDelayed(detailRunnable, 1000);
	}
	
	Runnable detailRunnable = new Runnable() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			aq.id(R.id.checkin_details_header).getTextView().setText(getString(R.string.check_in_details)+userName);
			
		}
	};
	
	
	
	public void datePick(final View v){
		Calendar c = Calendar.getInstance();
	    int mYear = c.get(Calendar.YEAR);
	    int mMonth = c.get(Calendar.MONTH);
	    int mDay = c.get(Calendar.DAY_OF_MONTH);
	    System.out.println("the selected " + mDay);
	    DatePickerDialog dpd = new DatePickerDialog(CheckDetailsActivity.this,
	            new OnDateSetListener() {
					
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear,
							int dayOfMonth) {
						// TODO Auto-generated method stub
						Button tv = (Button) v;
						SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");
						String dateString = new String(dayOfMonth+"/"+(monthOfYear+1)+"/"+year);
						try {
							Date date = pattern.parse(dateString);
							pattern = new SimpleDateFormat("dd MM yyyy");
							dateString = pattern.format(date);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
//						tv.setText(dayOfMonth+" "+
//								(monthOfYear+1)+" "+
//								year);
						tv.setText(dateString);
					}
				}, mYear, mMonth, mDay);
	    dpd.setTitle(R.string.signup_dob);
	    dpd.show();
	}
	
	public void selectFren(View v){
		Intent i = new Intent(CheckDetailsActivity.this, MyProfileActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("type", MyProfileActivity.MY_FREN_SELECT);
		i.putExtras(bundle);
		startActivityForResult(i, FREN_PICK);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    // Check which request we're responding to
	    if (requestCode == FREN_PICK) {
	        // Make sure the request was successful
	        if (resultCode == RESULT_OK) {
	        	Bundle bundle = data.getExtras();
	            if (bundle != null){
	            	String expDate = bundle.getString("expireDate", "");
	            	String issue = bundle.getString("issue", "");
	            	String dob = bundle.getString("dob", "");
					String national = bundle.getString("national", "");
					String passport = bundle.getString("passport", "");
					
					aq.id(R.id.check_in_passport).getTextView().setText(passport);
					
					SimpleDateFormat inPattern = new SimpleDateFormat("ddMMyyyy");
					SimpleDateFormat outPattern = new SimpleDateFormat("dd MM yyyy");
					try {
						Date date = inPattern.parse(expDate);
						String dateString = outPattern.format(date);

						aq.id(R.id.check_in_expiration).getButton().setText(dateString);
						
						date = inPattern.parse(dob);
						dateString = outPattern.format(date);
						aq.id(R.id.check_in_dob).getButton().setText(dateString);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					SQLhelper db = new SQLhelper(this);
					String countryName = db.getCountryName(national);

					aq.id(R.id.check_in_national).getButton().setText(countryName);
					aq.id(R.id.check_in_national).getTextView().setTag(national);

					countryName = db.getCountryName(issue);
					aq.id(R.id.check_in_issue).getButton().setText(countryName);
					aq.id(R.id.check_in_issue).getButton().setTag(issue);
					
					
	            }
	            
	            
	        }
	    } 
	}



	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		if (handler !=null){
			handler.removeCallbacks(detailRunnable);
		}
		super.onDestroy();
	}
	
	public void PickCountry(final View v){
		SQLhelper db = new SQLhelper(CheckDetailsActivity.this);
		List<CountryModel> countryList = db.getCountryModels();
		
		LogHelper.debug("Show Alert List");
	 	AlertDialog.Builder builder=new AlertDialog.Builder(CheckDetailsActivity.this);
		builder.setTitle(R.string.country);
		ListView listView=new ListView(CheckDetailsActivity.this);
		PopUpListView adapter = new PopUpListView(listView, CheckDetailsActivity.this, countryList, PopUpListView.COUNTRY); 
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	 
		    @Override
		     public void onItemClick(AdapterView<?> arg0, View view,
		        int position, long arg3) {
		         // TODO Auto-generated method stub
		    		Button btn = (Button) v;
		    		PopUpListView adp = (PopUpListView) arg0.getAdapter();
		    		CountryModel item = (CountryModel) adp.getItem(position);
		    		btn.setText(item.getName());
		    		btn.setTag(item.getCode());
		    		
		    		if (dialog.isShowing()){
		    			dialog.dismiss();
		    		}
		    	}
		});
		builder.setView(listView);
        dialog=builder.create();
        dialog.show();
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if(aq == null){
			aq = new AQuery(this);
		}
		super.onResume();
	}
	
	
	
}
