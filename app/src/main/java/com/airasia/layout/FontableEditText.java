package com.airasia.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.airasia.mobile.R;


public class FontableEditText extends EditText {

	public FontableEditText(Context context) {
		super(context);
	}

	public FontableEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableEditText,
					R.styleable.com_airasia_mobile_FontableEditText_font);
		}
	}

	public FontableEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode()) {
			UiUtil.setCustomFont(this, context, attrs,
					R.styleable.com_airasia_mobile_FontableEditText,
					R.styleable.com_airasia_mobile_FontableEditText_font);
		}
	}
}
